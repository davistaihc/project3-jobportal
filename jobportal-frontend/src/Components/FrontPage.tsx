import React, { useEffect, useState } from 'react'
import { Card, Row, CardDeck, Button, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import Moment from 'react-moment';
import { getAllJobThunk } from '../Redux/Job/thunk'
import { Link } from 'react-router-dom'
import { Homepage } from './Homepage'
import { setSelectedViewJob } from '../Redux/Job/actions'
import { MyVerticallyCenteredModal } from './MyVerticallyCenteredModal'
import { getStudentProfileThunk } from '../Redux/Profile/thunks'
import { roleObj } from '../variable/variable'
import { getStaffNameThunk } from '../Redux/Staff/thunks'
import ChartCohortStatus from './ChartCohortStatus'
import ChartApplicationStatus from './ChartApplicationStatus'
import './FrontPage.scss'
import { push } from 'connected-react-router'



export const FrontPage = () => {
   const dispatch = useDispatch();
   const isLoggedIn = localStorage.getItem('token') ? true : false;
   const role = useSelector((state:RootState)=> state.auth.roleId)

   // useEffect(() => {
   //    const params = new URLSearchParams(window.location.search)
   //    dispatch(loginGitlabThunk(params.get('code')!));
   // }, [dispatch])

   useEffect(() => {
      if (role?.toString() === roleObj.other) {
         dispatch(push("/company/dashboard"))
      }else if(role?.toString() === roleObj.student){
      dispatch(getStudentProfileThunk())
      dispatch(getAllJobThunk())
      }else if(role?.toString() === roleObj.staff){
      dispatch(getStaffNameThunk())
      }
   }, [dispatch, role]);


   const jobIds = useSelector((state: RootState) => state.jobs.jobs)
   const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))
   const studentName = useSelector((state: RootState) => state.student.first_name)
   const [showAll, setShowAll] = useState(false)
   const [modalShow, setModalShow] = React.useState(false);
   const handleShowAll = () => setShowAll(true)
   const handleShowFour = () => setShowAll(false)

   const staff = useSelector((state: RootState) => state.staff.staff_name)


   function makeUp(jobHighlight: string | null) {
      const highlight = jobHighlight?.toString().split(/•|\r\n/)
      return (
         highlight?.map((e, i) => (e !== "" &&
            <li key={i}>{e}</li>
         ))
      )
   }


   const jobArr = []
   for (const job of jobs) {
      if (job.is_approved === true && job.availability === true) {
         jobArr.push(job)
      }
   }

   return (
      <>
         {isLoggedIn ?
            role?.toString() === roleObj.staff ?
               <>

                  <Row>
                     <Col>  <div id="peopleName">{staff.toUpperCase()}, Good Day ! </div>
                        <br></br>
                     </Col>
                  </Row>

                  <div>
                     <Row id="chartBody">
                        <Col md={6}>
                           <ChartCohortStatus />
                        </Col>
                        <Col md={6}>
                           <ChartApplicationStatus />
                        </Col>
                     </Row>
                  </div>
               </>
               :

               <div id="jobNews" >

                  <Row id='header'>
                     <div>
                        <div id="peopleName">Hello {studentName}, here are the latest Jobs. Good Luck! </div>
                     </div>
                     <hr />
                     {showAll && <Button variant="outline-secondary" onClick={handleShowFour}>Hide</Button>}
                     {!showAll && <Button variant="outline-secondary" onClick={handleShowAll}>Show All</Button>}
                  </Row>
                  {/* style={{ minWidth: '15rem', maxWidth: '22rem', marginBottom: '2rem'  }} */}
                  <Row id='body'>
                     <CardDeck id="cardBox">
                        {!showAll && jobArr.map((job: any, i) => (i < 4 &&
                           <Card className="card" key={i} >
                              <Card.Body>
                                 <Card.Title>{job.title}</Card.Title>
                                 <Card.Subtitle className="mb-2 text-muted">{job.name}</Card.Subtitle>
                                 <Card.Text>
                                    {job.location}
                                 </Card.Text>
                                 <Card.Text>
                                    <ul>
                                       {makeUp(job.highlight)}
                                    </ul>
                                 </Card.Text>
                                 {i < 1 && <MyVerticallyCenteredModal
                                    show={modalShow}
                                    onHide={() => setModalShow(false)}
                                 />}
                              </Card.Body>
                              <Card.Footer className="text-muted">
                                 <Moment toNow>{job.created_at}</Moment>
                                 <Button variant="outline-primary" onClick={() => { setModalShow(true); dispatch(setSelectedViewJob(job)) }}>Learn More</Button>
                              </Card.Footer>

                           </Card>
                        ))}
                        {showAll && jobArr.map((job: any, i) => (
                           <Card className="card"  >
                              <Card.Body>
                                 <Card.Title>{job.title}</Card.Title>
                                 <Card.Subtitle className="mb-2 text-muted">{job.name}</Card.Subtitle>
                                 <Card.Text>
                                    {job.location}
                                 </Card.Text>
                                 <Card.Text>
                                    <ul>
                                       {makeUp(job.highlight)}
                                    </ul>
                                 </Card.Text>
                                 {/* <Card.Link href="#" onClick={() => { setModalShow(true); dispatch(setSelectedViewJob(job)) }} className="detail" key={`jobId_${job.id}`}> Details</Card.Link> */}
                                 {i < 1 && <MyVerticallyCenteredModal
                                    show={modalShow}
                                    onHide={() => setModalShow(false)}
                                 />}
                              </Card.Body>
                              <Card.Footer className="text-muted">
                                 <Moment toNow>{job.created_at}</Moment>
                                 <Button variant="outline-primary" onClick={() => { setModalShow(true); dispatch(setSelectedViewJob(job)) }}>Learn More</Button>
                              </Card.Footer>

                           </Card>
                        ))}
                     </CardDeck>
                  </Row>
               </div >
            :
            <Link to="/">
               <Homepage />
            </Link>
         }
      </>
   )
}