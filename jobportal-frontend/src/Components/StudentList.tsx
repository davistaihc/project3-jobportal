import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchStudents } from '../Redux/Student/thunks';
import { RootState } from '../store';
import { fetchCohorts } from '../Redux/cohorts/thunk';
import { useRouteMatch } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import { postEmailMapping } from '../Redux/EmailMapping/thunk';
import './StudentList.scss'


export function StudentList() {
    const cohorts = useSelector((state: RootState) => state.cohorts.cohorts)
    const [selectedCohort, setSelectedCohort] = useState('')
    const studentIds = useSelector((state: RootState) => state.students.studentsByCohort[selectedCohort])
    const students = useSelector((state: RootState) => studentIds?.map(id => state.students.studentsById[id]))
    const match = useRouteMatch<{ jobId?: string }>();
    const jobId = match.params.jobId

    const [showAvailable, setShowAvailable] = useState<boolean>(true)
    const showAvailableV = () => setShowAvailable(!showAvailable)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchStudents())
        dispatch(fetchCohorts())
    }, [dispatch])

    // const refresh = () => {
    //     window.location.reload()
    // }

    return (
        <div>

            <div className="StudentHeader">
                <div><h5>Students list</h5></div>
                <div>
                    {
                        showAvailable ?
                            <Button variant="outline-secondary" onClick={showAvailableV}>Only Students w/ CV and available</Button>
                            :
                            <Button variant="outline-secondary" onClick={showAvailableV}>Show All Students</Button>
                    }</div>
            </div>
            <hr />
            <div id="studentListForApplication">
                <Table striped bordered hover id="StudentTable" >
                    <thead>
                        <tr><th><select onChange={event => setSelectedCohort(event.currentTarget.value)}>
                            <option>Please select Cohort</option>
                            {cohorts.map((coh, index) => <option key={`cohort_${index}`}>{coh.cohort}</option>)}
                        </select></th>
                            <th>Status</th>
                            <th>Resume</th>
                            <th>Portfolio</th>
                        </tr>
                    </thead>
                    <tbody>
                        {!showAvailable && students?.map((student, index) => student.resume !== null && student.status !== "Not Available" && student.status !== "Employed" && (
                            <tr className="student" style={{ cursor: "pointer" }} key={`student_${index}`} onClick={async () => {
                                if (jobId == null) {
                                    return;
                                }
                                dispatch(postEmailMapping(parseInt(jobId), student.id))
                                // refresh()
                            }}>
                                <th><div id="studentName">{student.first_name} {student.surname}</div></th>
                                <th>{student.status}</th>
                                <th >{student.resume}</th>
                                <th >{student.portfolio}</th>

                            </tr>
                        ))}
                        {showAvailable && students?.map((student, index) => (
                            <tr className="student" style={{ cursor: "pointer" }} key={`student_${index}`} onClick={async () => {
                                if (jobId == null) {
                                    return;
                                }
                                dispatch(postEmailMapping(parseInt(jobId), student.id))
                                // refresh()
                            }}>
                                <th>{student.first_name} {student.surname}</th>
                                <th>{student.status}</th>
                                <th>{student.resume}</th>
                                <th>{student.portfolio}</th>

                            </tr>
                        ))}

                    </tbody>
                </Table>
            </div>
        </div >
    )
}
