import React, { useState } from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import "./UploadResume.scss"
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { RootState } from '../store'
import { uploadPortfolioThunk } from '../Redux/Profile/thunks'


interface Data {
    portfolio: string
}

export const UploadPortfolio = () => {

    const dispatch = useDispatch()
    const student = useSelector((state: RootState) => state.student, shallowEqual);
    const [show, setShow] = useState(false)
    const handleShow = () => setShow(true)
    const [portfolio, setPortfolio] = useState<undefined | File>(undefined)
    const onHide = () => setShow(false)


    return (
        <>
            <Button variant="outline-success" onClick={handleShow} >Upload Portfolio</Button>
            <Modal show={show} onHide={onHide} size="sm" centered >
                <Modal.Header closeButton  >
                    <Modal.Title>Upload Portfolio</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form onSubmit={(event: any) => {
                        event.preventDefault();
                        const formData = new FormData();
                        if(portfolio && portfolio.size > 2000000){
                            alert("Please upload less than 2Mb!")
                            return
                        }

                        if (portfolio !== undefined) {
                            formData.append('portfolio', portfolio)
                        }
                        dispatch(uploadPortfolioThunk(student.studentId, formData))
                        onHide()
                    }} >
                        <div id="upload">
                            <div>
                                <Form.File
                                    id="portfolio"
                                    label="Caption: Only accept PDF format and file size less 2mb."
                                    data-browse="Upload"
                                    onChange={(e: any) => setPortfolio(e.currentTarget.files?.[0])}
                                    accept="application/pdf"
                                />
                            </div>
                            <div>
                                <Button type="submit" >Upload Now</Button>
                            </div>
                        </div>

                    </Form>
                </Modal.Body>
            </Modal>

        </>

    )



}