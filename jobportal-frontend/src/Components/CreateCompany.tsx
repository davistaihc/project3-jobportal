import React, { useState } from 'react'
import './CreateCompany.scss'
import { Row, Container, Col, Form, Button } from 'react-bootstrap';
// import { createCompanyThunk } from '../Redux/Company/thunk';
import { useDispatch } from 'react-redux';
// import { push } from 'connected-react-router';
import { RegisterThunk } from '../Redux/auth/thunk';
import logotecky from '../website_photo/logotecky.png';


export default function CreateCompany() {
    const dispatch = useDispatch();

    const [name, setName] = useState("")
    const [position, setPosition] = useState("")
    const [contact_number, setContactNumber] = useState("")
    const [contact_person, setContactPerson] = useState("")
    const [location, setLocation] = useState("")
    const [email, setEmail] = useState("")
    const [website, setWebsite] = useState("")
    const [company_background, setCompany_background] = useState("")
    const [validated, setValidated] = useState(false);
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const is_partner = false

    //Submit handle
    const handleSubmit = (event: any) => {
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;
        if (!isNaN(parseInt(name))) {
            alert("Please type in English for your Company name")
            return
        } else if (!isNaN(Number(location))) {
            alert("Location should not just number(s)")
            return
        } else if (!isNaN(Number(company_background))) {
            alert("Company background should not just number(s)")
            return
        }
        if (form.checkValidity() === false) {
            return;
        } else {
            dispatch(RegisterThunk(email, password, name, position, confirmPassword, contact_number, contact_person, location, website, company_background, is_partner))
            setValidated(true)
        }

    };

    return (
        <>
            <div id="logo">
                <img src={logotecky} alt="tecky logo" />
            </div>
            <Container className="create-company">

                <Row className="title">
                    <h2>Company Registration</h2>
                    <Col md="12" className="content">
                        <Form onSubmit={handleSubmit} validated={validated} >


                            <Form.Group as={Col} controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" value={email} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" value={password} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="confirmPassword">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control type="password" value={confirmPassword} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setConfirmPassword(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="name">
                                <Form.Label>Company Name</Form.Label>
                                <Form.Control type="text" value={name} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setName(e.currentTarget.value)} required />
                            </Form.Group>



                            <Form.Group as={Col} controlId="contact_person">
                                <Form.Label>Contact Person</Form.Label>
                                <Form.Control type="text" value={contact_person} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setContactPerson(e.currentTarget.value.replace(/[0-9]/g, ''))} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="position">
                                <Form.Label>Position</Form.Label>
                                <Form.Control type="text" value={position} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPosition(e.currentTarget.value.replace(/[0-9]/g, ''))} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="contact-number">
                                <Form.Label>Contact Number</Form.Label>
                                <Form.Control type="text" value={contact_number} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setContactNumber(e.currentTarget.value.replace(/[^\d]/g, ''))} maxLength={8} pattern="[0-9]*" required />
                            </Form.Group>



                            <Form.Group as={Col} controlId="location">
                                <Form.Label>Location</Form.Label>
                                <Form.Control type="text" value={location} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setLocation(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="backgound">
                                <Form.Label>Company Background</Form.Label>
                                <Form.Control type="text" as="textarea" value={company_background} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setCompany_background(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="website">
                                <Form.Label>Company Website</Form.Label>
                                <Form.Control type="url" value={website} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setWebsite(e.currentTarget.value)} required />
                            </Form.Group>

                            <div className="submit">
                                <Button variant="outline-secondary" type="submit" >
                                    Submit
                            </Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    )
}