import React, { useEffect, useState } from 'react'
import { Doughnut } from 'react-chartjs-2'
import { useDispatch, useSelector } from 'react-redux';
import { fetchCohorts } from '../Redux/cohorts/thunk';
import { RootState } from '../store';
import { fetchStudentAva, fetchStudentNotAva, fetchStudentEmployed } from '../Redux/Statistic/thunk';
import { Col } from 'react-bootstrap';
import { roleObj } from '../variable/variable';
import { Link } from 'react-router-dom';
import { Homepage } from './Homepage';



export default function ChartCohortStatus() {
    const isLoggedIn = localStorage.getItem('token') ? true : false;
    const role = localStorage.getItem("roleId")

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchCohorts())
    }, [dispatch])

    const cohorts = useSelector((state: RootState) => state.cohorts.cohorts)
    const studentsAvailable = useSelector((state: RootState) => state.studentsAvailable.studentsAvailable)
    const studentsNotAvailable = useSelector((state: RootState) => state.studentsNotAvailable.studentsNotAvailable)
    const studentsEmployed = useSelector((state: RootState) => state.studentsEmployed.studentsEmployed)
    const [selectedCohort, setSelectedCohort] = useState('Student')

    const cohort = cohorts[0]?.cohort

    useEffect(() => {
        setSelectedCohort(cohort)
    }, [dispatch, cohort])

    useEffect(() => {
        dispatch(fetchStudentAva(selectedCohort));
        dispatch(fetchStudentNotAva(selectedCohort));
        dispatch(fetchStudentEmployed(selectedCohort));
    }, [dispatch, selectedCohort])



    const studentsLookForJob = studentsAvailable.map((student) => (student.count))
    const studentsNotLookForJob = studentsNotAvailable.map((student) => (student.count))
    const studentsAlreadyEmployed = studentsEmployed.map((student) => (student.count))

    if (studentsLookForJob.find(num => isNaN(num))) {
        return <div>Loading...</div>;
    } else if (studentsNotLookForJob.find(num => isNaN(num))) {
        return <div>Loading...</div>;
    } else if (studentsAlreadyEmployed.find(num => isNaN(num))) {
        return <div>Loading...</div>;
    }

    const options = {
        title: {
            display: true,
            text: `Students' employment status`,
            fontSize: 18
        },
        tooltips: {
            enabled: false
        },
        legend: {
            fontSize: 20,
            labels: {
                fontSize: 13
            }
        }
    }
    return (

        <>
            {isLoggedIn ?
                role === roleObj.staff &&

                <Col>
                    <div>
                        <select onChange={event => setSelectedCohort(event.currentTarget.value)}>
                        {/* <option>Select Cohort</option> */}
                        {cohorts.map((coh, index) => <option key={index}>{coh.cohort}</option>)}
                    </select>

                        <Doughnut data={{

                            labels: [`Looking for jobs ${studentsLookForJob}`,
                            `Not available ${studentsNotLookForJob}`,
                            `Employed ${studentsAlreadyEmployed}`],
                            datasets: [
                                {
                                    label: `Students' status`,
                                    data: [studentsLookForJob,
                                        studentsNotLookForJob,
                                        studentsAlreadyEmployed],
                                    backgroundColor: ['rgba(255,206,86,0.2)', 'rgba(19,247,222,0.2)', 'rgba(255,22,86,0.2)'],
                                }
                            ]
                        }
                        } options={options} />

                    </div>



                </Col>
                :
                <Link to="/">
                    <Homepage />
                </Link>

            }
        </>


    )
}

