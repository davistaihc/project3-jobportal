import React, { useEffect } from 'react'
import { Card, CardDeck } from 'react-bootstrap'
import './JobList.scss'
import { useDispatch, useSelector } from 'react-redux'
import { Job } from '../Redux/Job/state'
import { getAllJobThunk } from '../Redux/Job/thunk'
import { RootState } from '../store'
import Moment from 'react-moment'
import { push } from 'connected-react-router'

export const JobListStaff = () => {
   const dispatch = useDispatch();

   useEffect(() => {
      dispatch(getAllJobThunk())
   }, [dispatch]);

   const jobIds = useSelector((state: RootState) => state.jobs.jobs)
   const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))

   function makeUp(jobHighlight: string | null) {
      const highlight = jobHighlight?.toString().split(/•|\r\n/)
      return (
         highlight?.map((e, index) => (e !== "" &&
            <li key={index}>{e}</li>
         ))
      )
   }

   return (
      <>
         <div id="Listing">
            <div><h5>Job List</h5></div>
            <br />
            <hr></hr>
            <div id="JobList">
               <CardDeck id='cardList'>

                  {jobs.map((job: Job) => job.is_approved === true && job.availability === true && (<Card style={{ cursor: "pointer" }} key={`job_forStaff_${job.id}`} onClick={() => {
                     dispatch(push(`/staff_application/${job.id}`))
                  }} >
                     <Card.Body id='cardBody'>
                        <Card.Title>{job.title}</Card.Title>
                        <Card.Text> {job.name} </Card.Text>
                        {/* <Card.Text> {job.location} </Card.Text> */}
                        {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                        <Card.Text> Requirement: <ul> {makeUp(job.requirement)}</ul> </Card.Text>
                     </Card.Body>
                     <Card.Footer>
                        <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                     </Card.Footer>
                  </Card>))}



               </CardDeck>

            </div >
         </div >
      </>
   )
}