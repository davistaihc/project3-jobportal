import React, { useEffect, useState } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import './ApplicationForm.scss'
import { Modal, Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { sentEmailStaffThunk } from '../Redux/ApplicationJob/thunk';
import { fetchEmailMappingInfo } from '../Redux/StaffApplication/thunk';
import { RootState } from '../store';
import './ApplicationFormStaff.scss'


// interface IApplication {
//    title: string,
//    company: string,
//    resume: FileList | string
// }

export const ApplicationFormStaff = () => {

   const dispatch = useDispatch();

   const [isOpen, setIsOpen] = useState(false);
   const [coverLetter, setCoverLetter] = useState("")
   const [subject, setSubject] = useState<string>("")
   const studentJobMapping = useSelector((state: RootState) => state.studentJobMapping, shallowEqual)



   const match = useRouteMatch<{ jobId?: any }>();
   let selectedJobId = match.params.jobId || 0

   useEffect(() => {
      dispatch(fetchEmailMappingInfo(selectedJobId))
   }, [dispatch, selectedJobId])


   const companyEmail = (studentJobMapping.studentJobMapping.map(application => (application.email)))[0]

   const contactPerson = (studentJobMapping.studentJobMapping.map(application => (application.contact_person)))[0]
   const jobTitle = (studentJobMapping.studentJobMapping.map(application => (application.title)))[0]
   const companyName = (studentJobMapping.studentJobMapping.map(application => (application.name)))[0]


   const showModal = () => setIsOpen(true);
   const hideModal = () => setIsOpen(false);

   const handleSubmit = (event: any) => {
      event.preventDefault()
      event.stopPropagation()
      if (coverLetter.length <= 5) {
         alert("No Blank Content!!!!")
         return
      } else {
         dispatch(sentEmailStaffThunk(coverLetter, selectedJobId, companyEmail,   subject))
         hideModal()
      }
   }

   const onChangeSubject = (e: React.ChangeEvent<HTMLInputElement>) => setSubject(e.currentTarget.value)

   return (
      <div id='applicationForm'>
         <Button variant="outline-info" id="StaffApplicationButton" onClick={() => { showModal(); dispatch(fetchEmailMappingInfo(selectedJobId)) }}>Apply Job</Button>
         {
            <Modal show={isOpen} onHide={hideModal} size="lg" id="form" scrollable >
               <Modal.Header>
                  <Modal.Title>Staff Application Form</Modal.Title> <Button variant="outline-secondary" onClick={hideModal} >Close</Button>
               </Modal.Header>
               <Modal.Body>

                  <Form noValidate onSubmit={handleSubmit}>
                     <Form.Group controlId="CompanyEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                           type="text"
                           name="email"
                           value={companyEmail}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="CompanyEmail">
                        <Form.Label>Contact Person</Form.Label>
                        <Form.Control
                           type="text"
                           name="contactPerson"
                           value={contactPerson}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="Company">
                        <Form.Label>Company</Form.Label>
                        <Form.Control
                           type="text"
                           name="company"
                           value={companyName}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="jobTitle">
                        <Form.Label>Job Title</Form.Label>
                        <Form.Control
                           type="text"
                           name="title"
                           value={jobTitle}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="subject">
                        <Form.Label>Subject</Form.Label>
                        <Form.Control
                           type="text"
                           name="subject"
                           value={subject}
                           onChange={onChangeSubject}
                        />
                     </Form.Group>

                     <Form.Group controlId="coverLetter">
                        <Form.Label>Cover Letter</Form.Label>
                        <Form.Control as="textarea" value={coverLetter} rows={6} onChange={(e) => setCoverLetter(e.currentTarget.value)} />
                     </Form.Group>
                     <div id="formFooterStaff">
                        <div> <Button variant="outline-success" type="submit">Send</Button></div>
                     </div>
                  </Form>
               </Modal.Body>
            </Modal>
         }
      </div>
   );

}
