import React, { useEffect, useState } from 'react'
import 'react-calendar/dist/Calendar.css';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { getStudentProfileThunk, editInfoThunk, updateCoverLetterThunk, deleteResumeThunk, deletePortfolioThunk } from '../Redux/Profile/thunks';
import { RootState } from '../store';
import './Profile.scss'
import { Table, Row, Container, Col, Form, Button } from 'react-bootstrap';
import { UploadResume } from './UploadResume';
import { DownloadFile } from './DownloadFile';
import { UploadPortfolio } from './UploadPortfolio';
import Calendar from 'react-calendar'
import moment from 'moment'
import { push } from 'connected-react-router';



export default function Profile() {
    const dispatch = useDispatch();
    const student = useSelector((state: RootState) => state.student, shallowEqual);
    const [first_name, setFirstName] = useState("")
    const [surname, setSurname] = useState("")
    const [contact_number, setContactNumber] = useState("")
    const [statue, setStatue] = useState("")
    const [salary, setSalary] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [position, setPosition] = useState("")
    const [way, setWay] = useState("")
    const [click, setClick] = useState(false);
    const [onboard_date, setOnboardDate] = useState("")
    const [editorShow, setEditorShow] = useState(false)
    const [Editor, setEditor] = useState("")
    const [date, setDate] = useState(new Date())
    const [validated, setValidated] = useState(false);
    const status = ["Available", "Not Available", "Employed"]
    const resumeName = student.resume_url?.split("/", -1)[4]
    const portfolioName = student.portfolio_url?.split("/", -1)[4]
    const role = localStorage.getItem("roleId")

    //refresh function
    const refresh = () => {
        window.location.reload()
    }

  
    useEffect(() => {
        if (role !== "2") {
            dispatch(push("/"))
            return
        }
        dispatch(getStudentProfileThunk())
    }, [dispatch, student.resume, student.portfolio, role])

    useEffect(() => {
        setFirstName(student.first_name)
        setSurname(student.surname)
        setContactNumber(student.contact_number)
        setStatue(student.status)
        setSalary(student.salary)
        setCompanyName(student.company_name)
        setPosition(student.position)
        setWay(student.way)
        setOnboardDate(student.onboard_date)
        setEditor(student.cover_letter)
    }, [dispatch, student.first_name, student.status, student.contact_number, student.surname, student.salary, student.way, student.company_name, student.position, student.onboard_date, student.cover_letter]);


    //Submit handle
    const handleSubmit = (event: any) => {
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;

        if (contact_number !== "" && contact_number.length <= 7) {
            alert("Please input the right contact number")
            return
        }
        if (form.checkValidity() === false) {
            return;
        }
        if (!click) {
            dispatch(editInfoThunk(student.studentId, first_name, surname, statue, contact_number, salary, companyName, position, way, student.onboard_date))
            setValidated(true)
            return
        } else {
            dispatch(editInfoThunk(student.studentId, first_name, surname, statue, contact_number, salary, companyName, position, way, date.toISOString()))
            setValidated(true)
        }


    };

    const handleShow = () => {
        if (editorShow === false) {
            setEditorShow(true)
        } else {
            setEditorShow(false)
        }
    }

    const handleClick = () => setClick(true)
    const onHideClick = () => setClick(false)

    const handleCoverLetterSubmit = (event: any) => {
        event.preventDefault();
        event.stopPropagation();
        dispatch(updateCoverLetterThunk(student.studentId, Editor))
        setEditorShow(false)
    }
    const onChange = (nextValue: any) => setDate(nextValue)

    const deleteResume = () => {
        dispatch(deleteResumeThunk(student.studentId))
    }
    const deletePortfolio = () => {
        dispatch(deletePortfolioThunk(student.studentId))
    }
    //If loading 
    if (first_name === undefined || surname === undefined || contact_number === undefined || statue === undefined) {
        refresh()
        return (<div id="loading"><div>Loading....</div></div>)
    }
    const onboardDateStr = date.toISOString()



    return (

        <Container className="profile" >
            <Row className="myProfile">
                <h3>My Profile</h3>
                <Col md="12" className="info">


                    <Form onSubmit={handleSubmit} noValidate validated={validated}  >
                        <Form.Row>
                            <Form.Group as={Col} controlId="firstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="text" value={first_name} onChange={(e: any) => setFirstName(e.currentTarget.value)} required />

                            </Form.Group>

                            <Form.Group as={Col} controlId="surname">
                                <Form.Label>Surname</Form.Label>
                                <Form.Control type="text" value={surname} onChange={(e: any) => setSurname(e.currentTarget.value)} />

                            </Form.Group>
                        </Form.Row>

                        <Form.Group controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" readOnly disabled defaultValue={student.email} />
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="contact">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="text" value={contact_number} onChange={e => setContactNumber(e.currentTarget.value.replace(/[^\d]/g, ''))} maxLength={8} minLength={8} pattern="[0-9]*" />

                            </Form.Group>

                            <Form.Group as={Col} controlId="status">
                                <Form.Label>Employment Status</Form.Label>
                                <Form.Control as="select" name="status" onChange={e => setStatue(e.currentTarget.value)}>
                                    {<option>{`${student.status}`}</option>}
                                    {status.map((e ,i) => e !== student.status && <option key={i}>{e}</option>)}
                                </Form.Control>

                            </Form.Group>

                            <Form.Group as={Col} controlId="cohort">
                                <Form.Label>Cohort</Form.Label>
                                <Form.Control readOnly defaultValue={student.cohort} />

                            </Form.Group>
                        </Form.Row>
                        {statue === "Employed" && <Form.Row>
                            <Form.Group as={Col} controlId="salary">

                                <Form.Label>Salary</Form.Label>
                                <Form.Control type="text" value={salary} onChange={e => setSalary(e.currentTarget.value.replace(/[^\d]/g, ''))} maxLength={8} pattern="[0-9]*" />

                            </Form.Group>
                            <Form.Group as={Col} controlId="companyName">
                                <Form.Label>Company</Form.Label>
                                <Form.Control type="text" value={companyName} onChange={e => setCompanyName(e.currentTarget.value)} />

                            </Form.Group>
                            <Form.Group as={Col} controlId="jobTitle">
                                <Form.Label>Job Title</Form.Label>
                                <Form.Control type="text" value={position} onChange={e => setPosition(e.currentTarget.value)} />

                            </Form.Group>
                        </Form.Row>}
                        {statue === "Employed" &&
                           <>
                           <Form.Row>
                                <Form.Group as={Col} controlId="way">
                                    <Form.Label>The way to find job</Form.Label>
                                    <Form.Control type="text" value={way} onChange={e => setWay(e.currentTarget.value)} />

                                </Form.Group>
                                {click && <Calendar onChange={onChange} value={date} calendarType="ISO 8601" className="calender" />}

                                <Form.Group as={Col} controlId="onboard">
                                    <Form.Label>Onboard Date</Form.Label>
                                    {!click && <Form.Control type="text" value={moment(onboard_date).local().format('MMMM Do YYYY')} onClick={handleClick} />}
                                    {click && <Form.Control type="text" value={moment(onboardDateStr).local().format('MMMM Do YYYY')} onClick={onHideClick} />}
                                    
                                </Form.Group>
                          
                            </Form.Row>
                            <p id="remark">Remark: Employment information for Tecky survey only</p> 
                            </>}

                        <div className="submit">
                            <Button variant="outline-info" type="submit" >
                                Update
                                </Button>
                        </div>


                    </Form>


                </Col>
            </Row>
            <Row className="myProfile">
                <h3>My Resume & Portfolio </h3>&nbsp;&nbsp;&nbsp;<UploadResume />&nbsp;&nbsp;<UploadPortfolio />
                <Container>
                    <br></br>
                    <Table striped bordered hover responsive className="profileTable">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>FileName</th>
                                <th>Download</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {student.resume &&
                                <tr>
                                    <td>Resume</td>
                                    <td>{student.resume}</td>
                                    <td><DownloadFile file={resumeName} /></td>
                                    <td><Button variant="outline-danger" onClick={deleteResume}>Delete</Button></td>
                                </tr>}
                            {student.portfolio &&
                                <tr>
                                    <td>Portfolio</td>
                                    <td>{student.portfolio}</td>
                                    <td><DownloadFile file={portfolioName} /></td>
                                    <td><Button variant="outline-danger" onClick={deletePortfolio} >Delete</Button></td>
                                </tr>}

                        </tbody>

                    </Table>
                </Container>
            </Row>

            <Row className="coverLetter">
                <div>
                    <h3>My Cover Letter Template</h3>


                </div>

                <div>
                    <Form onSubmit={handleCoverLetterSubmit}>
                        {!editorShow && <Form.Group controlId="coverLetter">
                            <Form.Control as="textarea" value={Editor} rows={15} onChange={(e) => setEditor(e.currentTarget.value)} readOnly />
                        </Form.Group>}
                        {editorShow && <Form.Group controlId="coverLetter">
                            <Form.Control as="textarea" value={Editor} rows={15} onChange={(e) => setEditor(e.currentTarget.value)} />
                        </Form.Group>}




                        <div className="submit">
                            {!editorShow &&
                                <Button variant="outline-secondary" type="button" onClick={handleShow} >
                                    Edit
                            </Button>}
                            {editorShow &&
                                <Button variant="outline-secondary" type="submit"  >
                                    Submit
                            </Button>
                            }
                        </div>
                    </Form>
                </div>
            </Row>

        </Container>
    )
}
