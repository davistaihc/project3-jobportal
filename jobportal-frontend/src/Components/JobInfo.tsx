import React, { useEffect } from 'react'
import { JobSearch } from './JobSearch'
import { JobList } from './JobList'
import { JobView } from './JobView'
import { Row, Col } from 'react-bootstrap'
import "./JobInfo.scss"
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { push } from 'connected-react-router'
import { roleObj } from '../variable/variable'

export const JobInfo = (props: RouteComponentProps<{ id: string }>) => {
   const dispatch = useDispatch()
   const role = localStorage.getItem("roleId")

   useEffect(()=>{
      if(role === roleObj.other){
         dispatch(push("/company/dashboard"))
         return
      }
   },[dispatch,role])

   return (
      <>

         <div id='JobInfo'>
            <Row id="search">
               <Col md="12"><JobSearch /></Col>
            </Row>
            <Row id="body" >
               <Col md="4"><div id="content"><JobList
                  match={props.match}
                  location={props.location}
                  history={props.history}
               /></div></Col>
               <Col md="8"><div id="view"><JobView /></div></Col>
            </Row>

         </div>


      </>
   )
}