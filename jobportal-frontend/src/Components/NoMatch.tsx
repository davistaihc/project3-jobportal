import React from 'react'
import noMatchPhoto from '../website_photo/noMatchPhoto.jpg'
import './NoMatch.scss'


export const NoMatch = () =>{

    return (
        <>
            <div id="noMatch">
                <div><img src={noMatchPhoto} alt="404_Error"/></div>
            </div>

        
        </>
    )

}