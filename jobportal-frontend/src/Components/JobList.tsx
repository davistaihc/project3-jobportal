import React, { useEffect, useState } from 'react'
import { Card, CardDeck, Button } from 'react-bootstrap'
import './JobList.scss'
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { Job } from '../Redux/Job/state'
import { setSelectedViewJob } from '../Redux/Job/actions'
import { getAllJobThunk, searchJobThunk } from '../Redux/Job/thunk'
import { RootState } from '../store'
import Moment from 'react-moment'
import { RouteComponentProps } from 'react-router-dom'
import { roleObj } from '../variable/variable'

export const JobList = (props: RouteComponentProps) => {
   const dispatch = useDispatch();
   const role = localStorage.getItem("roleId")

   const keyword = new URLSearchParams(props.location.search).get("keyword" || " ");

   const jobIds = useSelector((state: RootState) => state.jobs.jobs)
   const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))
   const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual);

   useEffect(() => {
      if (keyword === null) {
         dispatch(getAllJobThunk());
      } else {
         dispatch(searchJobThunk(keyword as string));
      }
   }, [dispatch, keyword, selectedViewJob])

   function makeUp(jobHighlight: string | null) {
      const highlight = jobHighlight?.toString().split(/•|\r\n/)
      return (
         highlight?.map(e => (e !== "" &&
            <li>{e}</li>
         ))
      )
   }

   const [showPending, setShowPending] = useState(true)
   const [showA, setShowA] = useState<boolean>(false)

   const show = () => {
      setShowPending(!showPending)
   }
   const showAV = () => setShowA(!showA)


   const result = jobs.length

   return (
      <>
         {result === 0 ?
            <h1>No Result Found</h1>
            :
            role === roleObj.staff ?
               <div id="JobList">
                  {showA === false && showPending &&
                     <Button onClick={show} variant="outline-info">Show Approved Jobs</Button>
                  }
                  {showA === false && !showPending && <Button onClick={show} variant="outline-info">Show Pending jobs</Button>
                  }
                  <>&nbsp;&nbsp;&nbsp;</>
                  {
                     showA ?
                        <Button onClick={showAV} variant="outline-info" >Show All Jobs</Button>
                        :
                        <Button onClick={showAV} variant="outline-info" >Show Archived Jobs</Button>
                  }


                  <hr></hr>

                  <CardDeck id='cardList' style={{ cursor: "pointer" }}>
                     <div>
                        {showPending && !showA && jobs.map((job: Job) => job.is_approved === false && job.availability === true && (<Card key={`job_${job.id}`} onClick={() => dispatch(setSelectedViewJob(job))}>
                           <Card.Body id='cardBody' style={{ background: "rgb(176, 176, 176)" }}>
                              <Card.Title>{job.title}</Card.Title>
                              <Card.Text> {job.name} </Card.Text>
                              <Card.Text> {job.location} </Card.Text>
                              {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                              {job.salary && <Card.Text> {job.salary} </Card.Text>}
                           </Card.Body>
                           <Card.Footer>
                              <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                              <small>{job.availability}</small>
                           </Card.Footer>
                        </Card>))}
                     </div>

                     <div>
                        {!showPending && !showA && jobs.map((job: Job) => job.is_approved === true && job.availability === true && (<Card key={`job_${job.id}`} onClick={() => dispatch(setSelectedViewJob(job))}>
                           <Card.Body id='cardBody'>
                              <Card.Title>{job.title}</Card.Title>
                              <Card.Text> {job.name} </Card.Text>
                              <Card.Text> {job.location} </Card.Text>
                              {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                              {job.salary && <Card.Text> {job.salary} </Card.Text>}
                           </Card.Body>
                           <Card.Footer>
                              <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                              <small>{job.availability}</small>
                           </Card.Footer>
                        </Card>))}
                        {showA && jobs.map((job: Job) => job.availability === false && (<Card key={`job_${job.id}`} onClick={() => dispatch(setSelectedViewJob(job))}>
                           <Card.Body id='cardBody' className="archived">
                              <Card.Title>{job.title}</Card.Title>
                              <Card.Text> {job.name} </Card.Text>
                              <Card.Text> {job.location} </Card.Text>
                              {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                              {job.salary && <Card.Text> {job.salary} </Card.Text>}
                           </Card.Body>
                           <Card.Footer>
                              <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                              <small>{job.availability}</small>
                           </Card.Footer>
                        </Card>))}

                     </div>
                  </CardDeck>
               </div >
               :
               <div id="JobList">
                  <CardDeck id='cardList'>
                     {jobs.map((job: Job) => (job.is_approved === true && job.availability === true && <Card key={`job_${job.id}`} onClick={() => dispatch(setSelectedViewJob(job))}>
                        <Card.Body id='cardBody'>
                           <Card.Title>{job.title}</Card.Title>
                           <Card.Text> {job.name} </Card.Text>
                           <Card.Text> {job.location} </Card.Text>
                           {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                           {job.salary && <Card.Text> {job.salary} </Card.Text>}
                        </Card.Body>
                        <Card.Footer>
                           <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                        </Card.Footer>
                     </Card>))}
                  </CardDeck>
               </div >
         }



      </>
   )
}