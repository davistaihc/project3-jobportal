import React from 'react'
import jobs from '../website_photo/jobs.jpg'
import { ApplicationForm } from './ApplicationsForm'
import './JobView.scss'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { RootState } from '../store'
import Moment from 'react-moment'
import { roleObj } from '../variable/variable'
import { Button } from 'react-bootstrap'
import { updateJobStatusThunk, deleteJobThunk } from '../Redux/Job/thunk'

export const JobView = () => {
   const dispatch = useDispatch();

   const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual);
   const role = localStorage.getItem("roleId");


   function makeUp(data: string | null) {
      const highlight = data?.toString().split(/•|\r\n/)
      return (
         highlight?.map(e => (e !== "" &&
            <li>{e}</li>
         ))
      )
   }

   let jobId = 0
   if (selectedViewJob?.id !== undefined) {
      jobId = selectedViewJob?.id
   }

   const approvedButton = () => {
      dispatch(updateJobStatusThunk(jobId, !selectedViewJob?.is_approved));
   }


   const deleteJob = () => {
      if (selectedViewJob && selectedViewJob.id !== undefined) {
         dispatch(deleteJobThunk(selectedViewJob.id))
      }
   }
   return (
      <>
         {
            selectedViewJob && selectedViewJob.id > 0 &&
            role === roleObj.staff &&
            <div id="jobView">
               <div id='body'>
                  <div id="header">
                     {!selectedViewJob.is_approved &&
                        <Button onClick={approvedButton} variant="outline-success">Approve</Button>}
                     {selectedViewJob.is_approved &&
                        <Button onClick={approvedButton} variant="outline-secondary">Undo-Approve</Button>}
                     <>&nbsp;&nbsp;&nbsp;</>

                     <Button onClick={deleteJob} variant="outline-danger">Archive</Button>

                  </div>
                  <div> <hr /></div>

                  <div id="jobInfo">
                     <div id='companyLogo'></div>
                     <div id='jobTitle'>{selectedViewJob.title}</div>
                     <div id='companyName'>{selectedViewJob.name}</div>
                        Job location:
                        <div id='location'>{selectedViewJob.location}</div>
                        Salary:
                        <div id='salary'>{selectedViewJob.salary}</div>
                     <div id='post' className="text-muted"><Moment toNow>{selectedViewJob.created_at}</Moment></div>
                  </div>

                  <div id="jobHighlight">
                     <div id='title'>Highlight</div>
                     <div id='highlight'><ul>{makeUp(selectedViewJob.highlight)}</ul></div>
                     <br />
                     <div id='title'>Responsibility</div>
                     <div id='responsibility'><ul>{makeUp(selectedViewJob.responsibility)}</ul></div>
                     <br />
                     <div id='title'>Requirement</div>
                     <div id='requirement'><ul>{makeUp(selectedViewJob.requirement)}</ul></div>
                     <br />
                     <div id='title'>Employment type</div>
                     <div id='employment'>{selectedViewJob.type}</div>

                  </div>
               </div>
            </div>}

         {selectedViewJob && selectedViewJob.id > 0 && role === roleObj.student &&
            <div id="jobView">

               <div id='body'>
                  <div id="header"><ApplicationForm /></div>
                  <div> <hr /></div>

                  <div id="jobInfo">
                     <div id='companyLogo'></div>
                     <div id='jobTitle'>{selectedViewJob.title}</div>
                     <div id='companyName'>{selectedViewJob.name}</div>
                     Job location:
                     <div id='location'>{selectedViewJob.location}</div>
                     Salary:
                     <div id='salary'>{selectedViewJob.salary}</div>
                     <div id='post' className="text-muted"><Moment toNow>{selectedViewJob.created_at}</Moment></div>
                  </div>

                  <div id="jobHighlight">
                     <div id='title'>Highlight</div>
                     <div id='highlight'><ul>{makeUp(selectedViewJob.highlight)}</ul></div>
                     <br />
                     <div id='title'>Responsibility</div>
                     <div id='responsibility'><ul>{makeUp(selectedViewJob.responsibility)}</ul></div>
                     <br />
                     <div id='title'>Requirement</div>
                     <div id='requirement'><ul>{makeUp(selectedViewJob.requirement)}</ul></div>
                     <br />
                     <div id='title'>Employment type</div>
                     <div id='employment'>{selectedViewJob.type}</div>

                  </div>
               </div>
            </div>
         }
         {selectedViewJob && selectedViewJob.id === undefined && <div id="jobViewEmpty">
            <div>Select a job to view details</div>
            <div><img src={jobs} alt="findJobPhoto" /></div>

         </div>}

      </>
   )
}