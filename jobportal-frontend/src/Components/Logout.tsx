import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { Button } from "react-bootstrap";
import { RootState } from "../store";
import { logoutThunk } from "../Redux/auth/thunk";

export default function Logout() {
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const dispatch = useDispatch();

    const clickLogout = () => {
        dispatch(logoutThunk());
    }

    return (
        <div className="logout-bar">
            {
                isAuthenticated ?
                    <Button variant="outline-secondary" onClick={clickLogout}>Logout</Button> :
                    ""
            }
        </div>
    );
}