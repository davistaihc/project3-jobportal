import React, { useEffect, useState } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import './ApplicationForm.scss'
import { Modal, Button, Form } from 'react-bootstrap';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { getStudentProfileThunk } from '../Redux/Profile/thunks';
import { sentEmailByStudentThunk } from '../Redux/ApplicationJob/thunk';




export const ApplicationForm = () => {
   const dispatch = useDispatch();
   const [isOpen, setIsOpen] = useState<boolean>(false);
   const [title, setTitle] = useState<string>("Loading");
   const [company, setCompany] = useState<string>("Loading");
   const [coverLetter, setCoverLetter] = useState<string>("Loading")
   const [subject, setSubject] = useState<string>("")
   const [portfolio_url, setPortfolio_url] = useState<string>("")
   const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual);
   const student = useSelector((state: RootState) => state.student, shallowEqual)
   const [confirm, setConfirm] = useState<boolean>(false)
   const resumeName = student.resume_url?.split("/", -1)[4]
   const portfolioName = student.portfolio_url?.split("/", -1)[4]


   // let id:number = 0

   // if (selectedViewJob && selectedViewJob.id !== undefined  ) {
   //    id = selectedViewJob?.id
   // }
   const showModal = () => { student.resume ? setIsOpen(true) : alert("Please Upload Resume First!") };
   const hideModal = () => setIsOpen(false);

   const handleConfirm = (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()
      event.stopPropagation()
      if (resumeName !== undefined && selectedViewJob && selectedViewJob.id > 0 && student.resume_url !== undefined && student.resume) {
         dispatch(sentEmailByStudentThunk(student.studentId, selectedViewJob.id, resumeName, subject, portfolioName, portfolio_url, coverLetter, student.resume_url, student.resume))
         setConfirm(false)
         hideModal()
      }
   }

   const handleSubmit = (event: any) => {
      event.preventDefault()
      event.stopPropagation()
      if (!student.resume) {
         alert("Please Upload Resume First")
         return
      }
      if(coverLetter.length < 50 || coverLetter.includes("fuck")){
         alert("Please Write The Cover Letter Seriously")
         return
      }
      if (selectedViewJob !== undefined && selectedViewJob.id <= 0) {
         alert("Choose Job First!")
         return
      }
      setConfirm(true)

   }



   const onChangeSubject = (e: React.ChangeEvent<HTMLInputElement>) => setSubject(e.currentTarget.value)
   const onChangePortfolio_url = (e: React.ChangeEvent<HTMLInputElement>) => setPortfolio_url(e.currentTarget.value)

   useEffect(() => {
      dispatch(getStudentProfileThunk())
      if (selectedViewJob) {
         setTitle(selectedViewJob.title)
         setCompany(selectedViewJob.name)
         if (student.cover_letter !== null) {
            setCoverLetter(`${student.cover_letter}`)
         }
         setSubject(`Application for the position : ${selectedViewJob.title}`)
      }
   }, [dispatch, student.cover_letter, selectedViewJob, student.first_name])


   function makeUp(data: string | null) {
      const highlight = data?.toString().split(/•|\r\n/)
      return (
         highlight?.map(e => (e !== "" &&
            <li>{e}</li>
         ))
      )
   }



   return (
      <div id='applicationForm'>
         {selectedViewJob && selectedViewJob.id <= 0 && <Button variant="outline-success">Apply Job</Button>}
         {selectedViewJob && selectedViewJob.id > 0 && <Button variant="outline-success" onClick={showModal} >Apply Job</Button>}
         {selectedViewJob &&
            <Modal show={isOpen} onHide={hideModal} size="lg" id="form" scrollable >
               <Modal.Header>
                  <Modal.Title>Application Form</Modal.Title>
               </Modal.Header>
               <Modal.Body>

                  <Form noValidate onSubmit={handleSubmit}>

                     <Form.Group controlId="jobTitle">
                        <Form.Label>Job Title</Form.Label>
                        <Form.Control
                           type="text"
                           name="title"
                           value={title}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="companyName">
                        <Form.Label>Company</Form.Label>
                        <Form.Control
                           type="text"
                           name="company"
                           value={company}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     <Form.Group controlId="jobDescription">
                        <Form.Label>Job Description</Form.Label>
                        <ul>
                           {makeUp(selectedViewJob.responsibility)}
                        </ul>
                     </Form.Group>  <Form.Group controlId="resume">
                        <Form.Label>Resume</Form.Label>
                        <Form.Control
                           type="text"
                           name="resume"
                           value={student.resume}
                           readOnly
                           plaintext
                        />
                     </Form.Group>
                     {student.portfolio && <Form.Group controlId="portfolio">
                        <Form.Label>Portfolio</Form.Label>
                        <Form.Control
                           type="text"
                           name="portfolio"
                           value={student.portfolio}
                           readOnly
                           plaintext
                        />
                     </Form.Group>}
                     {!student.portfolio && <Form.Group controlId="portfolio">
                        <Form.Label>Portfolio Link</Form.Label>
                        <Form.Text>Please input the link of portfolio.</Form.Text>
                        <Form.Control
                           type="text"
                           name="portfolio_url"
                           value={portfolio_url}
                           onChange={onChangePortfolio_url}
                           placeholder="http://example.com/portfolio.ppt"
                        />
                     </Form.Group>}
                     <Form.Group controlId="subject">
                        <Form.Label>Subject</Form.Label>
                        <Form.Control
                           type="text"
                           name="subject"
                           onChange={onChangeSubject}
                           value={subject}
                        />
                     </Form.Group>
                     <Form.Group controlId="coverLetter">
                        <Form.Label>Cover Letter</Form.Label>
                        <Form.Control as="textarea" value={coverLetter} rows={15} onChange={(e) => setCoverLetter(e.currentTarget.value)} />
                     </Form.Group>
                     <div id="formFooter">
                        <div> <Button variant="outline-primary" type="submit">Submit</Button></div>
                        <div> <Button variant="outline-secondary" onClick={hideModal} >Close</Button></div>
                     </div>
                  </Form>
               </Modal.Body>
            </Modal>
         }
         <Modal show={confirm} onHide={() => setConfirm(false)} >
            <Form onSubmit={handleConfirm}>
               <Modal.Header closeButton>
                  {/* <Modal.Title>Confirmation</Modal.Title> */}
               </Modal.Header>
               <Modal.Body>Confirm to send the application?</Modal.Body>
               <Modal.Footer>

                  <Button type="submit" variant="outline-success">
                     Confirm
             </Button>
                  <Button variant="outline-secondary" onClick={() => setConfirm(false)}>
                     Close
             </Button>
               </Modal.Footer>
            </Form>
         </Modal>
      </div>
   );
}