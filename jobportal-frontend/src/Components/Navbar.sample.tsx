import React from 'react';
import { Navbar, Nav, Icon } from 'rsuite'
import './NavBar.scss'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';

export const NavBarSample = () => {
   const expanded = useSelector((state: RootState) => state.sideBar.expanded)
   const dispatch = useDispatch();
   const handleShow = () => {
      dispatch({
         type: "@@SIDEBAR_EXPANDED" as "@@SIDEBAR_EXPANDED",
         expanded
      })
   }

   return (
      <Navbar componentClass="nav" id="navbar">
         <Navbar.Header id="header">
            <a href="#" className="navbar-brand logo" onClick={handleShow} id="icon" ><Icon icon="bars" size="lg" /></a>
         </Navbar.Header>
         <Navbar.Body>
            <Nav>
            </Nav>
            <Nav pullRight>
               <Nav.Item icon={<Icon icon="off" />} id=" logout" >Logout</Nav.Item>
            </Nav>
         </Navbar.Body>
      </Navbar>
   )

}