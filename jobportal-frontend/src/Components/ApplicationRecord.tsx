import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Table, Modal, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { RootState } from "../store";
import Moment from 'react-moment';
import "@fortawesome/free-brands-svg-icons";
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { studentApplicationByUserIdThunk, UpdateApplicationStatusThunk } from "../Redux/ApplicationRecord/thunks"
import { Homepage } from "./Homepage";
import { setSelectedViewApp } from "../Redux/ApplicationRecord/actions";
import "./ApplicationRecord.scss";
import { roleObj } from "../variable/variable";
import { push } from "connected-react-router";



export default function ApplicationRecord() {
  
  const isLoggedIn = localStorage.getItem('token') ? true : false;
  const dispatch = useDispatch();
  const role = localStorage.getItem("roleId")
  const [showAll, setShowAll] = useState(false)
  const handleShowAll = () => setShowAll(true)
  const handleShowFour = () => setShowAll(false)
  const [status, setStatus] = useState<string>()
  const _status = ["Waiting for response", "Got interview", "Accept offer", "Reject offer"]
  const SelectedViewApp = useSelector((state: RootState) => state.studentApplications.selectedViewApplication, shallowEqual);
  

  // const handleHide = () => setHide(true)
  


  const handleSubmit = () => {
    let statusId: number;
    switch (status) {
      case "Waiting for response":
        statusId = 1
        break;
      case "Got interview":
        statusId = 2
        break;
      case "Accept offer":
        statusId = 3
        break;
      case "Reject offer":
        statusId = 4
        break;
      default:
        statusId = 0
        break;
    }
    dispatch(UpdateApplicationStatusThunk(statusId))
    setModalShow(false)
  }

  const studentApplications = useSelector((state: RootState) =>
    state.studentApplications.studentApplications);

  useEffect(() => {
    if (role === "3") {
      dispatch(push("/company/dashboard"))
    }
    dispatch(studentApplicationByUserIdThunk())
    setStatus(SelectedViewApp?.status)
  }, [dispatch, role ,SelectedViewApp]);

  const [modalShow, setModalShow] = React.useState(false);

  function MyVerticallyCenteredModal(props: any) {


    function makeUp(SelectedViewApp: string | null | undefined) {
      const highlight = SelectedViewApp?.toString().split(/•|\r\n/)
      return (
        highlight?.map(e => (e !== "" &&
          <li>{e}</li>
        ))
      )
    }

    // function makeUpNoDot(SelectedViewApp: string | null | undefined) {
    //   const highlight = SelectedViewApp?.toString().split(/•|\r\n/)
    //   return (
    //     highlight?.map(e => (e !== "" &&
    //       <div>{e}</div>
    //     ))
    //   )
    // }

    return (
      <Modal
        {...props}
        // onHide={hide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {SelectedViewApp?.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <h4>{SelectedViewApp?.title}</h4>
          <br></br>
          <Form onSubmit={handleSubmit} noValidate>
            <Form.Label>Job Status <br /><small>You can update status here.</small></Form.Label>
            <Form.Control as="select" name="status" onChange={e => setStatus(e.currentTarget.value)}>

              {/* {setStatus(SelectedViewApp?.status)} */}
              {<option>{status}</option>}
              {_status.map((e: string) => e !== status && <option>{e}</option>)}
            </Form.Control>
          </Form>
          <br />
          <p>
            Job location:<br></br>
            {SelectedViewApp?.location}<br></br><br></br>

            Job Responsibility:<br></br>
            <ul> {makeUp(SelectedViewApp?.responsibility)}</ul>
            <br></br>
            Job Requirement:<br></br>
            <ul> {makeUp(SelectedViewApp?.requirement)}</ul>
            <br></br>
            Salary:<br></br>
            {SelectedViewApp?.salary}<br></br><br></br>
            <hr />
            Cover Letter:<br></br>
            <br />
            {<pre id="coverLetter">{SelectedViewApp?.content}</pre>}<br></br>
            Resume:<br></br>
            {SelectedViewApp?.submitted_resume}<br></br><br></br>
            Application sent on:<br></br>
            <Moment format="YYYY/MM/DD">{SelectedViewApp?.created_at}</Moment><br></br><br></br>
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="outline-secondary" type="submit"  onClick={handleSubmit}>Submit</Button>
          <Button variant="outline-secondary" onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal >
    );
  }

  return (
    <>
      {isLoggedIn ?
        role === roleObj.staff ?
          <>
            <div>
              <br />
              <br />
              <br />
              <br />
              <div className="container">
                <div className="app-header">
                  <h2>Application History</h2>
                  <div>
                    {showAll && <Button variant="outline-secondary" onClick={handleShowFour}>Hide</Button>}
                    {!showAll && <Button variant="outline-secondary" onClick={handleShowAll}>Show All</Button>}
                  </div>
                </div>
                <hr></hr>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>Cohort</th>
                      <th>Student Name</th>
                      <th>Position</th>
                      <th>Company</th>
                      <th>Date Applied</th>
                    </tr>
                  </thead>
                  <tbody>
                    {!showAll && studentApplications.map((studentApp, i) => (i < 10 &&
                      <tr key={studentApp.Id} >
                        <td className="cohort">{studentApp.cohort}</td>
                        <td>{studentApp.first_name}<>&nbsp;</>{studentApp.surname}</td>
                        <td>{studentApp.title}</td>
                        <td>{studentApp.name}</td>
                        <td><Moment format="YYYY/MM/DD">{studentApp.created_at}</Moment></td>
                      </tr>
                    ))}
                    {showAll && studentApplications.map((studentApp, i) => (
                      <tr key={studentApp.Id} >
                        <td className="cohort">{studentApp.cohort}</td>
                        <td>{studentApp.first_name}<>&nbsp;</>{studentApp.surname}</td>
                        <td>{studentApp.title}</td>
                        <td>{studentApp.name}</td>
                        <td><Moment format="YYYY/MM/DD">{studentApp.created_at}</Moment></td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div >
          </>
          :
          <div>
            <br />
            <br />
            <br />
            <br />
            <div className="container">
              <h2>Application History</h2>
              <hr></hr>
              <Table striped bordered hover responsive>
                <thead>
                  <tr>
                    <th>Position</th>
                    <th>Company</th>
                    <th>Date Applied</th>
                    <th>Details / Resume</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  {studentApplications.map((studentApp, i) => (
                    <tr key={`${studentApp.Id}${i}`} >
                      <td>{studentApp.title}</td>
                      <td>{studentApp.name}</td>
                      <td><Moment format="YYYY/MM/DD">{studentApp.created_at}</Moment></td>
                      <td><div onClick={() => { setModalShow(true); dispatch(setSelectedViewApp(studentApp)) }} className="applicationDetail" key={`${studentApp.Id}`}><FontAwesomeIcon icon={faSearch} style={{ cursor: "pointer" }} /></div>
                        {i < 1 && <MyVerticallyCenteredModal
                          show={modalShow}
                          onHide={() => setModalShow(false)}
                        />}
                      </td>
                      <td key={i}>{studentApp.status}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </div > :
        <Link to="/">
          <Homepage />
        </Link>
      }
    </>
  );
}
