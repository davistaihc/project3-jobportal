import React, { useEffect } from 'react';
import { useRouteMatch } from 'react-router';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { RootState } from '../store';
import { fetchEmailMapping, RemoveEmailMapping } from '../Redux/EmailMapping/thunk';
import { Table } from 'react-bootstrap';
import './EmailMapping.scss'
import { ApplicationFormStaff } from './ApplicationsFormStaff';
import { fetchEmailMappingInfo } from '../Redux/StaffApplication/thunk';

export function EmailMapping() {
    const match = useRouteMatch<{ jobId?: any }>();
    const dispatch = useDispatch();
    const jobId = match.params.jobId
    const selectedStudentIds = useSelector((state: RootState) => jobId == null ? undefined : state.emailMapping.studentsByJobId[jobId])
    const selectedStudents = useSelector((state: RootState) => {
        return selectedStudentIds?.map(id => state.students.studentsById[id]).filter(a => typeof a !== 'undefined')
    })
    const studentJobMapping = useSelector((state: RootState) => state.studentJobMapping, shallowEqual)

    useEffect(() => {
        if (jobId == null) {
            return;
        }
        dispatch(fetchEmailMapping(parseInt(jobId)))
        dispatch(fetchEmailMappingInfo(jobId))
    }, [dispatch, jobId]);

    return (
        <><div className="selectedStudentTable"><div className="selectedStudents">
            <div><h5>Selected Students</h5></div>
            <div id="header"><ApplicationFormStaff /></div>
            {/* <hr></hr> */}
        </div>
            <hr></hr>

            {selectedStudents ?
                <Table striped bordered hover id="table" >
                    <thead>
                        <tr><th>Name</th>
                            <th>cohort</th>
                            <th>Application status</th>
                        </tr>
                    </thead>
                    <tbody>

                        {/* {selectedStudents?.map((studentList, index) => (
                            <tr key={index}>
                                <th>
                                    {studentList.first_name} {studentList.surname}
                                </th>
                                <th>
                                    {studentList.cohort}
                                </th>
                                <th>
                                    {studentList.app_sent_success && <span>Application Send </span>}
                                    {!studentList.app_sent_success && <span>Application Not Send </span>}

                                </th>
                            </tr>
                        ))} */}
                        {studentJobMapping.studentJobMapping?.map((studentList, index) => (
                            <tr style={{ cursor: "pointer" }} key={`student_mapping_${index}`} onClick={async () =>
                                dispatch(RemoveEmailMapping(parseInt(jobId), studentList.student_id))}>
                                <th>
                                    {studentList.first_name} {studentList.surname}
                                </th>
                                <th>
                                    {studentList.cohort}
                                </th>
                                <th>
                                    {studentList.app_sent_success && <span> Sent </span>}
                                    {!studentList.app_sent_success && <span>Not Send </span>}

                                </th>
                            </tr>
                        ))}

                    </tbody>

                    {/* {selectedStudents?.map((x) => (x.surname))} */}
                </Table>
                :
                <div id="SelectedViewEmpty">

                    <br></br>
                    <br></br>
                    <div><h5> Select a job, then select potential students.</h5><br />
                    </div>
                    <div><h5> Click "Apply Job" to send students' information to employer.</h5> </div>
                </div>
            }

        </div>

        </>
    )
}