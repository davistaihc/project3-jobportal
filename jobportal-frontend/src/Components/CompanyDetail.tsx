import React, { useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { RootState } from '../store';
import { Table, Card } from 'react-bootstrap';
import { getAllJobThunk } from '../Redux/Job/thunk';
import { Job } from '../Redux/Job/state';
import Moment from 'react-moment';
import { roleObj } from '../variable/variable';
import { push } from 'connected-react-router';

export const CompanyDetail = (props: RouteComponentProps<{ id: string }>) => {

    const dispatch = useDispatch();
    const companyId = Number(props.match.params.id)



    const selectedViewCompany = useSelector((state: RootState) => state.company.selectedViewCompany, shallowEqual);
    const jobIds = useSelector((state: RootState) => state.jobs.jobs)
    const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))
    const role = localStorage.getItem("roleId")
    useEffect(() => {
        if(role !== roleObj.staff){
            dispatch(push("/"))
        }
        dispatch(getAllJobThunk());
    }, [dispatch, companyId ,role]);


    function makeUp(jobHighlight: string | null) {
        const highlight = jobHighlight?.toString().split(/•|\r\n/)
        return (
            highlight?.map(e => (e !== "" &&
                <li>{e}</li>
            ))
        )
    }


    return (
        <>
            {
                selectedViewCompany &&
                <div className="container">
                    <h2>Company Contact</h2>
                    <hr></hr>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Contact Person</th>
                                <th>Contact Contact</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{selectedViewCompany?.name}</td>
                                <td>{selectedViewCompany?.contact_person}</td>
                                <td>{selectedViewCompany?.contact_number}</td>
                                <td><a href=" " onClick={() => window.location.href = `mailto:${selectedViewCompany?.email}`}>{selectedViewCompany?.email}</a></td>
                            </tr>
                        </tbody>
                    </Table>
                    <hr></hr>
                    <div>
                        <h2>Job</h2>
                    </div>
                    <hr></hr>
                    <Table striped bordered hover>
                        <tbody>
                            {jobs.map((job: Job) => (selectedViewCompany.id === job.companyId && <Card key={`job_${job.id}`}>
                                <Card.Body id='cardBody'>
                                    <Card.Title>{job.title}</Card.Title>
                                    <Card.Text> {job.name} </Card.Text>
                                    <Card.Text> {job.location} </Card.Text>
                                    {job.highlight !== "" && <Card.Text> <ul> {makeUp(job.highlight)} </ul></Card.Text>}
                                    {job.salary && <Card.Text> {job.salary} </Card.Text>}
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted"><Moment toNow>{job.created_at}</Moment></small>
                                </Card.Footer>
                            </Card>))}
                        </tbody>
                    </Table>
                </div >
            }
        </>
    )
}
