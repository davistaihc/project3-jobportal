import React from 'react'
import { StudentList } from './StudentList'
import { EmailMapping } from './EmailMapping'
import "./StaffApplicationMapping.scss";
import { Route, Link } from 'react-router-dom';
import { JobListStaff } from './JobListStaff';
import { Homepage } from './Homepage';
import { roleObj } from '../variable/variable';


export default function StaffApplicationMapping() {
    const isLoggedIn = localStorage.getItem('token') ? true : false;
    const role = localStorage.getItem("roleId")

    return (
        <>
            {isLoggedIn ?
                role === roleObj.staff &&


                <div className="staffApplication" >
                    <Route path="/staff_application/:jobId?">
                        <JobListStaff />
                        <EmailMapping />
                        <StudentList />
                    </Route>
                </div>
                :
                <Link to="/">
                    <Homepage />
                </Link>

            }
        </>
    )
}
