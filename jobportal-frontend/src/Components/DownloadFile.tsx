import React, { useState, useEffect } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux';
import { getStudentProfileThunk, downloadFileThunk } from '../Redux/Profile/thunks';

interface IProp {
    file?: string
}


export const DownloadFile = (prop: IProp) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false)
    const file = prop.file
    
    const handleDownload = (event:any) => {
        event.preventDefault();
        event.stopPropagation();
        try {
            setLoading(true)
            if (file) {
                dispatch(downloadFileThunk(file));
                setLoading(false)
            }

        } catch (e) {
            // console.log(e)
            alert("File download fail! Try again !")
        }
    }

    useEffect(() => {
        dispatch(getStudentProfileThunk())
    }, [dispatch]);

    return (
        <>
            {!loading && <Button variant="outline-primary" onClick={handleDownload} >Download</Button>}
            {loading && <Button variant="outline-primary" disabled >Loading</Button>}
        </>
    )
}