import React from 'react';
import logotecky from '../website_photo/logotecky.png';
import Navbar from 'react-bootstrap/Navbar';
import { NavDropdown, Nav, Form, Image } from 'react-bootstrap';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { push } from 'connected-react-router';
import { RootState } from '../store';
import './NavBar.scss'
import Logout from './Logout';
import { roleObj } from '../variable/variable';


export const NavBar = () => {

   const isLoggedIn = useSelector((state: RootState) => state.auth.token, shallowEqual);
   const dispatch = useDispatch();
   const role = useSelector((state: RootState) => state.auth.roleId) // DO NOT USE LOCALSTORAGE HERE



   const DashBoard = React.useCallback(() => {
      dispatch(push('/frontpage'))
   }, [dispatch])


   const profile = () => {
      dispatch(push(`/current/profile`));
   }

   const ApplicationRecord = () => {
      dispatch(push('/application_record'));
   }

   const Job = () => {
      dispatch(push('/job'))
   }

   const Company = () => {
      dispatch(push('/company'))
   }


   const StudentListStaff = () => {
      dispatch(push('/student_list'))
   }

   const StaffApplicationMapping = () => {
      dispatch(push('/staff_application'))
   }


   return (
      <>
         {isLoggedIn && role != null &&
            role.toString() === roleObj.staff &&
            <Navbar expand="lg">
               <Navbar.Brand><Image src={logotecky} fluid /></Navbar.Brand>
               <Navbar.Toggle aria-controls="basic-navbar-nav" />
               <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                     <Nav.Link onClick={DashBoard}>Home</Nav.Link>
                     <Nav.Link onClick={StudentListStaff}>Student Status</Nav.Link>
                     <Nav.Link onClick={Company}>Employers</Nav.Link>
                     <NavDropdown title="Job" id="basic-nav-dropdown">
                        <NavDropdown.Item onClick={Job}>Approve Jobs</NavDropdown.Item>
                        {/* <NavDropdown.Item disabled>Create Jobs (Coming Soon)</NavDropdown.Item> */}
                        <NavDropdown.Item onClick={StaffApplicationMapping}>Match Jobs & Graduates</NavDropdown.Item>
                        <NavDropdown.Item onClick={ApplicationRecord}>Application History</NavDropdown.Item>
                     </NavDropdown>
                  </Nav>
                  <Form inline>
                     <Logout />
                  </Form>
               </Navbar.Collapse>
            </Navbar>

         }
         {isLoggedIn &&
            role?.toString() === roleObj.student &&

            <Navbar expand="lg">
               <Navbar.Brand><Image src={logotecky} fluid /></Navbar.Brand>
               <Navbar.Toggle aria-controls="basic-navbar-nav" />
               <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                     <Nav.Link onClick={DashBoard}>Home</Nav.Link>
                     <Nav.Link onClick={profile}>Profile</Nav.Link>
                     <Nav.Link onClick={Job}>Apply Job</Nav.Link>
                     <Nav.Link onClick={ApplicationRecord}>History</Nav.Link>
                  </Nav>
                  <Form inline>
                     <Logout />
                  </Form>
               </Navbar.Collapse>
            </Navbar>

         }

         {isLoggedIn &&
            role?.toString() === roleObj.other &&

            <Navbar expand="lg">
               <Navbar.Brand><Image src={logotecky} fluid /></Navbar.Brand>
               <Navbar.Toggle aria-controls="basic-navbar-nav" />
               <Navbar.Collapse id="basic-navbar-nav">
                  {/* <Nav className="mr-auto">
                        <Nav.Link onClick={DashBoard}>Home</Nav.Link>
                        <Nav.Link onClick={profile}>Profile</Nav.Link>
                        <NavDropdown title="Advanced" id="basic-nav-dropdown">
                           <NavDropdown.Item disabled >Job</NavDropdown.Item>
                           <NavDropdown.Divider />
                           <NavDropdown.Item onClick={Job}>Apply Job</NavDropdown.Item>
                           <NavDropdown.Item onClick={ApplicationRecord}>History</NavDropdown.Item>
                        </NavDropdown>
                     </Nav> */}
                  <Form inline id="logout-company">
                     <Logout />
                  </Form>
               </Navbar.Collapse>
            </Navbar>
         }
      </>
   )
}