import React, { useState } from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import  "./UploadResume.scss"
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { uploadResumeThunk } from '../Redux/Profile/thunks'
import { RootState } from '../store'


interface Data {
    resume: string
}

export const UploadResume = () => {

    const dispatch = useDispatch()
    const student = useSelector((state: RootState) => state.student, shallowEqual);
    const [show, setShow] = useState(false)
    const handleShow = () => setShow(true)
    const [resume, setResume] = useState<undefined | File>(undefined)
    const onHide = () => setShow(false)


    return (
        <>
            <Button variant="outline-dark" onClick={handleShow} >Upload Resume</Button>
            <Modal show={show} onHide={onHide} size="sm" centered >
                <Modal.Header closeButton  >
                    <Modal.Title>Upload Resume</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form onSubmit={(event: React.FormEvent) => {
                        event.preventDefault();
                        // console.log(resume?.size)
                        if(resume && resume.size >2000000){
                           alert("Please upload less than 2mb!")
                           return;
                       }
                        const formData = new FormData();
                        if (resume !== undefined) {
                            formData.append('resume', resume)
                        }
                        dispatch(uploadResumeThunk(student.studentId, formData))
                        onHide()
                    }} >
                        <div id="upload">
                            <div>
                                <Form.File
                                    id="resume"
                                    label="Caption: Only accept PDF format and file size less 2mb."
                                    data-browse="Upload"
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setResume(e.currentTarget.files?.[0])}
                                    accept="application/pdf"
                                
                                />
                            </div>
                            <div>
                                <Button type="submit" >Submit</Button>
                            </div>
                        </div>

                    </Form>
                </Modal.Body>
            </Modal>

        </>

    )



}