import React, { useState } from 'react';
import logotecky from '../website_photo/logotecky.png';
import gitlabicon from '../website_photo/gitlabicon.png';
import './Homepage.scss'
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { FrontPage } from './FrontPage';
import { push } from 'connected-react-router';
import { Button, Form } from 'react-bootstrap';
import { loginThunk } from '../Redux/auth/thunk';

export function Homepage() {

  const isLoggedIn = useSelector((state: RootState) => state.auth.token, shallowEqual);
  const dispatch = useDispatch();
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")

  const employersRegister = () => {
    dispatch(push('/employers'))
  }
  
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (email !== "" && password !== "") {
      dispatch(loginThunk(email, password))
    } else {
      alert("Please fill in all the box.")
    }
  }

  // const employers = () => {
  //   dispatch(push('/public/login'))
  // }
  // const employers = () => {
  //   dispatch(push('/employers'))
  // }

  return (
    <>
      {isLoggedIn ?
        <FrontPage /> :
        <div className="gitlab-button">
          <div id="logo">
            <img src={logotecky} alt="tecky logo" />
          </div>
          <div className="jobportal-title" style={{ cursor: "default" }}>Job Portal<br/> <h3>Company Login</h3></div>
          <div className="gitlab-box">
            <Form onSubmit={handleSubmit} id="loginForm">
              <Form.Group controlId="Email">
                <Form.Label>Company Email</Form.Label>
                <Form.Control type="email" placeholder="Enter Company Email" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.currentTarget.value)} required />
                <Form.Text className="text-muted">
                  Input your company email
                        </Form.Text>
              </Form.Group>

              <Form.Group controlId="Password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" required maxLength={16} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value)} />
              </Form.Group>

              <Button variant="outline-secondary" type="submit" id="loginSubmit">
                Company Login
                    </Button>

              <Button variant="outline-success" type="button" id="register" onClick={employersRegister}>
                New Company Register
                    </Button>

            </Form>



            <Button variant="outline-danger" id="loginGitlabBtn" href={`https://gitlab.com/oauth/authorize?client_id=${process.env.REACT_APP_CLIENT_ID}&redirect_uri=${encodeURIComponent(`${process.env.REACT_APP_FRONTEND_URL}/login/gitlab`)}&response_type=code&scope=email+openid+profile`}>
              <img src={gitlabicon} alt="GitLab Icon" />Tecky's Sign in with Gitlab
               </Button>



            {/* <Button  onClick={employers}>Employers Please Click Here!!</Button> */}

          </div>

        </div>
      }
    </>
  )
}
