import React, { useEffect } from 'react'
import { loginGitlabThunk } from '../Redux/auth/thunk';
import { useDispatch } from 'react-redux';
import { FrontPage } from './FrontPage';

export function LoginGitLab() {
  const dispatch = useDispatch();

  useEffect(() => {
    const params = new URLSearchParams(window.location.search)
    dispatch(loginGitlabThunk(params.get('code')!));
  }, [dispatch])

  return (
    <div>
      <FrontPage />
    </div>
  )
}
