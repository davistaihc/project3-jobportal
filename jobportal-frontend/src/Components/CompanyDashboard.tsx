import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Card, Modal, Button, Form } from 'react-bootstrap'
import './CompanyDashboard.scss'
import { setSelectedViewJob } from '../Redux/Job/actions'
// import { MyVerticallyCenteredModal } from './MyVerticallyCenteredModal'
import Moment from 'react-moment'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { RootState } from '../store'
import { getAllJobThunk } from '../Redux/Job/thunk'
import CreateJob from './CreateJob'
import { Job } from '../Redux/Job/state'
import { push } from 'connected-react-router'


export const CompanyDashboard = () => {
    // const companyId = localStorage.getItem("companyId")
    const dispatch = useDispatch();
    // const [modalShow, setModalShow] = React.useState(false);
    const jobIds = useSelector((state: RootState) => state.jobs.jobs)
    const jobs = useSelector((state: RootState) => jobIds.map(id => state.jobs.jobsById[id]))
    const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual);
    const [isOpen, setIsOpen] = useState(false);
    // const [title, setTitle] = useState("Loading");
    // const [company, setCompany] = useState("Loading");
    // const showModal = () => setIsOpen(true);
    const hideModal = () => setIsOpen(false);
    const companyId = (localStorage.getItem("companyId") ? Number(localStorage.getItem("companyId")) : null)
    const role = localStorage.getItem("roleId")
    const companyName = localStorage.getItem("companyName")
    
    useEffect(() => {
        if (role !== "3") {
            dispatch(push("/"))
        }
        dispatch(getAllJobThunk())
    }, [dispatch, role])
   
    // useEffect(() => {
    //     // if (selectedViewJob) {
    //     //     // setTitle(selectedViewJob.title)
    //     //     // setCompany(selectedViewJob.name)
    //     //     // showModal()
    //     // }
    // }, [dispatch, selectedViewJob])

    function makeUp(jobHighlight: string | null) {
        const highlight = jobHighlight?.toString().split(/•|\r\n/)
        return (
            highlight?.map((e, i) => (e !== "" &&
                <li key={i}>{e}</li>
            ))
        )
    }


    return (
        <Container>
            <div id="dashboard">
                {<div id="companyName">{companyName}</div>}
                <Row id="body" >
                    <Col md="6" id='form'>
                        <div>
                            <CreateJob />
                        </div>
                    </Col>
                    <Col md="6" id="viewJob">
                        <div >
                            <h3 id="h2-header">Posted Jobs</h3>
                            {jobs.map((job: Job, i) => (job.companyId === companyId && 
                                <div className="job-card">
                                    <Card style={{ minWidth: '27rem', maxWidth: '22rem', marginBottom: '2rem' }} key={`job${i}`} >
                                        <Card.Body className="job-body">
                                            <Card.Title>{job.title}</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">{job.name}</Card.Subtitle>
                                            <Card.Text>
                                                {job.location}
                                            </Card.Text>
                                            <Card.Text>
                                                <ul>
                                                    {makeUp(job.highlight)}
                                                </ul>
                                            </Card.Text>
                                            <Card.Link  href="#" onClick={() => { setIsOpen(true); dispatch(setSelectedViewJob(job)) }} className="detail" key={`jobId_${job.id}`}> Details</Card.Link>
                                            {/* {i < 1 && <MyVerticallyCenteredModal
                                                show={modalShow}
                                                onHide={() => setModalShow(false)}
                                            />} */}
                                        </Card.Body>
                                        <Card.Footer className="text-muted">
                                            <Moment toNow>{job.created_at}</Moment>
                                        </Card.Footer>
                                    </Card>
                                </div>
                            ))}
                          

                        </div>
                    </Col>
                </Row>

                {selectedViewJob &&
                    <Modal show={isOpen} onHide={hideModal} size="lg" id="form" scrollable>
                        <Modal.Header>
                            <Modal.Title>Application Form</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form noValidate >

                            <h3>{selectedViewJob?.name}</h3><br></br>
               <h4>{selectedViewJob.title}</h4>
               <br></br>
               <p>
                  Job location:<br></br>
                  {selectedViewJob?.location}<br></br><br></br>
                Job Responsibility:<br></br>
                  <ul> {makeUp(selectedViewJob.responsibility)}</ul>
                  <br></br>
                Job Requirement:<br></br>
                  <ul> {makeUp(selectedViewJob.requirement)}</ul>
                  <br></br>
                Salary:<br></br>
                  {selectedViewJob.salary ?`$ ${selectedViewJob.salary}`: "Negotiable"}<br></br><br></br>
                Job posted:<br></br>
                  <Moment format="YYYY/MM/DD">{selectedViewJob?.created_at}</Moment><br></br><br></br>
               </p>
                                <div id="formFooter">
                                    <div> <Button onClick={() => setIsOpen(false)} >Close</Button></div>
                                </div>
                            </Form>
                        </Modal.Body>
                    </Modal>
                }
            </div>
        </Container>
    )
}