import "rsuite/dist/styles/rsuite-default.css";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Sidenav, Nav, Dropdown, Icon } from "rsuite";
import "./SideBar.scss";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../store";
import { push } from "connected-react-router";


const panelStyles = {
   padding: '15px 20px',
   color: '#000000',
   fontSize: 18,
};



export const Navigation = () => {
   const dispatch = useDispatch();

   const expanded = useSelector((state: RootState) => state.sideBar.expanded)

   const DashBoard = () => {
      dispatch(push('/'))
   }

   const profile = () => {
      dispatch(push('/profile'));
   }
   const ApplicationRecord = () => {
      dispatch(push('/application_record'));
   }

   const Job = () => {
      dispatch(push('/job'))
   }

   return (
      <Router>
         <div>
            <Sidenav defaultOpenKeys={["3", "4"]} expanded={expanded} className="sidebar" id="sidebar">
               <Sidenav.Header id="header">
                  <div></div>
               </Sidenav.Header>
               <Sidenav.Body>
                  <Nav>
                     <Nav.Item eventKey="1" active icon={<Icon icon="dashboard" size='lg' className="icon" />} id="title" onClick={DashBoard}>Dashboard</Nav.Item>
                     <Nav.Item eventKey="2" icon={<Icon icon="profile" size='lg' className="icon" />} id="profile" onClick={profile}>My Profile</Nav.Item>
                     <Dropdown eventKey="3" title="Advanced" icon={<Icon icon="list" size='lg' className="icon" />} id="advanced">
                        <Dropdown.Item divider />
                        <Dropdown.Item panel style={panelStyles} id="job">
                           Jobs
                        </Dropdown.Item>
                        <Dropdown.Item eventKey="3-1" id="apply" onClick={Job}>Apply Jobs</Dropdown.Item>
                        <Dropdown.Item eventKey="3-2" id="history" onClick={ApplicationRecord}>History</Dropdown.Item>

                        <Dropdown.Item divider />

                     </Dropdown>
                  </Nav>
               </Sidenav.Body>
            </Sidenav>
         </div>
      </Router>
   );
};
