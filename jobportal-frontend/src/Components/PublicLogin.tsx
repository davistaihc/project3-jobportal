import React, { useState } from 'react'
import './PublicLogin.scss'
import { Form, Button } from 'react-bootstrap'
import logotecky from '../website_photo/logotecky.png';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import { loginThunk } from '../Redux/auth/thunk';


export const PublicLogin = () => {
    const dispatch = useDispatch()
    const [email, setEmail] = useState<string>("")
    const [password, setPassword] = useState<string>("")
    const employers = () => {
        dispatch(push('/employers'))
    }
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (email !== "" && password !== "") {
            dispatch(loginThunk(email, password))
        } else {
            alert("Please fill in all the box.")
        }
    }
    return (
        <div id="login">

            <div id="loginBox">
                <div id="logo"><img src={logotecky} alt="Logo" /></div>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="Email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.currentTarget.value)} required />
                        <Form.Text className="text-muted">
                            Input your company email
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="Password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required maxLength={16} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.currentTarget.value)} />
                    </Form.Group>
        
                    <Button variant="outline-secondary" type="submit">
                       Submit
                    </Button>

                    <Button variant="outline-success" type="button" onClick={employers}>
               Register
                    </Button>
                    
                </Form>
            </div>

        </div>
    )

}