import React from 'react';
import logotecky from '../website_photo/logotecky.png';
import gitlabicon from '../website_photo/gitlabicon.png';
import './Homepage.scss'
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { FrontPage } from './FrontPage';
import { push } from 'connected-react-router';

export function Homepage() {

  const isLoggedIn = useSelector((state: RootState) => state.auth.token, shallowEqual);
  const dispatch = useDispatch();


  const employers = () => {
    dispatch(push('/public/login'))
  }
  // const employers = () => {
  //   dispatch(push('/employers'))
  // }

  return (
    <>
      {isLoggedIn ?
        <FrontPage /> :
        <div className="gitlab-button">
          <div id="logo">
            <img src={logotecky} alt="tecky logo" />
          </div>
          <div className="jobportal-title" style={{ cursor: "default" }}>Job Portal</div>
          <div className="gitlab-box">
            <div className="gitlab-link">
              <a
                className="app-link"
                href={`https://gitlab.com/oauth/authorize?client_id=${process.env.REACT_APP_CLIENT_ID}&redirect_uri=${encodeURIComponent(`${process.env.REACT_APP_FRONTEND_URL}/login/gitlab`)}&response_type=code&scope=email+openid+profile`}
                rel="noopener noreferrer"
              >
                <div>
                  <img src={gitlabicon} alt="GitLab Icon" />
                </div>
                <div>
                  Sign in with gitlab
            </div>
              </a>
            </div>
          </div>
          <div>
            {/* <div onClick={employers}><h5>Employers Please Click Here!!</h5></div> */}
            <div onClick={employers} style={{ cursor: "pointer" }}><h5>Employers Please Click Here!!</h5></div>

          </div>
        </div>
      }
    </>
  )
}
