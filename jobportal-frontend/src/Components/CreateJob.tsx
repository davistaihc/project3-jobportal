import React, { useState } from 'react'
import './CreateJob.scss'
import { Row, Col, Form, Button } from 'react-bootstrap';
import { createJobByEmployersThunk } from '../Redux/Company/thunk';
import { useDispatch } from 'react-redux';

export default function CreateJob() {

    const dispatch = useDispatch();

    const [title, setTitle] = useState("")
    const [highlight, setHighlight] = useState("")
    const [benefit, setBenefit] = useState("")
    const [responsibility, setResponsibility] = useState("")
    const [salary, setSalary] = useState("")
    const [industry, setIndustry] = useState("")
    const [requirement, setRequirement] = useState("")

    const availability = true
    const isApproved = false
    const companyId = (localStorage.getItem("companyId") ? Number(localStorage.getItem("companyId")) : null)
    const [validated, setValidated] = useState(false);

    const [fullTime, setFullTime] = useState(false)
    const [partTime, setPartTime] = useState(0)
    const [freelance, setFreelance] = useState(0)

    let arr: number[] = []
    if (fullTime) {
        arr.push(1)
    }

    if (partTime % 2 === 1) {
        arr.push(2)
    }

    if (freelance % 2 === 1) {
        arr.push(3)
    }

    const refresh = () => {
        window.location.reload()
    }


    //Submit handle
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        event.stopPropagation();

        const form = event.currentTarget;
        if (!isNaN(Number(highlight))) {
            alert("Highlight should not just number(s)")
            return
        } else if (!isNaN(Number(benefit))) {
            alert("Benefit should not just number(s)")
            return
        } else if (!isNaN(Number(responsibility))) {
            alert("Responsibility should not just number(s)")
            return
        } else if (!isNaN(Number(requirement))) {
            alert("Requirement should not just number(s)")
            return
        }

        if (salary.length <= 0 || salary === "") {
            setSalary("Negotiable")
        }

        if (arr.length === 0) {
            alert("Please select employment type!")
            return
        }
        if (form.checkValidity() === false) {
            return;
        } else {
            if (companyId !== null) {
                dispatch(createJobByEmployersThunk(title, highlight, benefit, responsibility, salary, industry, requirement, companyId, arr, availability, isApproved))
                alert("Your job vacancy has been submitted! Thank you very much!");
                refresh()
            }
        }
        setValidated(true)
    };


    return (
        <div id="create-job-header">
            <h3 id="create-job-h2">Post a Job</h3>
            <div>
                <br></br>
                <Row className="title">
                    <Col md="12" className="content">
                        <Form onSubmit={handleSubmit} validated={validated} >
                            <Form.Group as={Col} controlId="name">
                                <Form.Label>Job Title</Form.Label>
                                <Form.Control type="text" value={title} onChange={(e: any) => setTitle(e.currentTarget.value.replace(/[0-9]/g, ''))} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="highlight">
                                <Form.Label>Job Highlight</Form.Label>
                                <Form.Control as="textarea" rows={7} value={highlight} onChange={(e: any) => setHighlight(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="benefit">
                                <Form.Label>Benefit</Form.Label>
                                <Form.Control as="textarea" rows={7} value={benefit} onChange={(e: any) => setBenefit(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="responsibility">
                                <Form.Label>Responsibility</Form.Label>
                                <Form.Control as="textarea" rows={13} value={responsibility} onChange={(e: any) => setResponsibility(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="requirement">
                                <Form.Label>Requirement</Form.Label>
                                <Form.Control as="textarea" rows={10} value={requirement} onChange={(e: any) => setRequirement(e.currentTarget.value)} required />
                            </Form.Group>

                            <Form.Group as={Col} controlId="salary">
                                <Form.Label>Salary</Form.Label>
                                <Form.Control type="text" value={salary} placeholder="Negotiable" onChange={e => setSalary(e.currentTarget.value.replace(/[^\d]/g, ''))} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="industry">
                                <Form.Label>Industry</Form.Label>
                                <Form.Control type="text" value={industry} onChange={(e: any) => setIndustry(e.currentTarget.value.replace(/[0-9]/g, ''))} required />
                            </Form.Group>


                            <Form.Group as={Col} controlId="employmentType">
                                <Form.Label>Employment Type</Form.Label>
                                {['checkbox'].map((type) => (
                                    <div key={`custom-inline-${type}`} >
                                        <Form.Check
                                            custom
                                            inline
                                            label="Full Time"
                                            type={'checkbox'}
                                            id={`custom-inline-${type}-1`}
                                            checked={fullTime}
                                            onClick={(event: any) => setFullTime(event.currentTarget.checked)}
                                        />
                                        <Form.Check
                                            custom
                                            inline
                                            label="Part Time"
                                            type={'checkbox'}
                                            id={`custom-inline-${type}-2`}
                                            onClick={() => setPartTime(partTime + 1)}
                                        />
                                        <Form.Check
                                            custom
                                            inline
                                            label="Freelance"
                                            type={'checkbox'}
                                            id={`custom-inline-${type}-3`}
                                            onClick={() => setFreelance(freelance + 1)}
                                        />
                                    </div>
                                ))}
                            </Form.Group>
                            <div className="submit">
                                <Button variant="outline-primary" type="submit">
                                    Submit
                            </Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </div>
        </div>
    )
}
