import React, { useState } from 'react'
import InputGroup from 'react-bootstrap/InputGroup'
import { Button, FormControl } from 'react-bootstrap'
import { push } from 'connected-react-router';
import { useDispatch } from 'react-redux';


export const JobSearch = () => {

   const dispatch = useDispatch();


   const [keyword, setKeyword] = useState("");

   const onChangeHandler = (event:React.ChangeEvent<HTMLInputElement>) => {
      setKeyword(event.currentTarget.value)
    }

    const keyPressed = (event: React.KeyboardEvent) => {
      if (event.key === "Enter") {
         dispatch(push(`/job?keyword=${keyword}`));
      }
  }

   const searchButton = (event:React.MouseEvent) => {
      event.preventDefault();
      dispatch(push(`/job?keyword=${keyword}`));
    }

   return (
      <>
         <InputGroup className="mb-3">
            <FormControl
               placeholder="Search...."
               onChange={onChangeHandler}
               onKeyPress={keyPressed}
               value={keyword}
               aria-label="Search"
               aria-describedby="basic-addon2"
            />
            <InputGroup.Append>
               <Button variant="outline-secondary" onClick={searchButton}>Search</Button>
            </InputGroup.Append>
         </InputGroup>
      </>
   )
}