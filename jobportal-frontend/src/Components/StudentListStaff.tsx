import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchStudents } from '../Redux/Student/thunks';
import { RootState } from '../store';
import { fetchCohorts } from '../Redux/cohorts/thunk';
import { Table, Container } from 'react-bootstrap';
import './StudentListStaff.scss'
import { roleObj } from '../variable/variable';
import { push } from 'connected-react-router';
import { setSelectedViewStudent } from '../Redux/Student/action';
import { EditStudent } from './EditStudent';
import 'react-calendar/dist/Calendar.css';



export function StudentListStaff() {

    const cohortList = useSelector((state: RootState) => state.cohortList.cohortList)
    const cohortChosen = cohortList.map(function (cohort, i) {
        if (i < 1) {
            return cohort.cohort
        }
        return null
    })[0]

    const [selectedCohortInStudentStatus, setSelectedCohortInStudentStatus] = useState('')
    const studentIds = useSelector((state: RootState) => state.students.studentsStatusByCohort[selectedCohortInStudentStatus])
    const students = useSelector((state: RootState) => studentIds?.map(id => state.students.studentsById[id]))
    const dispatch = useDispatch();
    const role = localStorage.getItem("roleId")

    useEffect(() => {
        if (role !== roleObj.staff) {
            dispatch(push("/"))
        }
        dispatch(fetchCohorts())
        dispatch(fetchStudents())
    }, [dispatch, role])

    useEffect(() => {
        if (cohortChosen != null) {
            setSelectedCohortInStudentStatus(cohortChosen)
        }
    }, [cohortChosen])

    return (
        <>
            <Container fluid className="staff-sl-table">

                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Salary (HKD)</th>
                            <th>Title</th>
                            <th>Company</th>
                            <th>Source</th>
                            <th>Contact No.</th>
                            <th>Email</th>
                            <th >
                                <select style={{ fontWeight: "bold", color: "rgb(96, 140, 200)", cursor: "pointer" }} onChange={event => setSelectedCohortInStudentStatus(event.currentTarget.value)
                                }> {cohortList.map((co, index) => <option key={index}>{co.cohort}</option>)}
                                </select></th>
                            <th>Edit</th>

                        </tr>
                    </thead>
                    <tbody>
                        {students?.map((student, index) => (

                            <>
                                <tr className="studentForStaff" key={index} onClick={() => dispatch(setSelectedViewStudent(student))}>
                                    <th>{student.first_name} {student.surname}</th>
                                    <th>{student.status}</th>
                                    <th>{student.salary}</th>
                                    <th>{student.position}</th>
                                    <th>{student.company_name}</th>
                                    <th>{student.way}</th>
                                    <th>{student.contact_number}</th>
                                    <th><a href={`mailto:${student.email}`}>{student.email}</a></th>
                                    <th>{student.cohort}</th>
                                        <EditStudent />
                                </tr>
                            </>
                        ))}
                    </tbody>
                </Table>
            </Container>

        </>
    )
}
