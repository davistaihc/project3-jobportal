import React, { useState, useEffect } from 'react'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { RootState } from '../store'
import { Button, Modal, Form } from 'react-bootstrap'


export const EditJobStatus = async () => {
    const dispatch = useDispatch()
    const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual)
    const [show ,setShow] = useState(false)
    const handleShow = () =>  setShow(true) 
    const handleHide = () => setShow(false)
    const handleSubmit = () => {
        return
    }

    useEffect(()=>{
        dispatch
    },[dispatch,])

    useEffect(()=>{

    },[selectedViewJob?.title , selectedViewJob?.requirement , selectedViewJob?.location ,selectedViewJob?.responsibility,selectedViewJob?.salary , selectedViewJob?.benefit])


    return (
        <>
            <Button variant="outline-secondary" type="button" onClick={handleShow}>Edit</Button>
            <Modal show={show} scrollable  >               
                <Modal.Header closeButton>
                Update Job Status
                </Modal.Header>
                <Form onSubmit={handleSubmit} >
               {selectedViewJob &&
                <Modal.Body>
                
  
                </Modal.Body>
               }
                <Modal.Footer>
                <Button variant="outline-secondary" type="submit">Upload</Button>
                <Button variant="outline-danger" type="button" onClick={handleHide} >Close</Button>
                </Modal.Footer>
                </Form>
            </Modal>


        </>
    )
}