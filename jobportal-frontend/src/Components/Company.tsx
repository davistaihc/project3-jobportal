import React, { useEffect } from 'react'
import { Table } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './FrontPage.scss'
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { getAllCompanyThunk } from '../Redux/Company/thunk'
import { RootState } from '../store'
import { ICompanyItem } from '../Redux/Company/state'
import './Company.scss'
import { Link, RouteComponentProps } from 'react-router-dom'
import { Homepage } from './Homepage'
import { roleObj } from '../variable/variable'
import { setSelectedViewCompany } from '../Redux/Company/actions'
import { push } from 'connected-react-router'


export const Company = (props: RouteComponentProps) => {
    const dispatch = useDispatch();

    const keyword = new URLSearchParams(props.location.search).get("keyword" || " ");

    const isLoggedIn = localStorage.getItem('token') ? true : false;
    const role = localStorage.getItem("roleId")

    const companyIds = useSelector((state: RootState) => state.company.companies, shallowEqual)
    const companies = useSelector((state: RootState) => companyIds.map(id => state.company.companyById[id]))


    useEffect(() => {
        if (keyword === null) {
            dispatch(getAllCompanyThunk())
        } else {
            // dispatch(searchCompanyThunk(keyword as string));
        }
    }, [dispatch, keyword]);

    const result = companies.length

    return (
        <>
            {isLoggedIn ?
                role === roleObj.staff &&
                    result === 0 ?
                    <h1>No Result Found</h1>
                    :
                    <div className="container">
                        <h2>Company</h2>
                        <hr></hr>
                        <Table striped bordered hover responsive>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact Person</th>
                                    <th>Title</th>
                                    <th>Contact No.</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody style={{ cursor: "pointer" }}>
                                {companies.map((company: ICompanyItem) => (<tr key={`job_${company.id}`} onClick={() => {
                                    dispatch(push(`/company/detail/${company.id}`))
                                    dispatch(setSelectedViewCompany(company))
                                }}>
                                    <td>{company.name}</td>
                                    <td>{company.contact_person}</td>
                                    <td>{company.position}</td>
                                    <td>{company.contact_number}</td>
                                    <td><a href=" " onClick={() => window.location.href = `mailto:${company.email}`}>{company.email}</a></td>
                                </tr>))}
                            </tbody>
                        </Table>
                    </div >
                :
                <Link to="/">
                    <Homepage />
                </Link>
            }
        </>
    )
}