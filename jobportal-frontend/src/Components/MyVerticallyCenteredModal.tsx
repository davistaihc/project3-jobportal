import React from 'react'
import { useSelector, shallowEqual } from 'react-redux';
import { RootState } from '../store';
import Moment from 'react-moment';
import { Button, Modal } from 'react-bootstrap';
import { ApplicationForm } from './ApplicationsForm';
import { roleObj } from '../variable/variable';


export function MyVerticallyCenteredModal(props: any) {

   const selectedViewJob = useSelector((state: RootState) => state.jobs.selectedViewJob, shallowEqual);
   const role = localStorage.getItem("roleId")

   function makeUp(selectedViewJob: string | null | undefined) {
      const highlight = selectedViewJob?.toString().split(/•|\r\n/)
      return (
         highlight?.map(e => (e !== "" &&
            <li>{e}</li>
         ))
      )
   }

   return (
      <>
         {selectedViewJob && selectedViewJob.id > 0 && <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            scrollable
         >
            <Modal.Header closeButton>
               <Modal.Title id="contained-modal-title-vcenter">

                  {
                     role !== roleObj.other &&
                     <ApplicationForm />
                  }

               </Modal.Title>

            </Modal.Header>
            <Modal.Body>
               <h3>{selectedViewJob?.name}</h3><br></br>
               <h4>{selectedViewJob.title}</h4>
               <br></br>
               <p>
                  Job location:<br></br>
                  {selectedViewJob?.location}<br></br><br></br>
                Job Responsibility:<br></br>
                  <ul> {makeUp(selectedViewJob.responsibility)}</ul>
                  <br></br>
                Job Requirement:<br></br>
                  <ul> {makeUp(selectedViewJob.requirement)}</ul>
                  <br></br>
                Salary:<br></br>
                  {selectedViewJob.salary}<br></br><br></br>
                Job posted:<br></br>
                  <Moment format="YYYY/MM/DD">{selectedViewJob?.created_at}</Moment><br></br><br></br>
               </p>
            </Modal.Body>
            <Modal.Footer>
               <Button variant="outline-secondary" onClick={props.onHide}>Close</Button>
            </Modal.Footer>
         </Modal >}
      </>
   );
}