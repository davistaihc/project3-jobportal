import React, { useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import { useDispatch, useSelector } from 'react-redux';
import { fetchCohorts } from '../Redux/cohorts/thunk';
import { Col } from 'react-bootstrap';
import { RootState } from '../store';
import { fetchStudentSalary } from '../Redux/Statistic/thunk';
import './ChartApplicationStatus.scss'


export default function ChartApplicationStatus() {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchCohorts())
    }, [dispatch])



    const cohortList = useSelector((state: RootState) => state.cohortList.cohortList)
    const cohort = cohortList.map((cohort) => cohort.cohort)

    const cohortSalary = useSelector((state: RootState) => state.cohortSalary.cohortSalary)

    useEffect(() => {
        dispatch(fetchStudentSalary());
    }, [dispatch])



    const options = {
        title: {
            display: true,
            text: `Cohorts' average salary (hkd/month)`,
            fontSize: 20,
        },
        tooltips: {
            enabled: true
        },
        legend: {
            display: false,
            labels: {
                // fontSize: 100
            },
        },
        scales: {
            yAxes: [{
                ticks: {
                    // beginAtZero: true,
                    min: 10000,
                    stepSize: 2000,
                    // fontSize: 15
                }
            }],
            xAxes: [{
                ticks: {
                    fontSize: 12
                }
            }]

        },
        responsive:true 
    }

    return (
        <>
            <Col>

                <div className="barChart" >
                    <br />
                    <Bar data={{

                        labels: cohort,
                        datasets: [{
                            label: 'HKD',
                            data: cohortSalary,
                            // barThickness: 70,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 55, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 55, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    }
                    } options={options} />

                </div>

            </Col>
        </>
    )
}

