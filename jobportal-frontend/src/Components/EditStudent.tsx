import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import { Modal, Form, Button, Col } from 'react-bootstrap'
import { updateStudentByStaffThunk } from '../Redux/Staff/thunks'
import Calendar from 'react-calendar'
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from 'moment'



export const EditStudent = () => {

    const dispatch = useDispatch()
    const status = ["Available", "Not Available", "Employed"]
    const [show, setShow] = useState<boolean>(false)
    const [statue, setStatue] = useState("")
    const [salary, setSalary] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [position, setPosition] = useState("")
    const [way, setWay] = useState("")
    const [click, setClick] = useState(false);
    const [date, setDate] = useState(moment())
    const handleClick = () => setClick(true)
    const onHideClick = () => setClick(false)



    // const show = useSelector((state:RootState)=>state.students.expanded)
    const student = useSelector((state: RootState) => state.students.selectedViewStudent)
    // const student = prop.student
    const handleShow = () => setShow(!show)
    const onChange = (nextValue: any) => setDate(nextValue)
    const onboardDateStr = date.toISOString()


    useEffect(() => {
        if (student !== undefined) {
            setStatue(student.status)
            setSalary(student.salary)
            setStatue(student.status)
            setCompanyName(student.company_name)
            setPosition(student.position)
            setWay(student.way)
            setDate(moment(student.onboard_date))
        }
    }, [dispatch,student])
    
    const reload = () => {
        window.location.reload()
    }

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        event.stopPropagation()
        if (student) {
            dispatch(updateStudentByStaffThunk(student.id, statue, salary,companyName, position, way, date.toISOString()))
            handleShow()
            reload()

        }
    }

    return (
        <> <th onClick={handleShow}><FontAwesomeIcon icon={faEdit} size="1x" /></th>
            {
                <Modal show={show} onHide={() => { setShow(false) }} size="lg"   >
                    <Form noValidate onSubmit={handleSubmit} suppressContentEditableWarning>

                        <Modal.Header>
                            <Modal.Title>Update Students Status</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group as={Col} controlId="status">
                                <Form.Label>Status</Form.Label>
                                <Form.Control as="select" name="status" onChange={e => setStatue(e.currentTarget.value)}>
                                    {<option>{`${student?.status}`}</option>}
                                    {status.map(e => e !== student?.status && <option>{e}</option>)}
                                </Form.Control>
                            </Form.Group>
                            {statue === "Employed" &&
                                <>
                                    <Form.Group as={Col} controlId="salary">
                                        <Form.Label>Salary</Form.Label>
                                        <Form.Control type="text" value={salary} onChange={e => setSalary(e.currentTarget.value)} maxLength={8} pattern="[0-9]*" />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="companyName">
                                        <Form.Label>Company</Form.Label>
                                        <Form.Control type="text" value={companyName} onChange={e => setCompanyName(e.currentTarget.value)} />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="jobTitle">
                                        <Form.Label>Job Title</Form.Label>
                                        <Form.Control type="text" value={position} onChange={e => setPosition(e.currentTarget.value)} />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="way">
                                        <Form.Label>The way to find job</Form.Label>
                                        <Form.Control type="text" value={way} onChange={e => setWay(e.currentTarget.value)} />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="onboard">
                                        <Form.Label>Onboard Date</Form.Label>
                                        {!click && <Form.Control type="text" value={moment(onboardDateStr).local().format('MMMM Do YYYY')} onClick={handleClick} />}
                                        {click && <Form.Control type="text" value={moment(onboardDateStr).local().format('MMMM Do YYYY')} onClick={onHideClick} />}
                                    </Form.Group>

                                </>
                            }
                            {click && <Modal show={true} onHide={onHideClick} center size="sm">
                                <Modal.Header closeButton>
                                    <Modal.Title>Choose Onboard Day</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {<Calendar onChange={onChange} calendarType="ISO 8601" className="calender" />}
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={onHideClick}>Submit</Button>
                                </Modal.Footer>
                            </Modal>}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button type="submit" variant="outline-success">
                                Confirm
                         </Button>
                            <Button variant="outline-secondary" onClick={() => setShow(false)}>
                                Close
                        </Button>

                        </Modal.Footer>
                    </Form>
                </Modal>}
        </>
    )
}