import { ThunkDispatch } from '../../store';
import { loadCohorts, loadCohortList } from './action';

const { REACT_APP_API_SERVER } = process.env;

export const fetchCohorts = () => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/student/cohort`, {

         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      });

      const cohort = await res.json();
      dispatch(loadCohorts(cohort))
      dispatch(loadCohortList(cohort))

   }
}