import { CohortActions } from "./action";

export interface Cohort {
  cohort: string;
}

export interface CohortsState {
  cohorts: Cohort[];
  cohortList: Cohort[];
}

const initialState: CohortsState = {
  cohorts: [],
  cohortList: []
}


export const cohortsReducer = (oldState: CohortsState = initialState, action: CohortActions): CohortsState => {
  switch (action.type) {
    case "@@COHORTS/LOAD_COHORTS":
      return {
        ...oldState,
        cohorts: action.cohorts,
      }
    case "@@COHORTS_LIST/LOAD_COHORT_LIST":
      return {
        ...oldState,
        cohortList: action.cohortList
      }
  }
  return oldState;
}
