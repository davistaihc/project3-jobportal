import { Cohort } from "./reducer";

export function loadCohorts(cohorts: Cohort[]) {
    return {
        type: "@@COHORTS/LOAD_COHORTS" as "@@COHORTS/LOAD_COHORTS",
        cohorts
    }
}

export function loadCohortList(cohortList: Cohort[]) {
    return {
        type: "@@COHORTS_LIST/LOAD_COHORT_LIST" as "@@COHORTS_LIST/LOAD_COHORT_LIST",
        cohortList
    }
}

export type CohortActions = ReturnType<typeof loadCohorts | typeof loadCohortList>;    