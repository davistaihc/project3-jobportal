export function ShowSideBar(expanded: boolean | undefined) {
   return {
      type: "@@SIDEBAR_EXPANDED" as "@@SIDEBAR_EXPANDED",
      expanded
   }
}

export type SideBarAction = ReturnType<typeof ShowSideBar>