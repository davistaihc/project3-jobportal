import { SideBarAction } from './action';


export interface SideBarState {
   expanded: boolean
}

const initialState: SideBarState = {
   expanded: false
}


export const SideBarReducer = (state: SideBarState = initialState, action: SideBarAction) => {
   switch (action.type) {
      case "@@SIDEBAR_EXPANDED":
         return {
            expanded: !state.expanded
         };
      default:
         return state
   }


}
