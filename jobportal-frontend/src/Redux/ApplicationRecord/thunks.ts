import { ThunkDispatch, RootState } from "../../store";
import { failed, studentApplicationByUserId } from "./actions";

const { REACT_APP_API_SERVER } = process.env;


export function studentApplicationByUserIdThunk() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/application/student`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
    });

    const studentApplications = await res.json()

    if (res.status === 200) {
      dispatch(studentApplicationByUserId(studentApplications));
    } else {
      dispatch(failed("@@STUDENT_APPLICATION/LOAD_INFO_FAILED", studentApplications.msg));
    }
  }
}


export function UpdateApplicationStatusThunk(statusId:number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {

    const token = getState().auth.token
    // const studentId = getState().auth.studentId 
    const studentId = localStorage.getItem("studentId")
    const jobId = getState().studentApplications.selectedViewApplication?.job_id
    
    const data = {
      studentId,
      statusId ,
      jobId
    }

    

    const res = await fetch(`${REACT_APP_API_SERVER}/student/updateStatus/:${studentId}`, {
      method: "PUT",
      headers: {
          'Content-Type': "application/json",
          'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data)
    });

    const result = await res.json()

    if (res.status === 200) {
      dispatch(studentApplicationByUserIdThunk())
      alert(result.msg)
    } else {
      dispatch(failed("@@STUDENT_APPLICATION/LOAD_INFO_FAILED", result.msg));
    }
  }
}


