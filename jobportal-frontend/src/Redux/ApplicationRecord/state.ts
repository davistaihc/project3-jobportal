
export interface StudentApplication {
    Id: number
    created_at: Date
    content: string | null
    submitted_resume: string | null
    submitted_resume_url: string | null
    job_id: number
    title: string | null
    responsibility: string | null
    requirement: string | null
    salary: string | null
    company_id: number
    name: string | null
    location: string | null
    status: string
    surname: string | null
    first_name: string | null
    cohort: string | null
}
