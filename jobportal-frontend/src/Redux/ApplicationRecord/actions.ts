import { StudentApplication } from "./state";

export function studentApplicationByUserId(studentApplications: StudentApplication[]) {
    return {
        type: "@@STUDENT_APPLICATION/LOAD_INFO" as "@@STUDENT_APPLICATION/LOAD_INFO",
        studentApplications
    }
}

export const setSelectedViewApp = (studentApplication: StudentApplication | null) => {
    return {
        type: "@@STUDENTAPPLICATIONS/SET_SELECT_STUDENTAPPLICATIONS" as "@@STUDENTAPPLICATIONS/SET_SELECT_STUDENTAPPLICATIONS",
        studentApplication
    }
}


    

export function failed(type: FAILED, msg: string) {
    return {
        type,
        msg
    }
}

type FAILED = "@@STUDENT_APPLICATION/LOAD_INFO_FAILED";

type actionCreator = typeof studentApplicationByUserId | typeof failed | typeof setSelectedViewApp 

export type StudentApplicationActions = ReturnType<actionCreator>