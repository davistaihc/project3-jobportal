import { StudentApplicationActions } from "./actions"
import { StudentApplication } from "./state";

export interface StudentApplicationState {
    studentApplications: StudentApplication[];
    selectedViewApplication: null | StudentApplication
}
const initialState: StudentApplicationState = {
    studentApplications: [],
    selectedViewApplication: null
}

export const studentApplicationReducer = (oldState: StudentApplicationState = initialState, action: StudentApplicationActions): StudentApplicationState => {
    switch (action.type) {
        case "@@STUDENT_APPLICATION/LOAD_INFO":
            return {
                ...oldState,
                studentApplications: action.studentApplications,
            }

        case "@@STUDENTAPPLICATIONS/SET_SELECT_STUDENTAPPLICATIONS":
            return {
                ...oldState,
                selectedViewApplication: action.studentApplication

            }
  
    }
    return oldState;
}
