import { Job } from "../Job/state";

export interface ICompanyItem {
   id: number;
   user_id: number | null
   name: string | null;
   position: string | null;
   contact_number: string | null;
   contact_person: string | null;
   location: string | null;
   email: string | null;
   website: string | null;
   company_background: string | null;
   isPartner: boolean | null;
}




export interface ICompanyState {
   companyById: {
      [companyId: string]: ICompanyItem
   },
   companies: number[]
   selectedViewCompany: null | ICompanyItem
   companiesArr: ICompanyItem | null
   job: Job | null
}
