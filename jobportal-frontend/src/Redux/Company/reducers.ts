import { ICompanyState } from './state'
import produce from 'immer'
import { ICompanyAction } from './actions'

const initialState: ICompanyState = {
   companiesArr: null,
   companyById: {},
   companies: [],
   selectedViewCompany: null,
   job: null
}



export const CompanyReducer = (state: ICompanyState = initialState, action: ICompanyAction) => {
   return produce(state, state => {
      switch (action.type) {
         case "@@COMPANY/GET_ALL_COMPANY":
            for (const company of action.companies) {
               state.companyById[company.id] = company
            }
            state.companies = action.companies.map(company => company.id);
            break;

         case "@@COMPANY/SET_SELECT_COMPANY":
            state.selectedViewCompany = action.company;
            break;

         case "@@COMPANY/CREATE_COMPANY":
            state.companiesArr = action.company;
            break;

         case "@@COMPANY/CREATE_JOB_BY_EMPLOYERS":
            state.job = action.job;
            break;
      }
   })
}