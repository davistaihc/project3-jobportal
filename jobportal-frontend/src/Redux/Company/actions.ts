import { ICompanyItem } from "./state";
import { Job } from "../Job/state";

export const getAllCompany = (companies: ICompanyItem[]) => {
   return {
      type: "@@COMPANY/GET_ALL_COMPANY" as "@@COMPANY/GET_ALL_COMPANY",
      companies
   }
}


export function failed(type: FAILED, msg: string) {
   return {
      type,
      msg
   }
}

export const setSelectedViewCompany = (company: ICompanyItem | null) => {
   return {
      type: "@@COMPANY/SET_SELECT_COMPANY" as "@@COMPANY/SET_SELECT_COMPANY",
      company
   }
}

export function getCompanyById (companyId: number, companyItems: ICompanyItem[]){
   return {
       type:"@@COMPANY/GET_COMPANY" as "@@COMPANY/GET_COMPANY",
       companyId,
       companyItems
   }
}

export function createCompany(company:ICompanyItem){
   return {
       type: "@@COMPANY/CREATE_COMPANY" as "@@COMPANY/CREATE_COMPANY",
       company
   };
}

export function createCompanyFailed(msg:string){
   return {
       type: "@@COMPANY/CREATE_COMPANY_FAILED" as "@@COMPANY/REGISTER_FAILED",
       msg 
   }
}

export function createJobByEmployers(job:Job){
   return {
       type: "@@COMPANY/CREATE_JOB_BY_EMPLOYERS" as "@@COMPANY/CREATE_JOB_BY_EMPLOYERS",
       job
   };
}

export function createByEmployersFailed(msg:string){
   return {
       type: "@@COMPANY/CREATE_JOB_BY_EMPLOYERS_FAILED" as "@@COMPANY/CREATE_JOB_BY_EMPLOYERS_FAILED",
       msg 
   }
}

type FAILED = "@@COMPANY/LOAD_FAILED" | typeof failed;



type actionCreator = typeof getAllCompany | typeof getCompanyById | typeof setSelectedViewCompany |
                     typeof createCompany | typeof createCompanyFailed |
                     typeof createJobByEmployers | typeof createByEmployersFailed

export type ICompanyAction = ReturnType<actionCreator>