import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getAllCompany, failed } from '../actions';
import { getAllCompanyThunk } from '../thunk';
import fetchMock from 'fetch-mock';
import { RootState, ThunkDispatch } from '../../../store';



describe('company thunks', () => {
    let store: MockStoreEnhanced<RootState, ThunkDispatch>;

    beforeEach(() => {
        const mockStore = configureMockStore<RootState, ThunkDispatch>([thunk])
        store = mockStore();
    })

    it('should get company successfully', async () => {
        const result = [{
            id: 1,
            user_id: 2,
            name: "DEF Company",
            position: "HR Manager",
            contact_number: "31318989",
            contact_person: "May May",
            location: "Central",
            email: "hr@def.com",
            website: "www.def.com",
            company_background: "Work life balance working environment",
            isPartner: true,
        }];
        fetchMock.get(`${process.env.REACT_APP_API_SERVER}/company`,
            { body: result, status: 200 });

        const expectedActions = [
            getAllCompany([{
                id: result[0].id,
                user_id: result[0].user_id,
                name: result[0].name,
                position: result[0].position,
                contact_number: result[0].contact_number,
                contact_person: result[0].contact_person,
                location: result[0].location,
                email: result[0].email,
                website: result[0].website,
                company_background: result[0].company_background,
                isPartner: result[0].isPartner
            }]),
        ]
        await store.dispatch(getAllCompanyThunk());
        expect(store.getActions()).toEqual(expectedActions);
    });

})