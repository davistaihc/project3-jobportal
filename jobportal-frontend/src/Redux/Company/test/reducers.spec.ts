import { ICompanyState } from '../state';
import { CompanyReducer } from '../reducers';
import { getAllCompany } from '../actions';


describe('threadReducer', () => {
    let initialState: ICompanyState;

    beforeEach(() => {

        initialState = {
            companies: [],
            companiesArr: null,
            companyById: {},
            selectedViewCompany: null,
            job: null
        }
    });

    it("should get all companies", () => {
        const finalState = CompanyReducer(initialState, getAllCompany([{
            id: 1,
            user_id: 2,
            name: "DEF Company",
            position: "HR Manager",
            contact_number: "31318989",
            contact_person: "May May",
            location: "Central",
            email: "hr@def.com",
            website: "www.def.com",
            company_background: "Work life balance working environment",
            isPartner: true,
        }, {
            id: 2,
            user_id: 3,
            name: "GHI Company",
            position: "HR Manager",
            contact_number: "89987777",
            contact_person: "LadyMama",
            location: "Tokyo",
            email: "hr@ghi.com",
            website: "www.ghi.com",
            company_background: "International working environment",
            isPartner: false,
        }]));

        expect(finalState.companies.length).toBe(2);
    })
})