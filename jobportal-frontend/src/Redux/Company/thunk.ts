import { ThunkDispatch } from '../../store';
import { getAllCompany, failed, getCompanyById, createCompany, createCompanyFailed, createJobByEmployers, createByEmployersFailed } from './actions'
const { REACT_APP_API_SERVER } = process.env;

export const getAllCompanyThunk = () => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/company`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      });
      const companies = await res.json();
      if (res.status === 200) {
         dispatch(getAllCompany(companies))
      } else {
         dispatch(failed("@@COMPANY/LOAD_FAILED", companies.msg));
      }
   }
}

export const createCompanyThunk = (
   name: string,
   position: string,
   contactNumber: string,
   contactPerson: string,
   location: string,
   email: string,
   website: string,
   companyBackground: string,
   password:string,
   confirmPassword:string
) => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/company`, {
         method: "POST",
         headers: {
            'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            name,
            position,
            contactNumber,
            contactPerson,
            location,
            email,
            website,
            companyBackground,
            password,
            confirmPassword,
         })
      });
      const companies = await res.json();
      if (res.status === 200) {
         dispatch(createCompany(companies))
      } else {
         dispatch(createCompanyFailed(companies.msg));
      }
   }
}

export const createJobByEmployersThunk = (
   title: string,
   highlight: string,
   benefit: string,
   responsibility: string,
   salary: string,
   industry: string,
   requirement: string,
   companyId: number,
   employmentTypeId: number[],
   availability: boolean,
   isApproved:boolean
) => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/company/job`, {
         method: "POST",
         headers: {
            'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            title,
            highlight,
            benefit,
            responsibility,
            salary,
            industry,
            requirement,
            companyId,
            employmentTypeId,
            availability,
            isApproved
         })
      });
      const job = await res.json();
      if (res.status === 200) {
         dispatch(createJobByEmployers(job))
      } else {
         dispatch(createByEmployersFailed(job.msg));
      }
   }
}

export function getCompanyByIdThunk(companyId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/company/${companyId}`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      });
      const result = await res.json();

      if (res.status === 200) {
         dispatch(getCompanyById(result.companyId, result.data));
      } else {
         dispatch(failed("@@COMPANY/LOAD_FAILED", result.msg));
      }
   }
}

// export function searchCompanyThunk(keyword: string) {

//    return async (dispatch: ThunkDispatch) => {

//       const res = await fetch(`${REACT_APP_API_SERVER}/company?keyword=${keyword}`, {
//          headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${localStorage.getItem("token")}`
//          },
//       });

//       const companies = await res.json();

//       if (res.status === 200) {
//          dispatch(getAllCompany(companies));
//       } else {
//          dispatch(failed("@@COMPANY/LOAD_FAILED", companies.msg));
//       }
//    }
// }