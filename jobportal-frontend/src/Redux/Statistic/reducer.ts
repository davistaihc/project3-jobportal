import { StudentsStatusAction } from "./action";

export interface Count {
  count: number;
}
export interface Salary {
  salary: number;
}

export interface StudentsStatusState {
  studentsAvailable: Count[];
  studentsNotAvailable: Count[];
  studentsEmployed: Count[];
  cohortSalary: Salary[]
}

const initialState: StudentsStatusState = {
  studentsAvailable: [],
  studentsNotAvailable: [],
  studentsEmployed: [],
  cohortSalary: []
}

export const statisticReducer = (oldState: StudentsStatusState = initialState, action: StudentsStatusAction): StudentsStatusState => {
  switch (action.type) {
    case "@@STUDENT_AVAILABLE":
      return {
        ...oldState,
        studentsAvailable: action.studentsAvailable
      }
    case "@@STUDENT_NOT_AVAILABLE":
      return {
        ...oldState,
        studentsNotAvailable: action.studentsNotAvailable
      }
    case "@@STUDENT_EMPLOYED":
      return {
        ...oldState,
        studentsEmployed: action.studentsEmployed
      }
    case "@@LOAD_COHORT_SALARY":
      return {
        ...oldState,
        cohortSalary: action.cohortSalary
      }
  }
  return oldState;
}
