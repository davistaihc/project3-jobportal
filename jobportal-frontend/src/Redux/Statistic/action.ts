import { Count, Salary } from "./reducer";

export function loadStudentsAvailable(studentsAvailable: Count[]) {
    return {
        type: "@@STUDENT_AVAILABLE" as "@@STUDENT_AVAILABLE",
        studentsAvailable
    }
}

export function loadStudentsNotAvailable(studentsNotAvailable: Count[]) {
    return {
        type: "@@STUDENT_NOT_AVAILABLE" as "@@STUDENT_NOT_AVAILABLE",
        studentsNotAvailable
    }
}

export function loadStudentsEmployed(studentsEmployed: Count[]) {
    return {
        type: "@@STUDENT_EMPLOYED" as "@@STUDENT_EMPLOYED",
        studentsEmployed
    }
}

export function loadCohortSalary(cohortSalary: Salary[]) {
    return {
        type: "@@LOAD_COHORT_SALARY" as "@@LOAD_COHORT_SALARY",
        cohortSalary
    }
}

export type StudentsStatusAction = ReturnType<typeof loadStudentsAvailable | typeof loadStudentsNotAvailable | typeof loadStudentsEmployed | typeof loadCohortSalary>;    