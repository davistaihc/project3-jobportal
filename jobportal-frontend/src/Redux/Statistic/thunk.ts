import { ThunkDispatch } from '../../store';
import { loadStudentsAvailable, loadStudentsNotAvailable, loadStudentsEmployed, loadCohortSalary } from './action';

const { REACT_APP_API_SERVER } = process.env;

export const fetchStudentAva = (selectedCohort: string) => {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/statistic/studentAva/${selectedCohort}`,
         {
            headers: {
               'Content-Type': 'application/json',
               'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
         })
      const json = await res.json();
      dispatch(loadStudentsAvailable(json))

   }
}

export const fetchStudentNotAva = (selectedCohort: string) => {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/statistic/studentNotAva/${selectedCohort}`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      })
      const json = await res.json();
      dispatch(loadStudentsNotAvailable(json))

   }
}

export const fetchStudentEmployed = (selectedCohort: string) => {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/statistic/studentEmployed/${selectedCohort}`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      })
      const json = await res.json();
      dispatch(loadStudentsEmployed(json))

   }
}

export const fetchStudentSalary = () => {

   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/statistic/cohortSalary`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },

      })
      const json = await res.json();
      dispatch(loadCohortSalary(json))

   }
}