import { ThunkDispatch } from "../../store";
import { loadEmailMapping } from "./actions";
import { EmailMappingResult } from "./reducers";
import { fetchEmailMappingInfo } from "../StaffApplication/thunk";

export function fetchEmailMapping(jobId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/application/jobs/${jobId}/students`,
         {
            headers: {
               'Content-Type': 'application/json',
               'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
         });
      const json: EmailMappingResult[] = await res.json();
      dispatch(loadEmailMapping(jobId, json.map(res => res.student_id)))
   }
}


export function postEmailMapping(jobId: number, studentId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/application/jobs/${jobId}/students`, {
         method: 'POST',
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
         body: JSON.stringify({
            student_id: studentId
         })
      })
      const json: any = await res.json();
      if (json === false) {
         return alert("The student has been selected / Missing CV / The student is not available ")
      }

      dispatch(loadEmailMapping(jobId, json.map((res: { student_id: any; }) => res.student_id)))
      dispatch(fetchEmailMappingInfo(jobId))
   }
}

export function RemoveEmailMapping(jobId: number, studentId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/application/jobs/${jobId}/students/removed`, {
         method: 'PUT',
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
         body: JSON.stringify({
            student_id: studentId
         })
      })
      const json: any = await res.json();
      if (json === false) {
         return alert("Application has been sent. The record cannot be removed")
      }

      dispatch(loadEmailMapping(jobId, json.map((res: { student_id: any; }) => res.student_id)))
      dispatch(fetchEmailMappingInfo(jobId))

   }
}