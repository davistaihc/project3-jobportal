import produce from "immer";
import { EmailMappingActions } from "./actions";

export interface EmailMappingState {
    studentsByJobId: {
        [jobId: string]: number[]
    },
}

export interface EmailMappingResult {
    student_id: number;
    job_id: number;
    app_sent_success: boolean;
}

const initialState: EmailMappingState = {
    studentsByJobId: {}
}

export function emailMappingReducer(state: EmailMappingState = initialState, action: EmailMappingActions): EmailMappingState {
    return produce(state, state => {
        switch (action.type) {
            case "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING":
                state.studentsByJobId[action.jobId] = action.students
                break;
        }
    })
}