export function loadEmailMapping(jobId: number, students: number[]) {
    return {
        type: "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING" as "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING",
        jobId,
        students
    }
}


type actionCreator = typeof loadEmailMapping;
export type EmailMappingActions = ReturnType<actionCreator>