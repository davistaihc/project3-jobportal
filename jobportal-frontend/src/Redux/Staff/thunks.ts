import { ThunkDispatch } from "../../store";
import { failed, getStaffName } from "./actions";

const { REACT_APP_API_SERVER } = process.env;

export function getStaffNameThunk() {
  return async (dispatch: ThunkDispatch) => {

    const res = await fetch(`${REACT_APP_API_SERVER}/current/profile`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
    });

    const result = await res.json();


    if (res.status === 200) {
      dispatch(getStaffName(result.staff_name));
    } else {
      dispatch(failed("@@STAFF/LOAD_NAME_FAILED", result));
    }
  }
}

export function updateStudentByStaffThunk(
  studentId: number,
  status: string,
  salary: string,
  company_name: string,
  position: string,
  way: string,
  onboard: string) {
  
    return async (dispatch: ThunkDispatch) => {
    
      const data = {
      id: studentId,
      status: status,
      salary: salary,
      company_name: company_name,
      position: position,
      way: way,
      onboard_date: onboard
    }


    const res = await fetch(`${REACT_APP_API_SERVER}/student/staff/${studentId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/JSON",
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(data)
    })

    const result = await res.json();

    if (res.status === 200) {
      alert(result.message)
    } else {
      alert(result.message)
      dispatch(failed("@@STAFF/LOAD_NAME_FAILED", result))
    }
  }
}
