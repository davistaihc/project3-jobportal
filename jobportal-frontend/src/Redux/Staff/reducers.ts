import { IStaffState } from "./state";
import { IStaffAction } from "./actions";


const initialState = {
    staff_name: "",

}

export const staffReducer = (state: IStaffState = initialState, action: IStaffAction) => {
    switch (action.type) {
        case "@@STAFF/GET_NAME":
            return {
                ...state,
                staff_name: action.staff_name,
            };
        default:
            return state
    }
}
