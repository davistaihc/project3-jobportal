export function getStaffName (staff_name: string){
    return {
        type:"@@STAFF/GET_NAME" as "@@STAFF/GET_NAME",
        staff_name
    }
}
export function failed(type: FAILED, msg:string){
    return {
        type,
        msg
    }
}


type FAILED = "@@STAFF/LOAD_NAME_FAILED"; 

type actionCreator = typeof getStaffName | typeof failed
                           

export type IStaffAction = ReturnType<actionCreator>