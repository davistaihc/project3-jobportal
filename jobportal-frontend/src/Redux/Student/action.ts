import { Student } from "./reducer";

export function loadStudents(students: Student[]) {
    return {
        type: "@@STUDENTS/LOAD_STUDENTS" as "@@STUDENTS/LOAD_STUDENTS",
        students
    }
}

export const setSelectedViewStudent = (student: Student | undefined ) => {
    return {
       type: "@@JOB/SET_SELECT_JOB" as "@@JOB/SET_SELECT_JOB",
       student,
    }
 }

 export function ShowEditStudentForm(expanded: boolean) {
    return {
       type: "@@EDIT_FORM_EXPANDED" as "@@EDIT_FORM_EXPANDED",
       expanded
    }
 }

export function loadStudentsStatus(students: Student[]) {
    return {
        type: "@@STUDENTS_STATUS/LOAD_STUDENTS" as "@@STUDENTS_STATUS/LOAD_STUDENTS",
        students
    }
}


type actionCreator = typeof loadStudents | typeof loadStudentsStatus |typeof setSelectedViewStudent | typeof ShowEditStudentForm
export type StudentsActions = ReturnType<actionCreator>;