import { ThunkDispatch } from "../../store";
import { loadStudents, loadStudentsStatus } from "./action";


const { REACT_APP_API_SERVER } = process.env;

export function fetchStudents() {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/student/students`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
    });
    const json = await res.json();

    dispatch(loadStudents(json));
    dispatch(loadStudentsStatus(json));

  }
}



