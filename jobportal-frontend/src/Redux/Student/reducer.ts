import produce from "immer";
import { StudentsActions } from "./action";

export interface Student {
    id: number,
    surname: string;
    first_name: string;
    contact_number: string;
    resume: string;
    cohort: string;
    salary: string;
    status: string;
    graduate_date: Date;
    onboard_date: Date;
    position: string;
    company_name: string;
    way: string;
    cover_letter: string;
    portfolio: string;
    portfolio_url: string;
    userId: number;
    email: string;
    app_sent_success: boolean;
}

export interface StudentsState {
    studentsById: {
        [studentId: string]: Student
    },
    studentsByCohort: {
        [cohort: string]: number[]
    },
    studentsStatusByCohort: {
        [cohort: string]: number[]
    },
    selectedViewStudent: Student | undefined,
    expanded: boolean

}

export interface StudentResume {
    studentId: number,
    resume: string
}

const initialState: StudentsState = {
    studentsById: {},
    studentsByCohort: {},
    studentsStatusByCohort: {},
    selectedViewStudent: {} as Student | undefined,
    expanded: false


}

export function studentsReducer(state: StudentsState = initialState, action: StudentsActions): StudentsState {
    return produce(state, state => {
        switch (action.type) {
            case "@@STUDENTS/LOAD_STUDENTS":
                state.studentsByCohort = {};
                for (const student of action.students) {
                    state.studentsById[student.id] = student
                    if (state.studentsByCohort[student.cohort] == null) { // ?? 
                        state.studentsByCohort[student.cohort] = [];
                    }
                    state.studentsByCohort[student.cohort].push(student.id)
                }
                break;
            case "@@STUDENTS_STATUS/LOAD_STUDENTS":
                state.studentsStatusByCohort = {};
                for (const student of action.students) {
                    state.studentsById[student.id] = student
                    if (state.studentsStatusByCohort[student.cohort] == null) {
                        state.studentsStatusByCohort[student.cohort] = [];
                    }
                    state.studentsStatusByCohort[student.cohort].push(student.id)
                }
                break;
            case "@@JOB/SET_SELECT_JOB":
                state.selectedViewStudent = action.student;
                break;

            case "@@EDIT_FORM_EXPANDED":
                state.expanded = !state.expanded;
                break;
            default:
                return state




        }
    })
}