import { JobsState, Job } from './state'
import produce from 'immer'
import { JobAction } from './actions'




const initialState: JobsState = {
   jobsById: {},
   jobsByCompanyId: {},
   jobs: [],
   selectedViewJob: {} as Job | undefined,
}



export const JobsReducer = (state: JobsState = initialState, action: JobAction) => {
   return produce(state, state => {
      switch (action.type) {
         case "@@JOB/GET_ALL_JOB":
            for (const job of action.jobs) {
               state.jobsById[job.id] = job
            }
            state.jobs = action.jobs.map(job => job.id);
            break;

         case "@@JOB/SET_SELECT_JOB":
            state.selectedViewJob = action.job;
            break;

         case "@@JOB/UPDATE_STATUS_INFO":
            if (state.selectedViewJob != null) {
               state.selectedViewJob.is_approved = action.is_approved;
            }
            break;

         case "@@JOB/GET_JOB_BY_COMPANY_ID":
            for (const job of action.jobs) {
               state.jobsByCompanyId[job.company_id] = job
            }
            state.jobs = action.jobs.map(job => job.company_id)
            break;
      }
   })
}