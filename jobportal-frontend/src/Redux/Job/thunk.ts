import { ThunkDispatch } from '../../store';
import { getAllJob, failed, getJobByCompanyID, updateStatus } from './actions'
const { REACT_APP_API_SERVER } = process.env;

export const getAllJobThunk = () => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/job`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      });
      const jobs = await res.json();
      if (res.status === 200) {
         dispatch(getAllJob(jobs))
      } else {
         dispatch(failed("@@JOB/LOAD_INFO_FAILED", jobs.msg));
      }
   }
}

export function searchJobThunk(keyword: string) {

   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/job?keyword=${encodeURIComponent(keyword)}`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
         },
      });

      const jobs = await res.json();

      if (res.status === 200) {
         dispatch(getAllJob(jobs));
      } else {
         dispatch(failed("@@JOB/LOAD_INFO_FAILED", jobs.msg));
      }
   }
}

export const getJobByCompanyThunk = (companyId: number) => {
   return async (dispatch: ThunkDispatch) => {

      const res = await fetch(`${REACT_APP_API_SERVER}/job/company/${companyId}`, {
         headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
         },
      });
      const jobs = await res.json();
      if (res.status === 200) {
         dispatch(getJobByCompanyID(jobs))
      } else {
         dispatch(failed("@@JOB/LOAD_INFO_FAILED", jobs.msg));
      }
   }
}

export function deleteJobThunk(jobId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/job/delete/${jobId}`, {
         method: "PUT",
         headers: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`
         }
      })
      const result = await res.json()
      if(res.status === 200){
         dispatch(getAllJobThunk())
      }else{
         dispatch(failed("@@JOB/LOAD_INFO_FAILED", result))
      }

   }
}


export function updateJobStatusThunk(jobId: number, is_approved: boolean) {
   return async (dispatch: ThunkDispatch) => {

      const data = {
         isApproved: is_approved,
      }

      const res = await fetch(`${REACT_APP_API_SERVER}/job/update/${jobId}`, {
         method: "PUT",
         headers: {
            "Content-Type": "Application/JSON",
            'Authorization': `Bearer ${localStorage.getItem("token")}`
         },
         body: JSON.stringify(data)
      })

      const result = await res.json();

      if (res.status === 200) {
         dispatch(updateStatus(is_approved));
      } else {
         dispatch(failed("@@JOB/LOAD_INFO_FAILED", result))
      }
   }
}