
export interface Job {
   location: string 
   id: number;
   title: string;
   highlight: string;
   benefit: string ;
   responsibility: string ;
   file: string ;
   salary: string ;
   industry: string ;
   requirement: string ;
   availability: boolean;
   name: string ;
   type: string ;
   created_at: string;
   is_approved: boolean;
   company_id:number;
   companyId:number;

}


export interface JobsState {
   jobsById: {
      [jobId: string]: Job
   },
   jobsByCompanyId:{
      [companyId:string]:Job
   },
   jobs: number[],
   selectedViewJob: Job | undefined
}
