import { Job } from "./state";

export const getAllJob = (jobs: Job[]) => {
   return {
      type: "@@JOB/GET_ALL_JOB" as "@@JOB/GET_ALL_JOB",
      jobs
   }
}

export const setSelectedViewJob = (job: Job | undefined) => {
   return {
      type: "@@JOB/SET_SELECT_JOB" as "@@JOB/SET_SELECT_JOB",
      job
   }
}
export const getJobByCompanyID = (jobs:Job[]) =>{
   return{
      type:"@@JOB/GET_JOB_BY_COMPANY_ID" as "@@JOB/GET_JOB_BY_COMPANY_ID",
      jobs
   }
}

export function updateStatus (is_approved: boolean) {
   return {
       type: "@@JOB/UPDATE_STATUS_INFO" as "@@JOB/UPDATE_STATUS_INFO",
       is_approved
   };
}

export function failed(type: FAILED, msg: string) {
   return {
      type,
      msg
   }
}

type FAILED = "@@JOB/LOAD_INFO_FAILED" | typeof failed ;



type actionCreator = typeof getAllJob | typeof setSelectedViewJob | 
                     typeof getJobByCompanyID |typeof updateStatus;
                     
export type JobAction = ReturnType<actionCreator>