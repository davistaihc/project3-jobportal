import { STUDENT_JOB_MAPPING } from "./reducers";

export function loadEmailMappingToEmail(studentJobMapping: STUDENT_JOB_MAPPING[]) {
    return {
        type: "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING_APP_INFO" as "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING_APP_INFO",
        studentJobMapping
    }
}

type actionCreator = typeof loadEmailMappingToEmail;
export type StudentJobMappingActions = ReturnType<actionCreator>


