import { ThunkDispatch } from "../../store";
import { loadEmailMappingToEmail } from "./actions";

export function fetchEmailMappingInfo(jobId: number) {
   return async (dispatch: ThunkDispatch) => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/application/jobs/${jobId}/selectedStudents`,
         {
            headers: {
               'Content-Type': 'application/json',
               'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
         });
      const studentJobMapping = await res.json();

      dispatch(loadEmailMappingToEmail(studentJobMapping))
   }
}

