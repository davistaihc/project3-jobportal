import { StudentJobMappingActions } from "./actions";

export interface STUDENT_JOB_MAPPING {
    id: number;
    student_id: number;
    job_id: number;
    app_sent_success: boolean;
    surname: string | null;
    first_name: string | null;
    cohort: string;
    resume: string;
    resume_url: string;
    name: string;
    email: string;
    position: string;
    contact_person: string;
    title: string;
}

export interface StudentJobMappingState {
    studentJobMapping: STUDENT_JOB_MAPPING[];
}

const initialState: StudentJobMappingState = {
    studentJobMapping: []
}

export const StaffApplicationReducer = (oldState: StudentJobMappingState = initialState, action: StudentJobMappingActions): StudentJobMappingState => {
    switch (action.type) {
        case "@@EMAIL_MAPPING/LOAD_EMAIL_MAPPING_APP_INFO":
            return {
                ...oldState,
                studentJobMapping: action.studentJobMapping,

            };
        default:
            return oldState
    }
}
