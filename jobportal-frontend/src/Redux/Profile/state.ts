export interface StudentState {
    studentId: number 
    surname: string
    first_name: string 
    email: string 
    contact_number: string 
    cohort: string 
    status: string 
    resume?: string 
    resume_url?:string
    salary:string
    portfolio?:string
    portfolio_url?:string
    graduate_date:string
    onboard_date:string
    position:string
    company_name:string
    way:string
    cover_letter:string
    cover_letter_url:string
}






