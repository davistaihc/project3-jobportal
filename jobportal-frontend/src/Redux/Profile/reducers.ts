import { StudentState } from "./state";
import { StudentAction } from "./actions";


const initialState = {
    studentId: parseInt(""),
    surname: "",
    first_name: "",
    email: "",
    contact_number: "",
    cohort: "",
    status: "",
    resume: "",
    resume_url:"",
    salary: "",
    portfolio:"",
    portfolio_url:"",
    graduate_date:"",
    onboard_date:"",
    position:"",
    company_name:"",
    way:"",
    cover_letter:"",
    cover_letter_url:"",
}

export const studentReducer = (state: StudentState = initialState, action: StudentAction): StudentState => {
    switch (action.type) {
        case "@@STUDENT/LOAD_INFO":
            return {
                ...state,
                studentId: action.studentId,
                surname: action.surname,
                first_name: action.first_name,
                email: action.email,
                contact_number: action.contact_number,
                cohort: action.cohort,
                status: action.status,
                resume: action.resume,
                resume_url:action.resume_url,
                salary: action.salary,
                portfolio:action.portfolio,
                portfolio_url:action.portfolio_url,
                graduate_date:action.graduate_date,
                onboard_date:action.onboard_date,
                position:action.position,
                company_name:action.company_name,
                way:action.way,
                cover_letter:action.cover_letter,
                cover_letter_url:action.cover_letter_url,
            };
        case "@@STUDENT/EDIT_INFO":
            return {
                ...state,
                first_name: action.first_name,
                surname: action.surname,
                contact_number: action.contact_number,
                status:action.status,
                salary:action.salary,
                company_name:action.company_name,
                position:action.position,
                way:action.way,
                graduate_date:action.graduate_date,
                onboard_date:action.onboard_date,
                cover_letter:action.cover_letter
            }
        case "@@STUDENT/UPLOAD_RESUME":
            return {
                ...state,
                resume: action.resume
            }
        case "@@STUDENT/UPLOAD_PORTFOLIO":
            return {
                ...state,
                portfolio: action.portfolio
            }
            case "@@STUDENT/UPDATE_COVER_LETTER":
                return {
                    ...state,
                    cover_letter: action.cover_letter
                }
        default:
            return state
    }
}
