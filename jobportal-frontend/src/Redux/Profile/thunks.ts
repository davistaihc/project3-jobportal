import { ThunkDispatch, RootState } from "../../store";
import { failed, editInfo, getStudentProfile, UploadResume, UploadPortfolio, UpdateCoverLetter } from "./actions";

const { REACT_APP_API_SERVER } = process.env;



export function getStudentProfileThunk() {
  return async (dispatch: ThunkDispatch,getState: () => RootState) => {
    
    const token  = getState().auth.token
    // console.log(token)

    let res ;
    
    if(token){
      res = await fetch(`${REACT_APP_API_SERVER}/current/profile`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      });
    }

    const result = await res?.json();


    if (res && res.status === 200) {
      dispatch(getStudentProfile(
        result.studentId,
        result.surname,
        result.first_name,
        result.email,
        result.contact_number,
        result.cohort,
        result.status,
        result.resume,
        result.salary,
        result.company_name,
        result.position,
        result.way,
        result.portfolio,
        result.portfolio_url,
        result.resume_url,
        result.graduate_date,
        result.onboard_date,
        result.cover_letter,
        result.cover_letter_url));
    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result));
    }
  }
}


export function updateCoverLetterThunk(studentId: number, cover_letter: string) {
  return async (dispatch: ThunkDispatch) => {

    const data = {
      cover_letter: cover_letter
    }

    const res = await fetch(`${REACT_APP_API_SERVER}/student/coverLetter/${studentId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/JSON",
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(data)
    })

    const result = await res.json();

    if (res.status === 200) {
      dispatch(UpdateCoverLetter(result.cover_letter))
    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }


  }

}

export function editInfoThunk(
  studentId: number,
  first_name: string,
  surname: string,
  status: string,
  contact_number: string,
  salary: string,
  company_name: string,
  position: string,
  way: string,
  onboard: string
) {
  return async (dispatch: ThunkDispatch) => {


    const data = {
      id: studentId,
      first_name: first_name,
      surname: surname,
      status: status,
      contact_number: contact_number,
      salary: salary,
      company_name: company_name,
      position: position,
      way: way,
      onboard_date: onboard
    }

    const res = await fetch(`${REACT_APP_API_SERVER}/student/${studentId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/JSON",
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(data)
    })

    const result = await res.json();

    if (res.status === 200) {
      dispatch(editInfo(
        result.first_name,
        result.surname,
        result.contact_number,
        result.status,
        result.salary,
        result.company_name,
        result.position,
        result.way,
        result.graduate_date,
        result.onboard_date,
        result.cover_letter));
    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }


  }
}



export function uploadResumeThunk(studentId: number | null, data: FormData) {
  return async (dispatch: ThunkDispatch) => {


    const res = await fetch(`${REACT_APP_API_SERVER}/student/resume/${studentId}`,
      {
        method: "PUT",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: data
      })
    const result = await res.json()
    if (res.status === 200) {
      dispatch(UploadResume(result.resume))
      alert(result.msg)
      
    } else {
      alert(result.msg)
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }


  }
}
export function deleteResumeThunk(studentId: number | null) {
  return async (dispatch: ThunkDispatch) => {
    const data = {
      studentId: studentId
    }
    const res = await fetch(`${REACT_APP_API_SERVER}/student/delete/resume/${studentId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "Application/JSON",
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(data)
      })
    const result = await res.json()
    if (res.status === 200) {
      dispatch(UploadResume(result.resume))
      alert("Delete Success")
    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }
  }
}
export function deletePortfolioThunk(studentId: number | null) {
  return async (dispatch: ThunkDispatch) => {
    const data = {
      studentId: studentId
    }
    const res = await fetch(`${REACT_APP_API_SERVER}/student/delete/portfolio/${studentId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "Application/JSON",
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(data)
      })
    const result = await res.json()
    if (res.status === 200) {
      dispatch(UploadResume(result.resume))
      alert("Delete Success")
    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }
  }
}


export function uploadPortfolioThunk(studentId: number | null, data: FormData) {
  return async (dispatch: ThunkDispatch) => {



    const res = await fetch(`${REACT_APP_API_SERVER}/student/portfolio/${studentId}`,
      {
        method: "PUT",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: data
      })

    const result = await res.json()

    if (res.status === 200) {
      dispatch(UploadPortfolio(result.portfolio))
      alert(result.msg)

    } else {
      alert(result.msg)
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", result))
    }

  }
}


export function downloadFileThunk(filename: string) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/student/download?filename=${filename}`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
      })

  


    if (res.status === 200) {
      const url = res.url
      const authHeader = `Bearer ${localStorage.getItem("token")}`
      const options = {
        headers: {
          Authorization: authHeader
        }
      };
      
      fetch(url, options)
        .then(res => res.blob())
        .then(blob => {
          var file = window.URL.createObjectURL(blob);
          window.location.assign(file);
        });


    } else {
      dispatch(failed("@@STUDENT/LOAD_INFO_FAILED", "Download Fail"))
    }
  }
}
