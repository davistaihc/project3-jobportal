export function getStudentProfile(studentId: number, surname: string, first_name: string, email: string, contact_number: string, cohort: string, status: string, resume: string, salary: string, company_name: string, position: string, way: string, portfolio: string, portfolio_url: string, resume_url: string, graduate_date:string,onboard_date:string ,cover_letter:string
    ,cover_letter_url:string) {
    return {
        type: "@@STUDENT/LOAD_INFO" as "@@STUDENT/LOAD_INFO",
        studentId,
        surname,
        first_name,
        email,
        contact_number,
        cohort,
        status,
        resume,
        resume_url,
        portfolio,
        portfolio_url,
        graduate_date,
        onboard_date,
        cover_letter,
        cover_letter_url,
        salary,
        company_name,
        position,
        way
    }
}
export function failed(type: FAILED, msg: string) {
    return {
        type,
        msg
    }
}

export function editInfo(first_name: string, surname: string , contact_number: string,  status: string,  salary: string, company_name: string, position: string, way: string, graduate_date:string,onboard_date:string,cover_letter:string) {
    return {
        type: "@@STUDENT/EDIT_INFO" as "@@STUDENT/EDIT_INFO",
        first_name,
        surname,
        contact_number,
        status,
        salary,
        company_name,
        position,
        way,
        graduate_date,
        onboard_date,
        cover_letter
    };
}

export function UploadResume(resume: string ,) {
    return {
        type: "@@STUDENT/UPLOAD_RESUME" as "@@STUDENT/UPLOAD_RESUME",
        resume
    }
}


export function UploadPortfolio(portfolio: string) {
    return {
        type: "@@STUDENT/UPLOAD_PORTFOLIO" as "@@STUDENT/UPLOAD_PORTFOLIO",
        portfolio
    }
}
export function UpdateCoverLetter(cover_letter:string) {
    return {
        type: "@@STUDENT/UPDATE_COVER_LETTER" as "@@STUDENT/UPDATE_COVER_LETTER",
        cover_letter
    }
}

type FAILED = "@@STUDENT/LOAD_INFO_FAILED";

type actionCreator = typeof getStudentProfile | typeof failed | typeof editInfo | typeof UploadResume |  typeof UploadPortfolio | typeof UpdateCoverLetter


export type StudentAction = ReturnType<actionCreator>