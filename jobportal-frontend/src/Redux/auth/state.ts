export interface IAuthState {
  token: string | null 
  userId: number | null 
  studentId: number | null 
  email: string |null 
  isAuthenticated: boolean
  roleId: number | null

}