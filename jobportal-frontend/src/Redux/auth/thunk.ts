import { ThunkDispatch } from "../../store";
import { push } from "connected-react-router";
import { login, loginFailed, logout } from "./actions";


interface IRegisterInfo {
  email:string,
  password:string,
  name:string,
  position:string,
  confirmPassword:string,
  contact_number:string,
  contact_person:string,
  location:string,
  website:string,
  company_background:string,
  is_partner:boolean
}


export function loginGitlabThunk(code: string) {
  return async (dispatch: ThunkDispatch) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login/loginGitlab`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        code: code
      })
    })

    const json = await res.json();

    if (json.token != null) {
      // dispatch login success with token
      localStorage.setItem('token', json.token)
      localStorage.setItem('userId', json.userId);
      localStorage.setItem('email', json.email);
      localStorage.setItem('roleId', json.roleId);
      localStorage.setItem('studentId',json.studentId)

      dispatch(login(json.token, json.userId, json.email, json.studentId, json.roleId));
      dispatch(push('/frontpage'))
    } else {
      dispatch(loginFailed(json.msg));
    }
  }
}
export function RegisterThunk(
  email:string,
  password:string,
  name:string,
  position:string,
  confirmPassword:string,
  contact_number:string,
  contact_person:string,
  location:string,
  website:string,
  company_background:string,
  is_partner:boolean
) {
  return async (dispatch: ThunkDispatch) => {

    const data:IRegisterInfo = {
      email,
      password,
      confirmPassword,
      name,
      position,
      contact_number,
      contact_person,
      location,
      website,
      company_background,
      is_partner,
    }

    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login/company/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })

    const result = await res.json();


    if(res.status === 200 ){
      alert("Your Registration has been submitted! Thank you very much!")
      dispatch(push(`/`));
    }else{
      dispatch(loginFailed(result.message))
      alert(result.message)
    }
  }
}

export function loginThunk(email:string , password:string){
  return async (dispatch:ThunkDispatch) =>{
    
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login/company`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password:password
      })
    })

    const result = await res.json();
    // console.log(result)
    if(res.status !== 200){
      alert(result.message)
      return 
    }
   
    if (result.token != null) {
      // dispatch login success with token
      localStorage.setItem('token', result.token)
      localStorage.setItem('userId', result.userId);
      localStorage.setItem('email', result.email);
      localStorage.setItem('companyId', result.companyId);
      localStorage.setItem('roleId', result.roleId);
      localStorage.setItem('companyName' , result.companyName)

      dispatch(login(result.token, result.userId, result.email, result.studentId, result.roleId));

      dispatch(push(`/company/dashboard`));
  
    } else {
      dispatch(loginFailed(result.message));
    }
  }
}



export function logoutThunk() {
  return async (dispatch: ThunkDispatch) => {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('studentId');
    localStorage.removeItem('email');
    localStorage.removeItem('roleId');
    localStorage.removeItem('staffId');
    localStorage.removeItem('companyName')
    localStorage.removeItem('companyId')
    dispatch(logout());
  }
}
