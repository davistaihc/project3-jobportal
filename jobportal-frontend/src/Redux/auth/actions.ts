

export function login(token:string, userId:number, email:string, studentId:number, roleId:number){
    return {
        type: "@@auth/LOGIN" as "@@auth/LOGIN",
        token,
        userId,
        email,
        studentId,
        roleId,
    };
}

export function loginFailed(msg:string){
    return {
        type: "@@auth/LOGIN_FAILED" as "@@auth/LOGIN_FAILED",
        msg 
    }
}

export function CompanyLogin(token:string, userId:number, email:string, companyId:number, roleId:number){
    return {
        type: "@@auth/LOGIN" as "@@auth/LOGIN",
        token,
        userId,
        email,
        companyId,
        roleId,
    };
}


export function logout(){
    return {
        type: "@@auth/LOGOUT" as "@@auth/LOGOUT"
    }
}

type actionCreators = typeof login | 
                      typeof loginFailed | 
                      typeof logout;

export type IAuthActions = ReturnType<actionCreators>;