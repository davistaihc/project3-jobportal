import { IAuthState } from "./state";
import { IAuthActions } from "./actions";

//ver 1
// const initialState = {
//   token: localStorage.getItem('token'),
//   userId: (localStorage.getItem('token') ? Number(localStorage.getItem('currentUserId')) : null),
//   studentId: (localStorage.getItem('token') ? Number(localStorage.getItem('studentId')) : null),
//   email: (localStorage.getItem('token') ? localStorage.getItem('currentEmail') : null),
//   isAuthenticated: (localStorage.getItem('token') !== null),
// }


//ver 2

const initialState = {
  token: localStorage.getItem('token'),
  userId: (localStorage.getItem('token') ? Number(localStorage.getItem('userId')) : null),
  studentId: (localStorage.getItem('token') ? Number(localStorage.getItem('studentId')) : null),
  email: (localStorage.getItem('token') ? localStorage.getItem('email') : null),
  isAuthenticated: (localStorage.getItem('token') !== null),
  roleId: (localStorage.getItem('roleId') ? Number(localStorage.getItem('roleId')) : null),
  staffId:(localStorage.getItem('staffId')?Number(localStorage.getItem('staffId')):null),
  // companyId:(localStorage.getItem('companyId')?Number(localStorage.getItem('companyId')):null),

}


export function authReducer(state: IAuthState = initialState, actions: IAuthActions) {
  switch (actions.type) {
    case "@@auth/LOGIN":
      return {
        ...state,
        token:actions.token,
        userId: actions.userId,
        studentId: actions.studentId,
        email: actions.email,
        isAuthenticated: true,
        roleId: actions.roleId
      };
    case "@@auth/LOGIN_FAILED":
      return {
        ...state,
        isAuthenticated: false,
        msg: actions.msg
      };
    case "@@auth/LOGOUT":
      return {
        ...state,
        token: null,
        userId: null,
        studentId: null,
        email: null,
        isAuthenticated: false,
        roleId: null
      };
    default:
      return state
  };
}


