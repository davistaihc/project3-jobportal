import { ThunkDispatch, RootState } from '../../store'
import { fetchEmailMappingInfo } from '../StaffApplication/thunk';

const { REACT_APP_API_SERVER } = process.env;

export interface IApplication {
    studentId: number,
    jobId: number,
    files: string[],
    subject: string,
    portfolio?: string,
    portfolio_url: string,
    cover_letter: string,
    resumeName: string,
    resume_url: string
}


export function sentEmailByStudentThunk(studentId: number, jobId: number, resumeName: string, subject: string, portfolioName: string | undefined, portfolio_url: string, cover_letter: string, resume_url: string, resume_original: string) {
    return async (dispatch: ThunkDispatch) => {
        try {
            const data: IApplication = {
                studentId: studentId,
                jobId: jobId,
                files: [resumeName],
                subject: subject,
                portfolio: portfolioName,
                portfolio_url: portfolio_url,
                cover_letter: cover_letter,
                resumeName: resume_original,
                resume_url: resume_url
            }

            if (portfolioName !== undefined) {
                data.files.push(portfolioName)
            }

            const res = await fetch(`${REACT_APP_API_SERVER}/application/student/email/${studentId}/${jobId}`, {
                method: "POST",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem("token")}`
                },
                body: JSON.stringify(data)
            });

            const result = await res.json()
            if (res.status === 200) {
                alert("Success ! Good Luck!")
            } else {
                alert(result)
            }

        } catch (e) {
            // console.log(e)
            alert("Fail to send email, Please Contact us .")
        }
    }
}

// export function sentEmailStaffThunk(resumes: string[], cover_letter: string, jobId: number, companyEmail: string) {
export function sentEmailStaffThunk(cover_letter: string, jobId: number, companyEmail: string, subject: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {


            const data = {
                // resumes: resumeName,
                cover_letter: cover_letter,
                jobId: jobId,
                companyEmail: companyEmail,
                subject: subject
            }

            const res = await fetch(`${REACT_APP_API_SERVER}/application/staff/email/${jobId}`, {
                method: "POST",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${getState().auth.token}`
                },
                body: JSON.stringify(data)
            });

            const result = await res.json()
            if (res.status === 200) {
                alert("Email has been sent!")
                dispatch(fetchEmailMappingInfo(jobId))
            } else {
                // console.log(result)
                alert(result.message)
            }


        } catch (e) {
            // console.log(e)
            alert("Fail to send email, Please Contact IT for support")
        }


    }
} 
