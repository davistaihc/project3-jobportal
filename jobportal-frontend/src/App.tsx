import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-calendar/dist/Calendar.css';
import "./App.scss";
import { Container } from "react-bootstrap";
import { Switch, Route } from "react-router-dom";
import Profile from "./Components/Profile";
import ApplicationRecord from "./Components/ApplicationRecord";
import { LoginGitLab } from "./Components/LoginGitlab";
import { Homepage } from "./Components/Homepage";
import { JobInfo } from "./Components/JobInfo";
import { NavBar } from "./Components/Navbar";
import StaffApplicationMapping from "./Components/StaffApplicationMapping";
import { PrivateRoute } from "./Components/PrivateRoute";
import { NoMatch } from "./Components/NoMatch";
import { Company } from "./Components/Company";
import ChartCohortStatus from "./Components/ChartCohortStatus";
import { StudentListStaff } from "./Components/StudentListStaff";
import { FrontPage } from "./Components/FrontPage";
import { CompanyDetail } from "./Components/CompanyDetail";
import CreateCompany from "./Components/CreateCompany";
import { PublicLogin } from "./Components/PublicLogin";
import { CompanyDashboard } from "./Components/CompanyDashboard";


function App() {

  return (
    <div className="App">
      <Container fluid>
        <NavBar />
      </Container>
      <div id="body">
        <Container fluid>
          <Switch>
            <Route path="/" exact><Homepage /></Route>
            <Route path="/login/gitlab" exact><LoginGitLab /></Route>
            <Route path="/public/login" exact component={PublicLogin} />
            <Route exact path="/employers" component={CreateCompany} />
            <PrivateRoute path="/company/dashboard" exact component={CompanyDashboard} />
            <PrivateRoute path="/frontpage" exact={true} component={FrontPage} />
            <PrivateRoute path="/current/profile/" component={Profile} />
            <PrivateRoute path="/application_record" component={ApplicationRecord} />
            <PrivateRoute path="/job" component={JobInfo} />
            <PrivateRoute path="/staff_application" component={StaffApplicationMapping} />
            <PrivateRoute path="/chart" component={ChartCohortStatus} />
            <PrivateRoute exact path="/company" component={Company} />
            <PrivateRoute exact path="/company/detail/:id" component={CompanyDetail} />
            <PrivateRoute path="/student_list" component={StudentListStaff} />
            <Route path="/404" component={NoMatch} />
            <Route component={NoMatch} />
          </Switch>
        </Container>
      </div>
    </div>
  );
}

export default App;
