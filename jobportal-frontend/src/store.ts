import { JobsReducer } from './Redux/Job/reducers';
import { JobsState } from './Redux/Job/state';
import { SideBarState, SideBarReducer } from './Redux/SideBar/reducer';
import { createStore, combineReducers, compose, applyMiddleware, AnyAction } from "redux";
import { createBrowserHistory } from 'history'
import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router'
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk'
import logger from 'redux-logger';
import { StudentState } from './Redux/Profile/state';
import { studentReducer } from './Redux/Profile/reducers';
import { StudentAction } from './Redux/Profile/actions';
import { StudentApplicationState, studentApplicationReducer } from './Redux/ApplicationRecord/reducers';
import { StudentApplicationActions } from './Redux/ApplicationRecord/actions';
import { IAuthState } from './Redux/auth/state';
import { IAuthActions } from './Redux/auth/actions';
import { authReducer } from './Redux/auth/reducers';
import { CohortsState, cohortsReducer } from './Redux/cohorts/reducer';
import { studentsReducer, StudentsState } from './Redux/Student/reducer';
import { StudentsActions } from './Redux/Student/action';
import { CohortActions } from './Redux/cohorts/action';
import { EmailMappingState, emailMappingReducer } from './Redux/EmailMapping/reducers';
import { IStaffState } from './Redux/Staff/state';
import { IStaffAction } from './Redux/Staff/actions';
import { staffReducer } from './Redux/Staff/reducers';
import { ICompanyState } from './Redux/Company/state';
import { ICompanyAction } from './Redux/Company/actions';
import { CompanyReducer } from './Redux/Company/reducers';
import { statisticReducer, StudentsStatusState } from './Redux/Statistic/reducer';
import { StudentsStatusAction } from './Redux/Statistic/action';
import { StudentJobMappingActions } from './Redux/StaffApplication/actions';
import { StudentJobMappingState, StaffApplicationReducer } from './Redux/StaffApplication/reducers';


declare global {

   interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
   }
}

export const history = createBrowserHistory();

export interface RootState {
   auth: IAuthState,
   jobs: JobsState,
   sideBar: SideBarState,
   student: StudentState,
   students: StudentsState,
   router: RouterState,
   studentApplications: StudentApplicationState,
   cohorts: CohortsState,
   cohortList: CohortsState,
   emailMapping: EmailMappingState,
   staff: IStaffState,
   company: ICompanyState,
   studentsAvailable: StudentsStatusState,
   studentsNotAvailable: StudentsStatusState,
   studentsEmployed: StudentsStatusState,
   studentJobMapping: StudentJobMappingState
   cohortSalary: StudentsStatusState,
}

export type RootAction = IAuthActions | IStaffAction | ICompanyAction | AnyAction | StudentAction | StudentApplicationActions | StudentsActions | CohortActions | StudentsStatusAction | StudentJobMappingActions;

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootAction>

const reducer = combineReducers<RootState>({
   cohorts: cohortsReducer,
   cohortList: cohortsReducer,
   emailMapping: emailMappingReducer,
   auth: authReducer,
   jobs: JobsReducer,
   sideBar: SideBarReducer,
   student: studentReducer,
   students: studentsReducer,
   studentApplications: studentApplicationReducer,
   staff: staffReducer,
   company: CompanyReducer,
   studentsAvailable: statisticReducer,
   studentsNotAvailable: statisticReducer,
   studentsEmployed: statisticReducer,
   cohortSalary: statisticReducer,
   studentJobMapping: StaffApplicationReducer,
   router: connectRouter(history)
})

const composeEnhancers = process.env.REACT_APP_CI ? compose : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = process.env.REACT_APP_CI ? createStore(reducer, composeEnhancers(
   applyMiddleware(thunk),
   applyMiddleware(routerMiddleware(history))
)) : createStore(reducer, composeEnhancers(
   applyMiddleware(thunk),
   applyMiddleware(logger),
   applyMiddleware(routerMiddleware(history))
))