import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import { LoginService } from './service/LoginService';
import { User } from './service/model';

const permit = new Bearer({
    query: "access_token"
})

export function isLoggedIn(loginService: LoginService) {
    return async (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) => {
        try {
            const token = permit.check(req);
            // console.log("Guard token")
            // console.log(token)

            if (!token) {
                return res.status(401).json({ msg: "Permission Denied1" });
            }
            const payload = jwtSimple.decode(token, jwt.jwtSecret);

            console.log("Guard payload")
            console.log(payload.id)

            const user: User = await loginService.getUser(payload.id);
            if (user) {
                req.user = user;

                console.log("Guard user")
                console.log(user)
                console.log("Guard req.user")
                console.log(req.user)

                return next();
            } else {
                return res.status(401).json({ msg: "Permission Denied2" });
            }
        } catch (e) {
            // console.log(e)
            return res.status(401).json({ msg: "Permission Denied3" });
        }
    }
}