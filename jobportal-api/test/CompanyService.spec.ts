import Knex from 'knex';
const knexConfig = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexConfig["testing"]); // Now the connection is a testing connection
import { CompanyService } from '../service/CompanyService';

describe("CompanyService", () => {
    let companyService: CompanyService;

    beforeEach(async () => {
        companyService = new CompanyService(knex);

        await knex.raw(`DELETE FROM student_job_mapping`);
        await knex.raw(`DELETE FROM job`);
        await knex.raw(`DELETE FROM company`);

        await companyService.addCompany({
            "name": "ABC Company",
            "position": "CTO",
            "contactNumber": "98765432",
            "contactPerson": "Alex",
            "location": "Tsuen Wan",
            "email": "alex@email.com",
            "website": "www.alex.com",
            "companyBackground": "Brand New Company",
            "isPartner": false

        });
    });

    it("should add 1 company", async () => {
        const company = (await companyService.getAllCompanies());
        console.log(company);
        expect(company.length).toBe(1);
    })

    afterAll(() => {
        knex.destroy();
    })
})

