import * as Knex from 'knex'
import { User } from './model';
import { role, status } from '../variables/variables';
import { hashPassword } from '../hash';

interface OpenId {
    name: string;
    email: string;
    sub: string;
    groups: string[];
}

export class LoginService {
    constructor(private knex: Knex) { }

    createUser = async (openId: OpenId) => {
        let cohorts: string[] = [];
        for (const group of openId.groups as string[]) {
            if (group.includes('tecky.io/archived/hk-map')) {
                const component = group.split('/');
                cohorts.push(component[2]);
            }
        }
        
        if (cohorts.length === 0) {
            return false;
        }

        const result = await this.knex.transaction(async trx => {
            try {
                const userRows = await trx.raw(`SELECT * FROM "user" WHERE gitlab_id = ?`, [parseInt(openId.sub)])
                const userRowsByEmail = await trx.raw('SELECT * FROM "user" WHERE email = ?', [openId.email])

                let user = userRows.rows[0] ? userRows.rows[0] : userRowsByEmail.rows[0]
               
                if (user == null || user === undefined) {
                    console.log("User is null ")
                    const result = await trx.raw(`INSERT INTO "user" (email, role_id, gitlab_id) VALUES (?, ?, ?) RETURNING id`, [openId.email, role.student, openId.sub])

                    user = {
                        userId: result.rows[0].id,
                        email: openId.email,
                        role_id: role.student,
                        gitlab_id: parseInt(openId.sub)
                    };

                    for (const cohort of cohorts) {
                        await trx.raw(`INSERT INTO student (first_name, cohort, student_status_id, user_id) VALUES (?, ?, ?, ?)`, [openId.name, cohort, status.Available, user.userId])
                    }
                    
                    await trx.commit(user);

                } else {
                  
                    console.log("User have email")

                    await trx.raw('UPDATE "user" SET gitlab_id = ? WHERE email = ?', [parseInt(openId.sub), openId.email])
                    const result = await trx.raw(`SELECT * FROM "user" WHERE email = ?`, [openId.email])

                    user = {
                        userId: result.rows[0].id,
                        email: openId.email,
                        role_id: role.student,
                        gitlab_id: parseInt(openId.sub)
                    };

                    for (const cohort of cohorts) {
                        await trx.raw(`UPDATE "student" SET cohort = ? , student_status_id = ? WHERE user_id = ? `, [ cohort, status.Available, user.userId])
                    }
                   
                    await trx.commit(user);
                    

                }
               
                
            } catch (e) {
                await trx.rollback();
                console.log(e)
            }
        })
        return result

    }

    createStaff = async (openId: OpenId) => {
        const result = await this.knex.transaction(async trx => {
            try {

                const userRows = await trx.raw(`SELECT * FROM "user" WHERE gitlab_id = ?`, [parseInt(openId.sub)])
                let user = userRows.rows[0]
                console.log(`Service create staff : ${user}`)
                if (user == null || user ===undefined) {
                    const result = await trx.raw(`INSERT INTO "user" (email, role_id, gitlab_id) VALUES (?, ?, ?) RETURNING id`, [openId.email, role.staff, openId.sub])

                    user = {
                        userId: result.rows[0].id,
                        email: openId.email,
                        role_id: role.staff,
                        gitlab_id: parseInt(openId.sub)
                    };

                    await trx.raw(`INSERT INTO staff (staff_name, user_id) VALUES (?, ?)`, [openId.name, user.userId])
                    return user
                }
                await trx.commit();
                return user
            } catch (e) {
                console.log(e)
                await trx.rollback();
            }
        })
        return result
    }

    getUser = async (id: number): Promise<User> => {
        const result = await this.knex.raw(/* SQL */`SELECT * FROM "user" WHERE id = ?`, [id]);
        return result.rows[0]
    }

    getUserByGitlabId = async (gitlabId: number): Promise<User> => {

        const result = await this.knex.raw(/* SQL */`
        SELECT 
        "user".id as "userId",
        "user".email,
        "user".gitlab_id, 
        student.id as "studentId",
        "user".role_id ,
        "user".created_at ,
        student.surname,
        student.first_name,
        student.contact_number,
        student.resume,
        student.resume_url,
        student.cohort,
        student.salary,
        student_status.status,
        student.graduate_date,
        student.on_board_date as "onboard_date",
        student.hired_position as "position",
        student.hired_company as "company_name",
        student.way_of_hired as "way",
        student.cover_letter ,
        student.portfolio,
        student.portfolio_url
        FROM "user" 
        JOIN student ON student.user_id = "user".id 
        JOIN student_status ON student.student_status_id = student_status.id
        WHERE gitlab_id = ?
        `, [gitlabId]);

        return result.rows[0]
    }

    getStaffByGitlabId = async (gitlabId: number): Promise<User> => {

        const result = await this.knex.raw(/* SQL */`
        SELECT "user".id as "userId", 
        email , gitlab_id, role_id, staff.id as "staffId",
        staff.staff_name
        FROM "user" 
        JOIN staff ON staff.user_id = "user".id 
        WHERE gitlab_id = ?
        `, [gitlabId]);

        return result.rows[0]
    }

    checkUserByEmail = async (email: string) => {

        const result = await this.knex.raw(/*SQL*/
            `SELECT "user".id as "userId",
        "user".email , role_id, password ,company.id as "companyId",
        company.name as "company_name"
        FROM "user"
        JOIN company ON company.user_id = "user".id 
        WHERE "user".email = ?`, [email]);
        console.log(result.rows[0])

        return result.rows[0]



    }


    createCompanyUser = async (email: string, password: string, name: string, position: string, contact_number: string, contact_person: string, location: string, website: string, company_background: string, is_partner: boolean) => {

        const result = await this.knex.transaction(async trx => {
            try {
                let user = await this.checkUserByEmail(email)
                if (!user) {
                    const hashedPassword = await hashPassword(password);
                    const result: any = await trx("user").insert({ email: email, role_id: role.other, password: hashedPassword }).returning("*")
                    console.log(result)
                    // question 
                    user = {
                        id: result[0].id,
                        userId: result[0].id,
                        email: email,
                        role_id: role.other,
                        gitlab_id: null
                    }
                    console.log(`Welcome ${result[0].email}`)
                    console.log(user)
                    await trx("company").insert({ name: name, email: email, position: position, contact_number: contact_number, contact_person: contact_person, location: location, website: website, company_background: company_background, is_partner: is_partner, user_id: user.id })

                    return user
                }

                await trx.commit();
                return user

            } catch (e) {
                await trx.rollback()
                return
            }
        })
        return result
    }


}