export interface Student {
    id?: number
    statusId: number
    email: string
    gitlabId: number
    contactNumber: string
    surname: string
    firstName: string
    resume: string
    cohort: string
    salary: number
}


export interface Job {
    id?: number
    title: string
    highlight: string
    benefit: string
    responsibility: string
    file: string
    salary: string
    industry: string
    requirement: string
    companyId: number
    employmentTypeId: number[]
    staffId: number
    availability: boolean
    createdAt: Date
    isApproved: boolean
}

export interface Company {
    id?: number
    name: string
    position: string
    contactNumber: string
    contactPerson: string
    location: string
    email: string
    website: string
    companyBackground: string
    isPartner: boolean
}

export interface Status {
    id?: number
    status: string
}

export interface StudentApplication {
    id: number;
    created_at: string;
    content: string;
    submitted_resume: string;
    job_id: string;
    title: string;
    responsibility: string;
    salary: string;
    file: string;
    company_id: string;
    name: string;
    location: string;
    status: string;
}

export interface User {
    id: number
    userId: number
    email: string
    gitlab_id: number | null 
    role_id: number
}

declare global {
    namespace Express {
        interface Request {
            user?: User
        }
    }
}

// export interface StaffApplication {
//     Id: number
//     staff_id: number
//     content: string
//     created_at: string
//     job_id: string
//     company_id: number
//     name: string
//     app_status: string
//     student_id: string
// }

export interface StudentJob {
    student_id: number;
    job_id: number;
    app_sent_success: boolean;
}

