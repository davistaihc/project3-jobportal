SELECT
    student_application.id,
    student_application.created_at,
    job_id,
    job.title,
    job.responsibility,
    job.salary,
    job.file,
    company_id,
    company.name,
    company.location,
    app_status.status
FROM student_Application
    INNER JOIN student ON student.id = student_application.student_id
    INNER JOIN job ON student_application.job_id = job.id
    INNER JOIN app_status ON student_application.app_status_id = app_status.id
    INNER JOIN company ON job.company_id = company.id
WHERE student_id = 3;


SELECT
    staff_application.id,
    staff_application.staff_id,
    staff_application.content,
    staff_application.created_at,
    company_id,
    company.name,
    app_status.status,
    staff_app_student_mapping.student_id
FROM staff_application
    INNER JOIN job ON staff_application.job_id = job.id
    INNER JOIN company ON job.company_id = company.id
    INNER JOIN staff_app_student_mapping ON staff_application.id = staff_app_student_mapping.staff_application_id
    INNER JOIN app_status ON staff_app_student_mapping.app_status_id = app_status.id
WHERE job.id = 3;

SELECT
    staff_app_student_mapping.student_id,
    staff_application.job_id
FROM staff_application
    INNER JOIN staff_app_student_mapping ON staff_application.id = staff_app_student_mapping.staff_application_id
WHERE staff_application.job_id = 3;


SELECT
    student_application.id,
    student_application.created_at,
    job_id,
    job.title,
    job.responsibility,
    job.salary,
    job."file",
    company_id,
    company.name,
    company.location,
    app_status.status
FROM student_application
    INNER JOIN student ON student.id = student_application.student_id
    INNER JOIN "user" ON student.user_id = "user".id
    INNER JOIN job ON student_application.job_id = job.id
    INNER JOIN company ON job.company_id = company.id
    INNER JOIN app_status ON student_application.app_status_id = app_status.id
WHERE "user".id = 7


SELECT
    COUNT (cohort)
FROM student
    INNER JOIN student_status ON student.student_status_id = student_status.id
WHERE student.cohort = 'hk-map-08-feb-20' AND student_status.id = 1;


SELECT
    student.id,
    student.surname,
    student.first_name,
    student.contact_number,
    student.resume,
    student.cohort,
    student.salary,
    student_status.status,
    student.graduate_date,
    student.on_board_date,
    student.hired_position,
    student.hired_company,
    student.way_of_hired,
    student.cover_letter,
    student.portfolio,
    student.portfolio_url,
    "user".id AS "user_id",
    "user".email
FROM student
    JOIN "student_status" ON student_status_id = student_status.id
    JOIN "user" on student.user_id = "user".id
WHERE student.id = 1


SELECT
    student_job_mapping.id,
    student_job_mapping.student_id AS student_id,
    student_job_mapping.job_id AS job_id,
    student_job_mapping.app_sent_success,
    student.surname,
    student.first_name,
    student.cohort,
    student.resume,
    student.resume_url,
    company.name,
    company.email,
    company.position
FROM student_job_mapping INNER JOIN student
    ON student_job_mapping.student_id = student.id
    INNER JOIN job
    ON student_job_mapping.job_id = job.id
    INNER JOIN company
    ON job.company_id = company.id
WHERE student_job_mapping.job_id = 12;


-- Testing staffemail sent, then update student job mapping record
select id, job_id, student_id, student_job_mapping
FROM student_job_mapping;

UPDATE student_job_mapping SET app_sent_success = FALSE WHERE student_job_mapping.job_id = 3;

UPDATE student_job_mapping SET app_sent_success = TRUE WHERE student_job_mapping.job_id = 3;


DELETE FROM student_job_mapping WHERE job_id = 5 AND student_id = 1;

update student_job_mapping set content = 'Application sent by Tecky', submitted_resume = 'Application sent by Tecky' where content = '';