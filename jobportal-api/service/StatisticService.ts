import * as Knex from 'knex';
import { StudentService } from './StudentService';

// const notAvailable = tables.STUDENT_NOT_AVAILABLE
// const employed = tables.STUDENT_EMPLOYED



export class StatisticService {

    constructor(private knex: Knex , private studentService:StudentService) {}


    async getCohortJobStatusByCohortAva(cohortName: string) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        COUNT (cohort)
        FROM student 
        INNER JOIN student_status ON student.student_status_id = student_status.id
        WHERE student.cohort = ? AND student_status.id = 1
        `, [cohortName])
        return result.rows;
    }

    async getCohortJobStatusByCohortNotAva(cohortName: string) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        COUNT (cohort)
        FROM student 
        INNER JOIN student_status ON student.student_status_id = student_status.id
        WHERE student.cohort = ? AND student_status.id = 2
        `, [cohortName])
        return result.rows;
    }

    async getCohortJobStatusByCohortEmployed(cohortName: string) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        COUNT (cohort)
        FROM student 
        INNER JOIN student_status ON student.student_status_id = student_status.id
        WHERE student.cohort = ? AND student_status.id = 3
        `, [cohortName])
        return result.rows;
    }

    async getCohortSalaryService() {


        // const result = await this.knex.raw(/*sql*/`
        // SELECT cohort
        // FROM student
        // GROUP BY cohort`)

        const result = await this.studentService.getAllCohorts()

        let cohorts = result
        const salary = []
        for (const co of cohorts) {
            // salary.push(
            let salary1 = await this.knex.raw(/*sql*/`
            SELECT 
            AVG (salary),'999999D' AS average_amount
            FROM student 
            WHERE student.cohort =?`, [co.cohort])

            let salaryOfCohort = salary1.rows[0].avg
            salary.push(Math.round(salaryOfCohort))
            // )

        }

        return salary
    }

}
