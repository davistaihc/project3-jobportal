import * as Knex from 'knex';
import Tables from '../utils/tables';
import { Company, Job } from './model';

const company = Tables.COMPANY
const job = Tables.JOB

export class CompanyService {

    constructor(private knex: Knex) { }

    //fot jest
    async getCompanyByName(companyId: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT * FROM ${company} 
        WHERE company.name = (?)`,
            [companyId]
        )
        return result.rows;
    }

    addCompany = async (body: Company) => {
        let isPartner = false
        const result = await this.knex.raw(/*sql*/`
        INSERT into ${company} 
        ("name", position, contact_number, contact_person, "location", email, website, company_background, is_partner)  
        VALUES (?,?,?,?,?,?,?,?,?) 
        RETURNING id`,
            [
                body.name,
                body.position,
                body.contactNumber,
                body.contactPerson,
                body.location,
                body.email,
                body.website,
                body.companyBackground,
                isPartner
            ]
        )
        console.log(result)
        return result.rows[0].id;
    }
         
    addJobByEmployers = async (body: Job) => {
        let isApproved = false
        let availability = true

        let types: number[] = body.employmentTypeId;
        console.log(types)
        for (const type of types) {
            await this.knex.raw(/*sql*/`
        INSERT into ${job} 
        (title, highlight, benefit, responsibility, salary, industry, requirement, company_id, employment_type_id, availability, is_approved)  
        VALUES (?,?,?,?,?,?,?,?,?,?,?)`,
                [
                    body.title,
                    body.highlight,
                    body.benefit,
                    body.responsibility,
                    body.salary,
                    body.industry,
                    body.requirement,
                    body.companyId,
                    type,
                    availability,
                    isApproved
                ]
            )
        }
    }

    async getCompanyById(companyId: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT * FROM ${company} 
        WHERE company.id = ?`,
            [companyId]
        )
        return result.rows;
    }

    async getAllCompanies() {
        const result = await this.knex.raw(/*sql*/`
        SELECT * FROM ${company}`
        )
        return result.rows;
    }

    async searchCompanyByKeyword(keyword: string) {
        const result = await this.knex.raw(/*sql*/`SELECT * FROM ${company}
        WHERE LOWER(company.name) LIKE LOWER('%${keyword}%')
        OR LOWER(company.location) LIKE LOWER('%${keyword}%')
        OR LOWER(company.contact_person) LIKE LOWER('%${keyword}%');`)

        return result.rows;
    }

    updateCompany = async (companyId: number, body: Company) => {
        await this.knex.raw(/*sql*/`
            UPDATE company
            SET 
            "name" = ?,
            position = ?,
            contact_number = ?,
            contact_person = ?,
            "location" = ?,
            email = ?,
            website = ?,
            company_background = ?,
            is_partner = ?
            WHERE id =?`,
            [body.name,
            body.position,
            body.contactNumber,
            body.contactPerson,
            body.location,
            body.email,
            body.website,
            body.companyBackground,
            body.isPartner, companyId])

    }

}
