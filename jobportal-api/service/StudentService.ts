import * as Knex from 'knex';
import { Student } from './model';

// const student = process.env.TB_STUDENT


export class StudentService {

    constructor(private knex: Knex) { }

    addStudent = async (body: Student) => {
        const result = await this.knex.raw(/*sql*/`
        INSERT into student 
        (email, gitlab_id, contact_number, surname, first_name, "resume", cohort, salary, status_id)  
        VALUES (?,?,?,?,?,?,?,?,?) 
        RETURNING id`,
            [
                body.email,
                body.gitlabId,
                body.contactNumber,
                body.surname,
                body.firstName,
                body.resume,
                body.cohort,
                body.salary,
                body.statusId
            ]
        )
        return result.rows[0].id;
    }

    async getAllStudents() {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student.id,
        student.surname,
        student.first_name,
        student.contact_number,
        student.resume,
        student.resume_url,
        student.cohort,
        student.salary,
        student_status.status,
        student.graduate_date,
        student.on_board_date as "onboard_date",
        student.hired_position as "position",
        student.hired_company as "company_name",
        student.way_of_hired as "way",
        student.cover_letter,
        student.portfolio,
        student.portfolio_url,
        "user".id AS "userId",
        "user".email
        FROM student
        JOIN "student_status" ON student_status_id = student_status.id
        JOIN "user" on student.user_id = "user".id
        ORDER BY student_status.status ASC,
        student.first_name ASC
        `)
        return result.rows;
    }

    async getStudentsById(studentId: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student.id,
        student.surname,
        student.first_name,
        student.contact_number,
        student.resume,
        student.resume_url,
        student.cohort,
        student.salary,
        student_status.status,
        student.graduate_date,
        student.on_board_date as "onboard_date",
        student.hired_position as "position",
        student.hired_company as "company_name",
        student.way_of_hired as "way",
        student.cover_letter,
        student.portfolio,
        student.portfolio_url,
        "user".id AS "userId",
        "user".email
        FROM student
        JOIN "student_status" ON student_status_id = student_status.id
        JOIN "user" on student.user_id = "user".id
        WHERE student.id = (?)
        `, [studentId])
        return result.rows[0];
    }

    async searchStudentsByKeyword(keyword: string) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student.id,
        student.surname,
        student.first_name,
        student.contact_number,
        student.resume,
        student.resume_url,
        student.cohort,
        student.salary,
        student_status.status,
        student.graduate_date,
        student.on_board_date,
        student.hired_position,
        student.hired_company,
        student.way_of_hired,
        student.cover_letter,
        student.portfolio,
        student.portfolio_url,
        "user".id AS "userId",
        "user".email
        FROM student
        JOIN "student_status" ON student_status_id = student_status.id
        JOIN "user" on student.user_id = "user".id
        WHERE to_tsvector(
            surname || ' ' || first_name || ' ' || cohort
        ) @@ to_tsquery(:keyword)
        ORDER BY student.first_name
        `, { keyword: keyword }
        );
        return result.rows
    }

    async getAllCohorts() {
        const result = await this.knex.raw(/*sql*/`
        SELECT cohort
        FROM student
        GROUP BY cohort 
        ORDER BY cohort ASC
        `)
        return result.rows;
    }

    updateStudentBasicInfo = async (studentId: number, first_name: string, surname: string, status: number, contact_number: string) => {
        await this.knex.raw(/*sql*/`
            UPDATE student
            SET first_name = ?,
                surname = ?,
                contact_number = ?,
                student_status_id = ?
            WHERE id = ?`,
            [first_name, surname, contact_number, status, studentId]
        )
        return
    }
    updateStudentGraduateDate = async (studentId: number, graduate_date: number) => {
        await this.knex.raw(/*sql*/`
            UPDATE student
            SET 
            graduate_date =?
            WHERE id = ?`,
            [graduate_date, studentId]
        )
        return
    }

    

    updateStudentStatus = async (studentId: number, status:number) => {
        await this.knex("student").update({ student_status_id :status}).where({ "id": studentId })
        return
    }

    updateStudentJobAppStatus = async(studentId:number, statusId:number , jobId:number) => {
        console.log(studentId,statusId,jobId)
       return await this.knex("student_job_mapping").update({app_status_id:statusId}).where({student_id:studentId}).where({job_id:jobId})
         
    }

    updateStudentEmployedInfo = async (studentId: number, salary: number | null, on_board_date: string |null, hired_position: string|null, hired_company: string |null, way: string | null) => {
        await this.knex("student").update({ "salary": salary, "on_board_date": on_board_date, "hired_position": hired_position, "hired_company": hired_company, "way_of_hired": way }).where({ "id": studentId })
        return
    }

    uploadStudentResume = async (studentId: number, resume: string, resume_url: string) => {
        console.log("Service! Resume")
        const result = await this.knex("student").update({ "resume": resume, "resume_url": resume_url }).where("id", studentId)
        console.log(result)
        return result
    }

    uploadStudentPortfolio = async (studentId: number, portfolio: string, portfolio_url: string) => {
        console.log("Service! Portfolio")
        const result = await this.knex("student").update({ "portfolio": portfolio, "portfolio_url": portfolio_url }).where("id", studentId)
        console.log(result)
        return result
    }

    uploadStudentCoverLetter = async (studentId: number, cover_letter: string) => {
        console.log("Service! Cover_letter")
        const result = await this.knex("student").update({ "cover_letter": cover_letter }).where("id", studentId)
        console.log(result)
        return result
    }


    deleteStudentResume = async (studentId: number) => {
        try {
            const result = await this.knex("student").update({ "resume": null, "resume_url": null }).where("id", studentId)
            console.log(result)
            return true
        } catch (e) {
            console.log(e)
            return false
        }
    }
    deleteStudentPortfolio = async (studentId: number) => {
        try {
            const result = await this.knex("student").update({ "portfolio": null, "portfolio_url": null }).where("id", studentId)
            console.log(result)
            return true
        } catch (e) {
            console.log(e)
            return false
        }
    }





}