import * as Knex from 'knex';
import Tables from '../utils/tables';
// import { StudentJob } from './model';

const emailApplication = Tables.STUDENT_JOB_MAPPING
const company = Tables.COMPANY
const job = Tables.JOB
const student = Tables.STUDENT
const appStatus = Tables.APP_STATUS



export class ApplicationService {

    constructor(private knex: Knex) { }

    

    async getAllApplications() {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student_job_mapping.id,
        student_job_mapping.created_at,
        job_id,
        job.title, 
        company_id,
        company.name,
        student.id,
        student.surname,
        student.first_name,
        student.cohort
        FROM ${emailApplication} 
        INNER JOIN ${student} ON student.id = student_job_mapping.student_id
        INNER JOIN ${job} ON student_job_mapping.job_id = job.id
        INNER JOIN ${company} ON job.company_id = company.id
        WHERE app_sent_success = TRUE
        ORDER BY created_at DESC
        `)
        return result.rows
    }

    async getStudentAllApplicationsByUserId(id: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student_job_mapping.id,
        student_job_mapping.created_at,
        student_job_mapping.content,
        student_job_mapping.submitted_resume,
        student_job_mapping.submitted_resume_url,
        student_job_mapping.app_sent_success,
        job_id,
        job.title, 
        job.responsibility, 
        job.requirement, 
        job.salary,
        company_id,
        company.name,
        company.location,
        app_status.status
        FROM ${emailApplication} 
        INNER JOIN ${student} ON student.id = student_job_mapping.student_id
        INNER JOIN "user" ON student.user_id = "user".id
        INNER JOIN ${job} ON student_job_mapping.job_id = job.id
        INNER JOIN ${company} ON job.company_id = company.id
        INNER JOIN ${appStatus} ON student_job_mapping.app_status_id = app_status.id
        WHERE "user".id = (?)
        ORDER BY created_at DESC
        `, [id])
        // console.log(result)
        return result.rows
    }


    async getStaffEmailInfoService(job_id: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student_job_mapping.id,
        student_job_mapping.student_id AS student_id,
        student_job_mapping.job_id AS job_id,
        student_job_mapping.app_sent_success,
        student.surname,
        student.first_name,
        student.cohort,
        student.resume,
        student.resume_url,
        company.name, 
        company.email, 
        company.position,
        company.contact_person,
        job.title
        FROM ${emailApplication} 
        INNER JOIN student 
        ON student_job_mapping.student_id = student.id
        INNER JOIN job
        ON student_job_mapping.job_id = job.id
        INNER JOIN company
        ON job.company_id = company.id
        WHERE ${emailApplication}.job_id = (?)
        `, [job_id])

        return result.rows

    }

    async getStaffAppByJobIdWithStudents(job_id: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT 
        student_job_mapping.student_id,
        student_job_mapping.job_id,
        student_job_mapping.app_sent_success,
        student.resume,
        student.resume_url
        FROM ${emailApplication} INNER JOIN student 
        ON student_job_mapping.student_id = student.id
        WHERE ${emailApplication}.job_id = (?)
        `, [job_id])

        return result.rows

    }



    async postStaffAppMappingByJobIdWithStudents(job_id: number, student_id: number) {
        let app_status_id = 1 //fix later
        let app_sent_success = false
        let content = "" //fix later

        const checkStudentAdded = await this.knex.raw(/*sql*/`SELECT*
        FROM ${emailApplication} 
        WHERE ${emailApplication}.job_id = (?)`, [job_id])

        const studentsJobStatus = checkStudentAdded.rows
        for (const student of studentsJobStatus) {
            if (job_id == student.job_id && student_id == student.student_id) {
                return false
            }
        }

        const checkStudentStatusResume = await this.knex.raw(/*sql*/`SELECT*
        FROM ${student} WHERE student.id = (?)`, [student_id])

        const studentResumeStatus = checkStudentStatusResume.rows
        console.log(checkStudentStatusResume.rows)
        for (const student of studentResumeStatus) {
            console.log(student.resume)
            if (student.resume === null || undefined) {
                return false
            }
        }
        for (const student of studentResumeStatus) {
            console.log(student.student_status_id)
            if (student.student_status_id !== 1) {
                return false
            }
        }

        const result = await this.knex.raw(/*sql*/`
            INSERT INTO ${emailApplication} (app_status_id, student_id,job_id, content,app_sent_success ) VALUES(?,?,?,?,?) RETURNING id`,
            [app_status_id, student_id, job_id, content, app_sent_success])
        console.log("mapping = " + result)

        const AllResult = await this.knex.raw(/*sql*/`SELECT*
            FROM ${emailApplication} 
            WHERE ${emailApplication}.job_id = (?)
            `, [job_id])
        return AllResult.rows
    }

    async removeMappingByJobIdStudentIdService(job_id: number, student_id: number) {

        const checkApplicationStatus = await this.knex.raw(/*sql*/`SELECT app_sent_success
        FROM ${emailApplication} WHERE job_id = ? AND student_id =?`, [job_id, student_id])

        const checkResult = checkApplicationStatus.rows
        console.log(checkResult[0].app_sent_success)
        if (checkResult[0].app_sent_success === true) {
            return false
        }

        const result = await this.knex.raw(/*sql*/`
            DELETE FROM ${emailApplication} WHERE job_id = ? AND student_id =?`, [job_id, student_id])
        console.log(result.rows)
        return result.rows
    }

    getJobEmailById = async (job_id: number) => {
        return await this.knex("job")
            .select("company.email")
            .innerJoin("company", { "job.company_id": "company.id" })
            .where("job.id", job_id).returning("*")

    }

    async updateAppStatusAfterEmailSent(job_id: number) {
        try {
            const result = await this.knex.raw(/*sql*/`
            UPDATE ${emailApplication} SET app_sent_success = TRUE, content = 'Application sent by Staff', submitted_resume = 'Application sent by Staff' WHERE app_sent_success = FALSE AND ${emailApplication}.job_id = (?)`, [job_id])
            console.log(result)
            return result.rows
        }
        catch (e) {
            console.log(e)
            return false
        }
    }

    async getStudentInfoByJobId(job_id: number) {
        return await this.knex("student_job_mapping").select("*").where({ "job_id": job_id, "app_sent_success": false })
    }

    async getStudentInfoById(student_id: number) {
        return await this.knex("student").select("*").where({ "id": student_id })
    }

    updateApplication = async (studentId: number, job_id: number, app_status_id: number, submitted_resume: string, submitted_resume_url: string, submitted_cover_letter: string, submitted_portfolio: string | undefined, submitted_portfolio_url: string, app_sent_success: boolean) => {
        try {
            console.log(submitted_cover_letter)
            if (submitted_portfolio !== undefined) {
                const result = await this.knex(`student_job_mapping`)
                    .insert({ "job_id": job_id, "student_id": studentId, "app_status_id": app_status_id, "submitted_resume": submitted_resume, "submitted_resume_url": submitted_resume_url, "content": submitted_cover_letter, "submitted_portfolio": submitted_portfolio, "submitted_portfolio_url": submitted_portfolio_url, "app_sent_success": app_sent_success, })
                console.log(result)
                return true
            } else {
                const result = await this.knex(`student_job_mapping`)
                    .insert({ "job_id": job_id, "student_id": studentId, "app_status_id": app_status_id, "submitted_resume": submitted_resume, "submitted_resume_url": submitted_resume_url, "content": submitted_cover_letter, "submitted_portfolio": null, "submitted_portfolio_url": submitted_portfolio_url, "app_sent_success": app_sent_success })
                console.log(result)
                return true
            }
        } catch (e) {
            console.log(e)
            return false

        }
    }
}

//    updateAppStatusAfterEmailSent = async (job_id: number) => {

//      const result = await this.knex.transaction(async trx => {
//      try{
//        await this.trx.raw(/*sql*/`
//             UPDATE ${emailApplication} SET app_sent_success = TRUE WHERE app_sent_success = FALSE AND ${emailApplication}.job_id = (?)`, [job_id])

//         const result = await this.trx.raw(/*sql*/`
//             UPDATE ${emailApplication} SET content = 'Application sent by Staff', SET submitted_resume = 'Application sent by Staff' WHERE AND ${emailApplication}.job_id = (?)`, [job_id])

//         await trx.commit();
//      } catch (e) {
//          await trx.rollback();
//      }    
//     })

//         return result.rows

// }


// const result = await this.knex.transaction(async trx => {
//     try {
//         const userRows = await trx.raw(`SELECT * FROM "user" WHERE gitlab_id = ?`, [parseInt(openId.sub)])
//         let user = userRows.rows[0]
//         if (user == null) {
//             const result = await trx.raw(`INSERT INTO "user" (email, role_id, gitlab_id) VALUES (?, ?, ?) RETURNING id`, [openId.email, role.student, openId.sub])


//             for (const cohort of cohorts) {
//                 await trx.raw(`INSERT INTO student (first_name, cohort, student_status_id, user_id) VALUES (?, ?, ?, ?)`, [openId.name, cohort, status.Available, user.userId])
//             }
//             return user
//         }
//         await trx.commit();
//         return user
//     } catch (e) {
//         await trx.rollback();
//     }
// }