import * as Knex from 'knex';
import Tables from '../utils/tables';
import { Job } from './model';

const job = Tables.JOB
const company = Tables.COMPANY
const employment_type = Tables.EMPLOYMENT_TYPE

export class JobService {

    constructor(private knex: Knex) { }

    addJob = async (body: Job) => {
        const result = await this.knex.raw(/*sql*/`
        INSERT into ${job} 
        (title, highlight, benefit, responsibility, file, salary, industry, requirement, company_id, employment_type_id, staff_id, availability)  
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?) 
        RETURNING id`,
            [
                body.title,
                body.highlight,
                body.benefit,
                body.responsibility,
                body.file,
                body.salary,
                body.industry,
                body.requirement,
                body.companyId,
                body.employmentTypeId,
                body.staffId,
                body.availability,
            ]
        )
        return result.rows[0].id;
    }


    async getAllJobs() {
        const result = await this.knex.raw(/*sql*/`SELECT job.id, company.id as "companyId", company.name, company.location, title, highlight, responsibility, requirement, benefit, salary, industry, job.availability, job.created_at, job.is_approved, employment_type.type FROM ${job}
        INNER JOIN ${company} ON ${job}.company_id = company.id
        INNER JOIN ${employment_type} ON job.employment_type_id = employment_type.id
        ORDER BY created_at DESC
        `)
        return result.rows;
    }

    async searchJobsByKeyword(keyword: string) {
        const result = await this.knex.raw(/*sql*/`SELECT job.id, company.id as "companyId", company.name, company.location, title, highlight, responsibility, requirement, benefit, salary, industry, job.availability, job.created_at, job.is_approved, employment_type.type FROM ${job}
        INNER JOIN ${company} ON ${job}.company_id = company.id
        INNER JOIN ${employment_type} ON job.employment_type_id = employment_type.id
        WHERE LOWER(company.name) LIKE LOWER(?)
        OR LOWER(title) LIKE LOWER('%${keyword}%')
        OR LOWER(requirement) LIKE LOWER('%${keyword}%')
        OR LOWER(company.location) LIKE LOWER('%${keyword}%'); `, ['%' + keyword + '%'])

        return result.rows;
    }

    async getJobById(jobId: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT
        job.title,
            job.highlight,
            job.benefit,
            job.responsibility,
            job.file,
            job.salary,
            job.industry,
            job.requirement,
            job.company_id,
            job.availability,
            job.is_approved, 
            job.created_at
        FROM job
        WHERE job.id = (?)
            `, [jobId])
        return result.rows[0];
    }

    async getJobByCompanyId(companyId: number) {
        const result = await this.knex.raw(/*sql*/`
        SELECT
        job.title,
            job.highlight,
            job.benefit,
            job.responsibility,
            job.file,
            job.salary,
            job.industry,
            job.requirement,
            job.company_id,
            job.availability,
            job.is_approved, 
            job.created_at
        FROM job
        WHERE job.company_id = (?)
            `, [companyId])
        return result.rows[0];
    }

    updateJobApproval = async (jobId: number, body: Job) => {
        await this.knex.raw(/*sql*/`
        UPDATE ${job}
        SET is_approved = ?
                WHERE id =? `,
            [body.isApproved, jobId])
    }

    updateJob = async (jobId: number, body: Job) => {
        await this.knex.raw(/*sql*/`
        UPDATE ${job}
        SET title = ?,
            highlight = ?,
            benefit = ?,
            responsibility = ?,
            "file" = ?,
            salary = ?,
            industry = ?,
            requirement = ?,
            company_id = ?,
            employment_type_id = ?,
            is_approved = ?, 
            "availability" =?
                WHERE id =? `,
            [body.title, body.highlight, body.benefit, body.responsibility,
            body.file, body.salary, body.industry, body.requirement,
            body.companyId, body.employmentTypeId, body.isApproved, body.availability, jobId])

    }

    deleteJob = async (jobId:number) =>{
        try {
            const result = await this.knex("job")
            .update({"availability":false})
            .where({"id":jobId})
            console.log(result)
            return true
        } catch (e) {
            console.log(e)
            return false
        }
        
    }

}