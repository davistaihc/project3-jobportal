import * as Knex from "knex";
import xlsx from 'xlsx';
import path from 'path'

interface STUDENT_STATUS {
    status: string;
}

interface APP_STATUS {
    status: string;
}

interface EMPLOYMENT_TYPE {
    type: string;
}

interface COMPANY {
    name: string;
    position: string;
    contact_number: string;
    contact_person: string;
    location: string;
    email: string;
    website: string;
    company_background: string;
    is_partner: boolean;
}

interface ROLE {
    role: string;
}

interface USER {
    email: string;
    gitlab_id: string;
    role_id: number;
}

interface STAFF {
    staff_name: string;
    user_id: number;
}

interface STUDENT {
    surname: string;
    first_name: string;
    contact_number: string;
    resume: string;
    resume_url: string;
    cohort: string;
    salary: number;
    student_status_id: number;
    user_id: number;
    graduate_date: Date;
    on_board_date: Date;
    hired_position: string;
    hired_company: string;
    way_of_hired: string;
    cover_letter: string;
    cover_letter_url: string;
    portfolio: string;
    portfolio_url: string;

}

interface JOB {
    title: string;
    highlight: string;
    benefit: string;
    responsibility: string;
    file: string;
    salary: string;
    industry: string;
    requirement: string;
    availability: boolean;
    company_id: number;
    employment_type_id: number;
    is_approved: boolean;
}

interface EMAIL_TEMPLATE {
    content: string;
    data_set_id: number;
}

interface STUDENT_JOB_MAPPING {
    job_id: number;
    student_id: number;
    app_status_id: number;
    submitted_resume: string;
    submitted_resume_url: string;
    submitted_cover_letter: string;
    submitted_cover_letter_url: string;
    submitted_portfolio: string;
    submitted_portfolio_url: string;
    remarks: string;
    app_sent_success: boolean;
    content: string;
}


export async function seed(knex: Knex): Promise<any> {
    // await knex('student_application').del();
    await knex('student_job_mapping').del();
    await knex('email_template').del();
    await knex('job').del();
    await knex('student').del();
    await knex('staff').del();
    await knex('company').del();
    await knex('user').del();
    await knex('role').del();
    await knex('employment_type').del();
    await knex('app_status').del();
    await knex('student_status').del();

    const workbook = xlsx.readFile(path.join(__dirname, '../importData/jobportal-database1.xlsx'));

    const student_status: STUDENT_STATUS[] = xlsx.utils.sheet_to_json(workbook.Sheets['student_status']);
    await knex("student_status").insert(student_status);
    
    const app_status: APP_STATUS[] = xlsx.utils.sheet_to_json(workbook.Sheets['app_status']);
    await knex("app_status").insert(app_status);

    const employment_type: EMPLOYMENT_TYPE[] = xlsx.utils.sheet_to_json(workbook.Sheets['employment_type']);
    await knex("employment_type").del();
    await knex("employment_type").insert(employment_type);

    const company: COMPANY[] = xlsx.utils.sheet_to_json(workbook.Sheets['company']);
    await knex("company").del();
    await knex("company").insert(company);

    const role: ROLE[] = xlsx.utils.sheet_to_json(workbook.Sheets['role']);
    await knex("role").del();
    await knex("role").insert(role);

    const user: USER[] = xlsx.utils.sheet_to_json(workbook.Sheets['user']);
    await knex("user").del();
    await knex("user").insert(user);

    const staff: STAFF[] = xlsx.utils.sheet_to_json(workbook.Sheets['staff']);
    await knex("staff").del();
    await knex("staff").insert(staff);

    const student: STUDENT[] = xlsx.utils.sheet_to_json(workbook.Sheets["student"]);
    await knex("student").del();
    await knex("student").insert(student);

    const job: JOB[] = xlsx.utils.sheet_to_json(workbook.Sheets['job']);
    await knex("job").del();
    await knex("job").insert(job);

    const email_template: EMAIL_TEMPLATE[] = xlsx.utils.sheet_to_json(workbook.Sheets['email_template']);
    await knex("email_template").del();
    await knex("email_template").insert(email_template);

    const staff_app_student_mapping: STUDENT_JOB_MAPPING[] = xlsx.utils.sheet_to_json(workbook.Sheets['student_job_mapping']);
    await knex("student_job_mapping").del();
    await knex("student_job_mapping").insert(staff_app_student_mapping);

}

