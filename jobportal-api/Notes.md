(V) Views
- (V) DTO Data Transfer Objects << Data Format to Frontend (without functions) >> Schema

(C) Routers
- (C) Routes
- (C) Controllers
- (C) Validators

(C+M) Services
- (C) Services (without Knex) << application/business Logic
- (M) Repository (with Knex) << NO Logic at all
- (M) Model (Object Relation Mapping) << Business Logic



Method 1:
https://grpc.io/

Method 2:
backend
```
const studentList = new StudentListResponse(students)
res.json(studentList)
```

frontend
```
const list:StudentListResponse = await res.json()
```



arr.filter((element, i) => i % 2 == 0).map(element => element.name)
