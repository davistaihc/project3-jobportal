import Knex from "knex";

export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('student_status', table => {
        table.increments();
        table.string('status');
    })

    await knex.schema.createTable('app_status', table => {
        table.increments();
        table.string('status');
    })

    await knex.schema.createTable('employment_type', table => {
        table.increments();
        table.string('type');
    })



    await knex.schema.createTable('role', table => {
        table.increments();
        table.string('role');
    })

    await knex.schema.createTable('user', table => {
        table.increments();
        table.string('email');
        table.string('password')
        table.integer('gitlab_id');
        table.integer('role_id').unsigned();
        table.foreign('role_id').references('role.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('company', table => {
        table.increments();
        table.string('name');
        table.string('position');
        table.string('contact_number');
        table.string('contact_person');
        table.string('location');
        table.string('email');
        table.string('website');
        table.text('company_background')
        table.boolean('is_partner');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('user.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('staff', table => {
        table.increments();
        table.string('staff_name');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('user.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('student', table => {
        table.increments();
        table.string('surname');
        table.string('first_name');
        table.string('contact_number');
        table.string('resume');
        table.string('resume_url');
        table.string('cohort');
        table.integer('salary');
        table.integer('student_status_id').unsigned();
        table.foreign('student_status_id').references('student_status.id');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('user.id');
        table.timestamp('graduate_date');
        table.timestamp('on_board_date');
        table.string('hired_position');
        table.string('hired_company');
        table.string('way_of_hired');
        table.text('cover_letter');
        table.string('cover_letter_url');
        table.string('portfolio');
        table.string('portfolio_url');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('job', table => {
        table.increments();
        table.string('title');
        table.text('highlight');
        table.text('benefit');
        table.text('responsibility');
        table.string('file');
        table.string('salary');
        table.string('industry');
        table.text('requirement');
        table.boolean('availability');
        table.integer('company_id').unsigned();
        table.foreign('company_id').references('company.id');
        table.integer('employment_type_id').unsigned();
        table.foreign('employment_type_id').references('employment_type.id');
        table.boolean('is_approved');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('email_template', table => {
        table.increments();
        table.text('content');
        table.integer('data_set_id');
    })

    await knex.schema.createTable('student_job_mapping', table => {
        table.increments();
        table.integer('job_id').unsigned();
        table.foreign('job_id').references('job.id');
        table.integer('student_id').unsigned();
        table.foreign('student_id').references('student.id');
        table.integer('app_status_id').unsigned();
        table.foreign('app_status_id').references('app_status.id');
        table.string('submitted_resume');
        table.string('submitted_resume_url');
        table.text('submitted_cover_letter');
        table.string('submitted_cover_letter_url');
        table.string('submitted_portfolio');
        table.string('submitted_portfolio_url');
        table.string('remarks');
        table.boolean('app_sent_success');
        table.text('content');
        table.timestamps(false, true);
    })

    return;
}

export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('student_job_mapping');
    await knex.schema.dropTable('email_template');
    await knex.schema.dropTable('job');
    await knex.schema.dropTable('student');
    await knex.schema.dropTable('staff');
    await knex.schema.dropTable('company');
    await knex.schema.dropTable('user');
    await knex.schema.dropTable('role');
    await knex.schema.dropTable('employment_type');
    await knex.schema.dropTable('app_status');
    await knex.schema.dropTable('student_status');
    return;
}

