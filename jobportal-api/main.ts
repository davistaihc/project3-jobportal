import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import Knex from 'knex';
import dotenv from 'dotenv'
import os from 'os'

//Import Service an d Router
import { StudentService } from './service/StudentService';
import { StudentRouter } from './router/StudentRouter';
import { JobService } from './service/JobService';
import { JobRouter } from './router/JobRouter';
import { CompanyService } from './service/CompanyService';
import { CompanyRouter } from './router/CompanyRouter';
import { ApplicationService } from './service/ApplicationService';
import { ApplicationRouter } from './router/ApplicationRouter';
import { LoginService } from './service/LoginService';
import { LoginRouter } from './router/loginRouter';
import { StatisticService } from './service/StatisticService';
import { StatisticRouter } from './router/StatisticRouter';
import { CurrentUserRouter } from './router/CurrentUserRouter';
import { isLoggedIn } from './guard';
import multer from 'multer';
import multerS3 from 'multer-s3'
import aws from 'aws-sdk'
import path from 'path';

dotenv.config()


const app = express();
const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
export const tmp = path.join(os.tmpdir())

app.use(cors(
  { 
    origin: [
      'https://api.jobs.tecky.io/',
      'https://cdn.jobs.tecky.io/',
      'https://jobs.tecky.io/'
    ],
  }
));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))


//multer S3 
const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: `${process.env.AWS_DEFAULT_REGION}`
});


const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: `${process.env.AWS_S3_BUCKET}`,
        metadata: (req,file,cb)=>{
            cb(null,{fieldName: file.fieldname});
        },
        key: (req,file,cb)=>{
            cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})


app.use(express.static(tmp))

export const studentService = new StudentService(knex);
const studentRouter = new StudentRouter(studentService,upload)
export const jobService = new JobService(knex);
const jobRouter = new JobRouter(jobService)
export const companyService = new CompanyService(knex);
const companyRouter = new CompanyRouter(companyService)
export const applicationService = new ApplicationService(knex);
const applicationRouter = new ApplicationRouter(applicationService)
export const loginService = new LoginService(knex);
const loginRouter = new LoginRouter(loginService)
const currentUserRouter = new CurrentUserRouter(loginService)
export const statisticService = new StatisticService(knex ,studentService);
const statisticRouter = new StatisticRouter(statisticService)


//Router
const isLoggedInGuard = isLoggedIn(loginService)
app.use('/login', loginRouter.router());
app.use('/application', isLoggedInGuard, applicationRouter.router());
app.use('/current', isLoggedInGuard, currentUserRouter.router());
app.use('/student', isLoggedInGuard,studentRouter.router());
app.use('/job', isLoggedInGuard, jobRouter.router());
app.use('/company', companyRouter.router());
app.use('/statistic', statisticRouter.router());




const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log("This did not add * to cors")
  console.log(`Listening at ${port}`)
})