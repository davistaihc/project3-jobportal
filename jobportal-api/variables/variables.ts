export const role = {
    student: 2,
    staff: 1,
    other:3
}

export const status = {
    Available:1,
    Not_Available:2,
    Employed:3
}