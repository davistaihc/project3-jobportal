import express from 'express';
import { Request, Response } from 'express';
import { ApplicationService } from '../service/ApplicationService';
import dotenv from 'dotenv'
import sgMail from '@sendgrid/mail'
import aws from 'aws-sdk'
import { tmp } from '../main';
import fs from 'fs'

dotenv.config()

const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_DEFAULT_REGION
});

interface IAttachment {
    content: string,
    filename: string,
    type: string,
    disposition: string,
}

interface IApplication {
    studentId: number,
    jobId: number,
    files: string[],
    subject: string,
    portfolio?: string,
    portfolio_url: string,
    cover_letter: string,
    resumeName: string,
    resume_url: string
}

const app_status = {
    Waiting_for_response: 1,
    Got_interview: 2,
    Accept_offer: 3,
    Reject_offer: 4
}


export class ApplicationRouter {
    constructor(private applicationService: ApplicationService) { }

    router() {
        const router = express.Router();
        router.get('/student', this.getStudentApplicationsByUserId)
        router.get('/jobs/:id/students', this.getStaffAppByJobId)
        router.get('/jobs/:id/selectedStudents', this.getStaffEmailInfo)
        router.post('/jobs/:id/students', this.postStaffAppMappingByJobId)
        router.post('/student/email/:studentId/:jobId', this.sendEmailByStudent)
        router.post('/staff/email/:jobId', this.sendEmailByStaff)
        router.put('/jobs/:id/students/removed', this.removeMappingByJobIdStudentId)
        return router;
    }


    private getStudentApplicationsByUserId = async (req: Request, res: Response) => {

        try {

            if (req.user == null) {

                return res.status(401).json({ message: "not user" })

            } else {

                let roleId = req.user.role_id
                if (roleId === 2) {
                    let id = req.user.id
                    const result = await this.applicationService.getStudentAllApplicationsByUserId(id);
                    if (result.length == 0) {
                        return console.log("no record found")
                    }
                    return res.status(200).json(result);
                } else if (roleId === 1) {
                    const result = await this.applicationService.getAllApplications();

                    if (result.length == 0) {
                        console.log("no record found")
                    }
                    return res.status(200).json(result);
                }
            }
        }
        catch (err) {
            console.log(err)
            return res.status(500).json({ message: 'internal server error' });
        }
    }

    private getStaffAppByJobId = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            console.log(id)
            const students_jobs = await this.applicationService.getStaffAppByJobIdWithStudents(id);
            res.json(students_jobs)
        }
        catch (err) {
            console.log(err)
            res.status(500).json({ message: 'internal server error, not able to get staff Application' });
        }
    }

    private getStaffEmailInfo = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            console.log("params = " + id)
            if (id == NaN) {
                id = 0
                console.log(id)
            } else id = id
            const studentJobMapping = await this.applicationService.getStaffEmailInfoService(id);

            res.json(studentJobMapping)

        }
        catch (err) {
            console.log(err)
            res.status(500).json({ message: 'internal server error, not able to get email mapping info' });
        }
    }

    private postStaffAppMappingByJobId = async (req: Request, res: Response) => {
        try {
            let job_id = parseInt(req.params.id)
            // console.log("postStaffAppMappingByJobId-jobId=" + job_id)
            req.body.student_id
            // console.log("postStaffAppMappingByJobId-student_id=" + req.body.student_id)

            const students_jobs = await this.applicationService.postStaffAppMappingByJobIdWithStudents(
                job_id,
                req.body.student_id,
            );

            // if (result.exist === false) {
            // res.status(403).json({ msg: "invalid student id" });
            // } else {
            console.log("app router response = " + students_jobs)
            res.json(students_jobs)
            // res.json(students_jobs.filter(student_job => student_job.job_id === parseInt(req.params.id)))
            // }
        }
        catch (err) {
            console.log(err)
            res.status(500).json({ message: 'internal server error, not able to post Application' });
        }
    }

    private removeMappingByJobIdStudentId = async (req: Request, res: Response) => {
        try {
            let job_id = parseInt(req.params.id)
            req.body.student_id

            const removed_result = await this.applicationService.removeMappingByJobIdStudentIdService(
                job_id,
                req.body.student_id,
            );

            console.log("removed record = " + removed_result)
            res.json(removed_result)

        }
        catch (err) {
            console.log(err)
            res.status(500).json({ message: 'internal server error, not able to removed student' });
        }
    }


    private getJobEmailById = async (jobId: number) => {
        const result = await this.applicationService.getJobEmailById(jobId)
        return result[0].email
    }

    private downloadFile = (file: string) => {
        // console.log(files)
        return new Promise((resolve, reject) => {
            try {
                console.log(tmp)
                const params = { Bucket: `${process.env.AWS_S3_BUCKET}`, Key: `${file}` }
                const writeFile = fs.createWriteStream(`${tmp}/${file}`)
                const s3Promise = s3.getObject(params).promise();
                s3Promise.then((data) => {
                    console.log(data)
                    writeFile.write(data.Body, () => {
                        writeFile.end();
                    });
                }).then(() => {
                    writeFile.on('finish', () => {
                        console.log("Success Write Resume")
                        resolve()
                    })
                }).catch((err) => {
                    console.log(err)
                    reject(err);
                })


            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }




    private sendEmailByStudent = async (req: Request, res: Response) => {
        try {
            const { studentId, files, subject, cover_letter, jobId, portfolio, portfolio_url, resumeName, resume_url }: IApplication = req.body
            const status: number = app_status.Waiting_for_response
            console.log(`StudentId is ${studentId}`)
            console.log(`Files are  ${files}`)
            console.log(req.body)
            console.log(resumeName)
            console.log(portfolio)

            //setKey
            // this is the job's email.
          
            const Key = process.env.SENDGRID_API_KEY
            sgMail.setApiKey(`${Key}`);
            //Put email resume
            const emailContent: any = [];
            for (const file of files) {
                const attachmentPath = `${tmp}/${file}`
                console.log(attachmentPath)
                console.log(`Cover_letter is ${cover_letter}`)
                await this.downloadFile(file)
                // const attachment = fs.readFileSync(attachmentPath).toString("base64")
                const attachment = (await this.readStream(attachmentPath))
                if(file.startsWith("resume")){
                    emailContent.push(
                        {
                            content: attachment,
                            filename: resumeName,
                            type: "application/pdf",
                            disposition: "attachment"
                        }
                    )}else{
                        emailContent.push(
                            {
                                content: attachment,
                                filename: portfolio,
                                type: "application/pdf",
                                disposition: "attachment"
                            })
                    }
                
            }

            const jobEmail = await this.getJobEmailById(jobId)
            console.log(jobEmail)
            
            const companyEmail = `nigellamaximusclark@gmail.com`

            const text = cover_letter.split(/\r/).join(`<br/>`)

            let msg;

            if (portfolio) {
                msg = {
                    to: `${companyEmail}`,
                    from: `${process.env.TECKY_EMAIL}`,
                    subject: `${subject}`,
                    text: `${cover_letter}`,
                    html: `
                    <pre>${text}</pre>
                    `,
                    attachments: emailContent
                };
            } else {
                msg = {
                    to: `${companyEmail}`,
                    from: `${process.env.TECKY_EMAIL}`,
                    subject: `${subject}`,
                    text: `${cover_letter}`,
                    html: `
                    <pre>${text}</pre>
                    <br/>
                    <span>Portfolio Link: ${portfolio_url}</span>
                    `,
                    attachments: emailContent
                };
            }



            sgMail.send(msg).then(async result => {
                result[0].statusCode === 202 && await this.applicationService.updateApplication(studentId, jobId, status, resumeName, resume_url, cover_letter, portfolio, portfolio_url, true) &&
                    res.status(200).json({ message: "Success" })

            },
                err => res.status(500).json({ message: err }));


        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Sorry ! There is an error for sending email!" })
        }
    }


    private readStream = async (file: string) =>
        new Promise<string>((resolve) => {
            const result = fs.readFileSync(file).toString("base64")
            resolve(result)
        });



    private sendEmailByStaff = async (req: Request, res: Response) => {
        try {

            const { subject, cover_letter, jobId, companyEmail } = req.body

            const studentsAlreadyMapped: any[] = await this.applicationService.getStudentInfoByJobId(jobId)

            const files: string[] = []

            for (const studentMapped of studentsAlreadyMapped) {
                const infoOfStudentMapped = await this.applicationService.getStudentInfoById(studentMapped.student_id)
                console.log("Resume URL of Student Mapped = " + infoOfStudentMapped[0].resume_url)

                const MappedStudentResume = infoOfStudentMapped[0].resume_url
                if (MappedStudentResume == null || MappedStudentResume == undefined) {
                    res.status(401).json({ message: "All selected students must have resumes before sent!" })
                    return
                }
                else {
                    files.push(MappedStudentResume)
                    console.log(MappedStudentResume)
                }

                const portfolio = infoOfStudentMapped[0].portfolio_url
                if (portfolio !== null && portfolio !== undefined) {
                    console.log(portfolio)
                    files.push(portfolio)
                }
            }
            console.log("files = " + files)

            const originalFiles = []
            if (files.length <= 0) {
                res.status(401).json({ message: "All students application already sent!" })
                return
            }
            for (const file of files) {
                originalFiles.push(file.split("/", -1)[4])
                console.log(originalFiles)
            }

            console.log("Mapped Student Files (resumes + portfolio)= " + originalFiles)
            console.log("sendEmailByStaff email address = " + companyEmail)
            console.log("sendEmailByStaff email content = " + cover_letter)
            console.log("sendEmailByStaff jod Id = " + jobId)

            const Key = process.env.SENDGRID_API_KEY
            sgMail.setApiKey(`${Key}`);

            const emailContent: IAttachment[] = [];

            for (const originalFile of originalFiles) {
                const attachmentPath: string = `${tmp}/${originalFile}`
                await this.downloadFile(originalFile)
                const attachment = (await this.readStream(attachmentPath))
                emailContent.push(
                    {
                        content: attachment,
                        filename: originalFile,
                        type: "application/pdf",
                        disposition: "attachment"
                    }
                )
            }

            const jobEmail = await this.getJobEmailById(jobId)
            console.log(jobEmail)

            const msg = {
                to: `${companyEmail}`,
                from: `${process.env.TECKY_EMAIL}`,
                subject: `${subject}`,
                text: `${cover_letter}`,
                attachments: emailContent
            };
            // console.log(msg)
            sgMail.send(msg).then(async result => {
                result[0].statusCode === 202 &&
                    await this.applicationService.updateAppStatusAfterEmailSent(jobId) &&
                    res.status(200).json({ message: "email has been sent" }) ||
                    result[0].statusCode === 401 && res.status(401).json({ message: "Fail" })
            }, err => console.log(err));
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Sorry! The mail has not been sent. Internal Error" })
        }
    }



}

