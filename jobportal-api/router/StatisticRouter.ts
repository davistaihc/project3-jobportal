import { StatisticService } from '../service/StatisticService';
import { Request, Response } from 'express';
import express from 'express'

export class StatisticRouter {
    constructor(private statisticService: StatisticService) { }

    router() {
        const router = express.Router();
        router.get('/studentAva/:cohort', this.getCohortJobStatusAva)
        router.get('/studentNotAva/:cohort', this.getCohortJobStatusNotAva)
        router.get('/studentEmployed/:cohort', this.getCohortJobStatusEmployed)
        router.get('/cohortSalary', this.getCohortSalary)
        return router;
    }
    private getCohortJobStatusAva = async (req: Request, res: Response) => {
        try {
            const cohort = req.params.cohort
            const result = await this.statisticService.getCohortJobStatusByCohortAva(cohort);
            res.json(result)
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }

    }
    private getCohortJobStatusNotAva = async (req: Request, res: Response) => {
        try {
            const cohort = req.params.cohort
            const result = await this.statisticService.getCohortJobStatusByCohortNotAva(cohort);
            res.json(result)
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }

    }
    private getCohortJobStatusEmployed = async (req: Request, res: Response) => {
        try {
            const cohort = req.params.cohort
            const result = await this.statisticService.getCohortJobStatusByCohortEmployed(cohort);
            res.json(result)
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }

    }

    private getCohortSalary = async (req: Request, res: Response) => {
        try {
            const result = await this.statisticService.getCohortSalaryService();
            res.json(result)
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }

    }

}
