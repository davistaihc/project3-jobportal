import express from 'express'
import { Request, Response } from 'express';
import { LoginService } from '../service/LoginService';
import fetch from 'node-fetch'
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { checkPassword } from '../hash';

export class LoginRouter {
  constructor(private loginService: LoginService) { }

  router() {
    const router = express.Router();
    router.post('/loginGitlab', this.loginByGitlab)
    router.post('/company', this.login)
    router.post('/company/register', this.register)
    return router
  }

  private loginByGitlab = async (req: Request, res: Response) => {
    try {
      // console.log("LoginRouter req.body.code")
      // console.log(req.body.code)
      if (req.body.code == null || req.body.code == "") {
        res.status(401).json({ message: "No auth code proivded." })
        return;
      }

      const fetchRes = await fetch("https://gitlab.com/oauth/token", {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          client_id: process.env.GITLAB_CLIENT_APP_ID,
          client_secret: process.env.GITLAB_CLIENT_APP_SECRET,
          code: req.body.code,
          grant_type: 'authorization_code',
          redirect_uri: process.env.GITLAB_REDIRECT_URI
        })
      })

      console.log("LoginRouter fetchRes")
      console.log(fetchRes)

      const tokenJson = await fetchRes.json();
      if (tokenJson.access_token == null || tokenJson.access_token == "") {
        res.status(401).json({ message: "Access code not found." })
        return;
      }
      console.log("LoginRouter tokenJson from gitlab")
      console.log(tokenJson)

      const openidRes = await fetch("http://gitlab.com/oauth/userinfo", {
        headers: {
          'Authorization': `Bearer ${tokenJson.access_token}`
        }
      })
      console.log("LoginRouter openidRes")
      console.log(openidRes)

      const openId = await openidRes.json();

      console.log("LoginRouter openId")
      console.log(openId)

      let user;

     if(openId.email.includes('tecky.io')){
        user = await this.loginService.getUserByGitlabId(openId.sub)
      }else{
         user = await this.loginService.getUserByGitlabId(openId.sub)
      }

    
      console.log("LoginRouter user")
      console.log(user)

      if (!user) {
        if (openId.email.includes('tecky.io') ) {
          console.log(`Create Staff!`)
          const result = await this.loginService.createStaff(openId);
          if (result) {
            user = result
          } else {
            res.status(401).json({ message: "Please ask Alex for help!" })
            return
          }
        } else {
          console.log("Create Student")
          const result = await this.loginService.createUser(openId)

          if (result) {
            user = result
          } else {
            res.status(401).json({ message: "Please Join our course!" })
            return
          }
        }
      }

      console.log("LoginRouter roleId")
      console.log(user.role_id)

      const payload = {
        id: user.userId
      };

      console.log("LoginRouter payload")
      console.log(payload)
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.status(200).json({ "token": token, "userId": user.userId, "email": user.email, "roleId": user.role_id,"studentId":user.studentId as number,"staffId" :user.staffId as number});

    } catch (e) {
      console.log(e)
      res.status(500).json({ message: "Login Error" })
    }
  }

  private login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body
      const user = await this.loginService.checkUserByEmail(email)
      // console.log(user)
      if (!user) {
         res.status(401).json({ message: "Please Register First" })
         return
      }
      
      if(user.password === undefined){
        res.status(401).json({ message: "Please Register First" })
         return
      }

      const match = await checkPassword(password, user.password);
      // console.log(match)
      if (match) {
        const payload = {
          id: user.userId
        }
        console.log(payload)
        const token = jwtSimple.encode(payload, jwt.jwtSecret);
        console.log("Success Login")
        console.log(token)
        res.status(200).json({ "token": token, "userId": user.userId, "email": user.email, "roleId": user.role_id ,companyId:user.companyId  ,companyName:user.company_name});
        return

      } else {
         res.status(401).json({ message: "Wrong User / Password , Please Try Again !" })
         return 
      }
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: "Login Fail Please Try Contact Us ,Thank you !" })
    }
  }

  
  private register = async (req: Request, res: Response) => {
    try {
      const { email, password, name, position, confirmPassword ,contact_number, contact_person, location, website, company_background, is_partner } = req.body
      console.log("Check1")
      if (email === null || password === null || name === null || position === null || contact_number === null || contact_person === null || location === null || website === null || company_background === null || is_partner === null) {
        res.status(401).json({ message: "Please Fill all the box . " })
        return
      }
      if (email === "" || password === "" || name === "" || position === "" || contact_number === "" || contact_person === "" || location === "" || website === "" || company_background === "" || is_partner === "") {
        res.status(401).json({ message: "Please Fill all the box . " })
        return
      }
      console.log("Check2")
      if(password !== confirmPassword){
        res.status(401).json({message:"Password is different , Please Try Again. "})
        return 
      }

      const user = await this.loginService.checkUserByEmail(email)
      console.log(user)
      console.log("Create")
      if (user) {
        res.status(400).json({ message: "Already Register" });
        return;
      }

      const userInfo = await this.loginService.createCompanyUser(email, password, name, position, contact_number, contact_person, location, website, company_background, is_partner)
      console.log(userInfo)

      if(userInfo){
        res.status(200).json(userInfo)
        return
      }else{
        res.status(400).json({isSuccess:false})
        return
      }
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: "Register Fail Please Try Contact Us ,Thank you !" })


    }
  }




}