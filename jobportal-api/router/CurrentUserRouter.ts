import * as express from 'express'
import { Request, Response } from 'express';
import { LoginService } from '../service/LoginService';

export class CurrentUserRouter {
    constructor(private loginService: LoginService) {

    }
    router() {
        const router = express.Router();
        router.get('/profile', this.getUserProfile)
        return router;
    }

    private getUserProfile = async (req: Request, res: Response) => {
        try {

            // console.log("Current User Router req.user")
            // console.log(req.user)
            // console.log("typeof req.user?.gitlab_id")
            // console.log(typeof req.user?.gitlab_id)

            if (req.user == null) {
                 res.status(401).json({ message: "Not Working" })
                 return
            } else {
                let gitlabId = req.user.gitlab_id;
                if (gitlabId == null) { return }

                // console.log("Current User Router gitlabId")
                // console.log(gitlabId)
                if (req.user.role_id === 1 && gitlabId) {
                    const result = await this.loginService.getStaffByGitlabId(gitlabId);
                     res.json(result);
                     return 
                } else if(req.user.role_id === 2 && gitlabId) {
                    const result = await this.loginService.getUserByGitlabId(gitlabId);
                    res.json(result);
                    return
                }

                // console.log("Current User Router result")
                // console.log(result)
                return


            }

        } catch (e) {
            console.log(e)
            return res.status(500).json({ msg: e.message })
        }
    }

}