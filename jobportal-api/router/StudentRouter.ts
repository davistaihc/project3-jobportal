import express from 'express';
import { Request, Response } from 'express';
import { StudentService } from '../service/StudentService';
const internalErrorMsg = "Sorry Internal Error!"
import multer from 'multer'
import aws from 'aws-sdk'
import { tmp } from '../main';
import fs from 'fs'
import dotenv from 'dotenv';

dotenv.config()


type Multer = ReturnType<typeof multer>

const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});


export class StudentRouter {
    constructor(private studentService: StudentService, private upload: Multer) { }

    router() {
        const router = express.Router();
        router.get('/download', this.downloadFileApi)
        router.get('/cohort', this.getCohorts)
        router.get('/students', this.searchStudents)
        router.get('/:id', this.getStudentsById)
        router.post('/', this.addStudent)
        router.put('/:id', this.updateStudent)
        router.put('/resume/:id', this.upload.single('resume'), this.uploadStudentResume)
        router.put('/portfolio/:id', this.upload.single('portfolio'), this.uploadStudentPortfolio)
        router.put('/coverLetter/:id', this.uploadStudentCoverLetter)
        router.put('/delete/resume/:id', this.deleteResume)
        router.put('/delete/portfolio/:id', this.deletePortfolio)
        router.put('/staff/:id', this.updateStudentByStaff)
        router.put('/updateStatus/:id', this.updateStudentJobApplicationStatus)
        return router;
    }

    private addStudent = async (req: Request, res: Response) => {
        const newStudent = req.body;
        try {
            const result = await this.studentService.addStudent(newStudent);
            res.json({ isSuccess: true, data: result })
        } catch (e) {
            console.log(e);
            res.status(400).json({ success: false })
        }
    }

    private getCohorts = async (req: Request, res: Response) => {
        try {
            const result = await this.studentService.getAllCohorts();
            res.status(200).json(result);

        } catch (e) {
            console.log(e.message);
            res.status(500).json({ msg: "internal error, not able to get cohorts info" })
        }
    }

    private getStudentsById = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const result = await this.studentService.getStudentsById(id);

            if (result.exist === false) {
                res.status(403).json({ msg: "invalid student id" });
            } else {
                res.status(200).json({ isSuccess: true, student: result });
            }

        } catch (e) {
            console.log(e.message);
            res.status(500).json({ msg: "internal error" })
        }
    }

    private searchStudents = async (req: Request, res: Response) => {
        try {
            if (!req.query.q) {

                const students = await this.studentService.getAllStudents()
                res.json(students);
                return
            }
            const keyword = req.query.q.toString().split(' ').join('&')
            const studentsResult = await this.studentService.searchStudentsByKeyword(
                keyword
            )
            res.json({ isSuccess: true, data: studentsResult });
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }
    }

    updateStudent = async (req: Request, res: Response) => {
        try {
            const studentId = parseInt(req.params.id);
            const { first_name, surname, status, contact_number,
                salary, company_name, position, way, onboard_date
            } = req.body
            console.log(req.body)


            const salaryInt = parseInt(salary)
            const onboard = parseInt(onboard_date)
            console.log(onboard)
            console.log(studentId)
            console.log(onboard_date)
            let statusId = 0;
            if (status == "Available") {
                statusId = 1
            } else if (status == "Not Available") {
                statusId = 2
            } else if (status == "Employed") {
                statusId = 3
            } else {
                const result = await this.studentService.getStudentsById(studentId)
                statusId = result.student_status_id
            }

            // await this.studentService.updateStudentGraduateDate(studentId,graduate_date)
            console.log(statusId)

            if (statusId === 3) {
                if (!salaryInt) {
                    await this.studentService.updateStudentEmployedInfo(studentId, null, onboard_date, position, company_name, way)
                } else {
                    await this.studentService.updateStudentEmployedInfo(studentId, salaryInt, onboard_date, position, company_name, way)
                }
            }

            console.log(`First Name = ${first_name}`)
            console.log(`Surname Name = ${surname}`)
            console.log(`Contact Number = ${contact_number}`)
            console.log(`Student StatusID = ${statusId}`)
            await this.studentService.updateStudentBasicInfo(studentId, first_name, surname, statusId, contact_number)

            return res.status(200).json({ msg: "Success Update!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ msg: "internal error" })
        }
    }


    private uploadStudentResume = async (req: Request, res: Response) => {
        try {
            console.log("Start UploadRouter")
            const studentId = parseInt(req.params.id)
            console.log(`studentId = ${studentId}`)
            const resume_url = (req.file as any).location
            const resume = req.file.originalname
            console.log(`resume = ${resume}`)
            console.log(req.file)

            if (req.file.mimetype !== "application/pdf") {
                res.json(400).json({ msg: "Please upload pdf !" })
                return
            }

            const result = await this.studentService.uploadStudentResume(studentId, resume, resume_url)
            console.log(result)
           
            if (result) {
                res.status(200).json({ msg: "Success Upload Resume" })
            } else {
                res.status(500).json({ msg: internalErrorMsg })
            }


        } catch (e) {
            console.log(e)
            res.status(500).json({ msg: "internal Error,Please Try Again Later!" })
        }
    }

    private uploadStudentPortfolio = async (req: Request, res: Response) => {
        try {
            console.log("Start UploadRouter")
            const studentId = parseInt(req.params.id)
            console.log(`studentId = ${studentId}`)
            const portfolio_url = (req.file as any).location
            const portfolio = req.file.originalname
            console.log(`portfolio = ${portfolio}`)
            console.log(req.file)

            if (req.file.mimetype !== "application/pdf") {
                res.json(400).json({ msg: "Please upload pdf !" })
                return
            }

            const result = await this.studentService.uploadStudentPortfolio(studentId, portfolio, portfolio_url)
            console.log(result)
            if (result) {
                res.status(200).json({ msg: "Success Upload PortFolio" })
            } else {
                res.status(500).json({ msg: internalErrorMsg })
            }


        } catch (e) {
            console.log(e)
            res.status(500).json({ msg: "internal Error,Please Try Again Later!" })
        }
    }

    private uploadStudentCoverLetter = async (req: Request, res: Response) => {
        try {
            console.log("Start UploadRouter")
            const studentId = parseInt(req.params.id)
            console.log(`studentId = ${studentId}`)
            const coverLetter = req.body.cover_letter
            console.log(`coverLetter = ${coverLetter}`)
            const result = await this.studentService.uploadStudentCoverLetter(studentId, coverLetter)
            console.log(result)
            if (result) {
                res.status(200).json({ msg: "Success Upload Resume" })
            } else {
                res.status(500).json({ msg: internalErrorMsg })
            }


        } catch (e) {
            console.log(e)
            res.status(500).json({ msg: "internal Error,Please Try Again Later!" })
        }
    }

    private download = async (filename: string) => {
        return new Promise<void>((resolve, reject) => {
            try {
                console.log(tmp)
                const params = { Bucket: `${process.env.AWS_S3_BUCKET}`, Key: `${filename}` }
                const file = fs.createWriteStream(`${tmp}/${filename}`)
                const s3Promise = s3.getObject(params).promise();
                s3Promise.then((data) => {
                    console.log(data)
                    file.write(data.Body, () => {
                        file.end();
                    });
                }).then(() => {
                    file.on('finish', () => {
                        console.log("Success Write Resume")
                        resolve();
                    })
                }).catch((err) => {
                    console.log(err)
                    reject(err);
                })

            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }

    private downloadFileApi = async (req: Request, res: Response) => {
        try {
            const file = req.query.filename.toString()
            console.log(file)
            await this.download(file)
            console.log(tmp)
            const url = `${tmp}/${file}`
            res.status(200).download(url)
        } catch (e) {
            console.log(e)
            res.status(500).json({ "Success": false })
        }
    }

    private deleteResume = async (req: Request, res: Response) => {
        try {
            const studentId = req.body.studentId
            const result = await this.studentService.deleteStudentResume(studentId)
            if (!result) {
                res.status(400).json({ message: "Delete Fail,Try Again" })
            } else {
                res.status(200).json({ isSuccess: true })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Delete Fail" })
        }
    }
    private deletePortfolio = async (req: Request, res: Response) => {
        try {
            const studentId = req.body.studentId
            const result = await this.studentService.deleteStudentPortfolio(studentId)
            if (!result) {
                res.status(400).json({ message: "Delete Fail,Try Again" })
            } else {
                res.status(200).json({ isSuccess: true })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Delete Fail" })
        }
    }

    private updateStudentByStaff = async (req: Request, res: Response) => {
        try {
            const studentId = parseInt(req.params.id);
            const { status,
                salary, company_name, position, way, onboard_date
            } = req.body
            const salaryInt = parseInt(salary)
            let statusId = 0;

            if (status === "Available") {
                statusId = 1
            } else if (status == "Not Available") {
                statusId = 2
            } else {
                statusId = 3
            }
            // } else {
            //     const result = await this.studentService.getStudentsById(studentId)
            //     console.log(result)
            //     statusId = result.student_status_id
            // }

            await this.studentService.updateStudentStatus(studentId, statusId)

            if (!salaryInt) {
                await this.studentService.updateStudentEmployedInfo(studentId, null, onboard_date, position, company_name, way)
                res.status(200).json({ message: "Update Success!" })
                return
            } else {
                await this.studentService.updateStudentEmployedInfo(studentId, salaryInt, onboard_date, position, company_name, way)
                res.status(200).json({ message: "Update Success" })
                return
            }



        } catch (e) {
            console.log(e)
            res.status(500).json({ message: "Update Fail" })
        }
    }

    private updateStudentJobApplicationStatus = async (req: Request, res: Response) => {
        try {
            const {studentId, statusId, jobId} = req.body;
            console.log(studentId,statusId,jobId)
            const result = await this.studentService.updateStudentJobAppStatus(studentId, statusId, jobId)

            if (result) {
                res.status(200).json({ msg: "Success Update JobApplication Status" })
            } else {
                res.status(500).json({ msg: "Update Fail" })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({ msg: "Update Fail" })
        }
    }



}

