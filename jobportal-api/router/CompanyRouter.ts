import express from 'express';
import { Request, Response } from 'express';
import { CompanyService } from '../service/CompanyService';


export class CompanyRouter {
    constructor(private companyService: CompanyService) { }

    router() {
        const router = express.Router();
        router.post('/', this.addCompany)
        router.post('/job', this.addJobByEmployers)
        router.get('/', this.getCompanies)
        router.get('/search', this.searchCompany)
        router.put('/update/:id', this.updateCompany)
        router.get('/:id', this.getCompanyById)

        return router;
    }

    private addCompany = async (req: Request, res: Response) => {
        const newCompany = req.body;
        try {
            const result = await this.companyService.addCompany(newCompany);
            res.json({ isSuccess: true, id: result })
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }
    }

    private addJobByEmployers = async (req: Request, res: Response) => {
        const newJob = req.body;
        console.log(newJob)
        try {
            const result = await this.companyService.addJobByEmployers(newJob);
            res.json({ isSuccess: true, data: result })
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }
    }

    private getCompanies = async (req: Request, res: Response) => {
        try {
            const companies = await this.companyService.getAllCompanies()
            res.json(companies);
        }
        catch (err) {
            console.log(err);
            res.status(500).json({ message: `internal server error, cannot get companies' info` });
        }
    }

    private searchCompany = async (req: Request, res: Response) => {
        try {
            const keyword = req.query.keyword as string;
            // if (!keyword) {
            //     company = await this.companyService.getAllCompanies();
            // } else {
            const company = await this.companyService.searchCompanyByKeyword(keyword);
            // }
            res.json(company);
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    private getCompanyById = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const result = await this.companyService.getCompanyById(id);

            if (result.exist === false) {
                res.status(403).json({ msg: "invalid company id" });
            } else {
                res.status(200).json(result);
            }

        } catch (e) {
            console.log(e.message);
            res.status(500).json({ msg: "internal error" })
        }
    }

    private updateCompany = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const body = req.body;
            await this.companyService.updateCompany(id, body)
            res.status(200).json({ isSuccess: true });
        } catch (e) {
            console.log(e);
            res.status(500).json({ msg: "internal error" })
        }
    }


}

