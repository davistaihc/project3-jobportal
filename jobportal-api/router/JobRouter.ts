import express from 'express';
import { Request, Response } from 'express';
import { JobService } from '../service/JobService';


export class JobRouter {
    constructor(private jobService: JobService) { }

    router() {
        const router = express.Router();
        router.post('/', this.addJob)
        // router.get('/', this.getJobs)
        router.get('/', this.searchJobs)
        router.put('/update/:id', this.updateJobApprovalStatus)
        router.get('/:id', this.getJobById)
        router.get('/company/:id', this.getJobByCompanyId)
        router.put('/delete/:id',this.deleteJob)
        return router;
    }

    private addJob = async (req: Request, res: Response) => {
        const newJob = req.body;
        try {
            const result = await this.jobService.addJob(newJob);
            res.json({ isSuccess: true, data: result })
        } catch (e) {
            console.log(e);
            res.status(400);
            res.json({ success: false })
        }
    }

    // private getJobs = async (req: Request, res: Response) => {
    //     try {
    //         const jobs = await this.jobService.getAllJobs()
    //         res.json(jobs);
    //     }
    //     catch (err) {
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // }

    private searchJobs = async (req: Request, res: Response) => {
        try {
            console.log(req.user)
            const keyword = req.query.keyword as string;
            console.log("Entered keyword is " + keyword)
            let jobs;
            if (keyword === undefined) {
                jobs = await this.jobService.getAllJobs();
            } else {
                jobs = await this.jobService.searchJobsByKeyword(keyword);
            }
            res.json(jobs);
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    private getJobById = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const result = await this.jobService.getJobById(id);

            if (result.exist === false) {
                res.status(403).json({ msg: "invalid job id" });
            } else {
                res.status(200).json(result);
            }

        } catch (e) {
            console.log(e.message);
            res.status(500).json({ msg: "internal error" })
        }
    }
    private getJobByCompanyId = async (req: Request, res: Response) => {
        try {
            const companyId = parseInt(req.params.id);
            const result = await this.jobService.getJobByCompanyId(companyId);

            if (result.exist === false) {
                res.status(403).json({ msg: "invalid job id" });
            } else {
                res.status(200).json(result);
            }

        } catch (e) {
            console.log(e.message);
            res.status(500).json({ msg: "internal error" })
        }
    }

    private updateJobApprovalStatus = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            const body = req.body;
            // console.log(body)
            await this.jobService.updateJobApproval(id, body)
            res.status(200).json({ isSuccess: true });
        } catch (e) {
            console.log(e);
            res.status(500).json({ msg: "internal error" })
        }
    }

    private deleteJob = async (req: Request, res: Response) => {
        try {
            const id  = parseInt(req.params.id);
            const result = await this.jobService.deleteJob(id) 
            
            if(!result){
                res.status(400).json({message:"Fail Delete Job"})
            }
            
            res.status(200).json({isSuccess:true})

        } catch (e) {
            console.log(e);
            res.status(500).json({ msg: "internal error" })
        }
    }



}

