export default Object.freeze({
    JOB_SKILL: "job_skill",
    STUDENT_JOB_MAPPING: "student_job_mapping",
    JOB: "job",
    STUDENT: "student",
    STAFF: "staff",
    SKILL: "skill",
    COMPANY: "company",
    // STATUS: "status",
    EMPLOYMENT_TYPE: "employment_type",
    APP_STATUS: "app_status",

})
